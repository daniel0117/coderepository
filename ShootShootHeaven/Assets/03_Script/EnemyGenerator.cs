﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
public class EnemyGenerator : MonoBehaviour {

	public Sprite[] enemySprite;
	public GameObject gameManager;
	public GameObject oldEnemy;
	public GameObject newEnemy;
	public GameObject Bullets;
	public Vector3 Position;
	public float spawnTerm = 5.0f;
	public int KillCount;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(!gameManager.GetComponent<GameManager>().isPaused){
		spawnTerm -= Time.deltaTime;
     	if ( spawnTerm < 0 )
     	{

			 KillCount++;
			 if(KillCount <=10){
        	 spawnTerm = 4.0f;
			 GenerateEnemy();
		 	}
			if(KillCount <= 16 && KillCount > 10){
        	 spawnTerm = 3.0f;
			 GenerateEnemy();
		 	}
			if(KillCount <=24 && KillCount > 16){
        	 spawnTerm = 2.5f;
			 GenerateEnemy();
		 	}	
			if(KillCount <=30 && KillCount > 24){
        	 spawnTerm = 2.0f;
			 GenerateEnemy();
		 	}
			if(KillCount>30 ){
        	 spawnTerm = 1.7f;
			 GenerateEnemy();
		 }	
     	}
		}
	}

	void GenerateEnemy(){
		Position = new Vector3(-150,UnityEngine.Random.Range(-50.0f, 50.0f), 0);
		newEnemy = Instantiate(oldEnemy, Position, oldEnemy.transform.rotation);
		newEnemy.GetComponent<EnemyManager>().isActive = true;
		int enemySpriteNum = UnityEngine.Random.Range(0,enemySprite.Length);
		newEnemy.GetComponent<Image>().sprite = enemySprite[enemySpriteNum];
		newEnemy.GetComponent<Image>().SetNativeSize();
		newEnemy.transform.SetParent(Bullets.transform,false);
		newEnemy.SetActive(true);
	}
}
