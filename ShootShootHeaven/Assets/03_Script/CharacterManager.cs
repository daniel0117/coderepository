﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CharacterManager : MonoBehaviour {
	public bool isActive = true;
	public int direction;
	public GameObject gameManager;	
	public float characterSpeed;
	public GameObject CharacterPosition;
	public Vector3 StartingPos = new Vector3(103.6f,3.9f,0);
	private Vector3 YposMax = new Vector3(110f,75,0);
    private Vector3 YposMin = new Vector3(110f,-75,0);

	public GameObject Shooter;
	// Use this for initialization
	void Start () {
		CharacterInit();
	}
	
	// Update is called once per frame
	void Update () {
		if(isActive && !gameManager.GetComponent<GameManager>().isGameOver && !gameManager.GetComponent<GameManager>().isPaused)
		CharacterMove();
	}
	void CharacterMove(){
			if(direction == 1){
				this.transform.localPosition += Vector3.up*characterSpeed*30*Time.deltaTime;
			}
			if(direction == -1){
				this.transform.localPosition += Vector3.down*characterSpeed*30*Time.deltaTime;
			}
			if(this.transform.localPosition.y < -75){
				direction = 1;
			}
			if(this.transform.localPosition.y > 75){
				direction = -1;
			}
	}
	void CharacterInit(){
		gameManager.GetComponent<GameManager>().isPaused = true;
		CharacterPosition.transform.localPosition = StartingPos;
		characterSpeed =1.2f;
		direction = 1;
	}
}
