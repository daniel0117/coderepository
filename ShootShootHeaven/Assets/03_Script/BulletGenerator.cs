﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BulletGenerator : MonoBehaviour {
	public GameObject oldMissile;
	public GameObject lineRenderer;
	public GameObject newMissile;
	public GameObject Bullets;
	public GameObject gameManager; 
	public float timeLeft = 3f;
	public float initialVelocity = 10f;
	public float power = 3.0f;
	public float acceleration = 10f;
	public Text LocText;
	public Text DirText;
	public Text AngText;
	public Text TimeText;
	private Vector3 direction;
	private bool missileExist = false;
	
	// Use this for initialization
	void Start () {
		missileExist = false;
		oldMissile.transform.gameObject.tag = "MainCamera";
	}
	
	void Update () {
		if(gameManager.GetComponent<GameManager>().isPlay){
			if(missileExist){
				TimeText.text = "Time : " + newMissile.GetComponent<BulletManager>().timer.ToString();
				DirText.text = "Direction : (" + (newMissile.GetComponent<BulletManager>().DIR.x +newMissile.transform.position.x).ToString() + " , " + (newMissile.GetComponent<BulletManager>().DIR.y +newMissile.transform.position.y).ToString() + ")";
				LocText.text = "Location : "+ newMissile.transform.position.ToString();
			}			
		}
	}
	public void GenerateBullet(Vector2 Direction, Vector3 Position)
	{

		missileExist = true; 
		AngText.text = "Angle : " + (90-lineRenderer.GetComponent<LineRenderManager>().ShootAngle).ToString();
		newMissile = Instantiate(oldMissile, Position, oldMissile.transform.rotation);
		newMissile.GetComponent<BulletManager>().DIR = Direction * power;
		newMissile.GetComponent<BulletManager>().isActive = true;
		newMissile.transform.gameObject.tag = "Finish";
		newMissile.transform.SetParent(Bullets.transform,true);
		newMissile.transform.localScale = Vector3.one;
	
		//Text.text = "Highscore : " + highscore.ToString();


	}
}


