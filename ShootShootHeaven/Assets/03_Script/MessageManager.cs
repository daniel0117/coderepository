﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
public class MessageManager : MonoBehaviour {
	public GameObject gameManager;
	public GameObject message;
	public Sprite[] countdown;
	public Sprite pause;
	public 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}
	public void PlayPressed(){
		if(gameManager.GetComponent<GameManager>().isPlay){
			gameManager.GetComponent<GameManager>().isPaused = true;
			
			Color tempColor = message.GetComponent<Image>().color;
			tempColor.a = 1.0f;
			message.GetComponent<Image>().color = tempColor;
			
			StartCoroutine(CoroutineCountDownAnimation(0, 0.8f, message));
			
			gameManager.GetComponent<GameManager>().isPlay = false; 
			gameManager.GetComponent<GameManager>().isPaused = false; 
			
			tempColor.a = 0.0f;
			message.GetComponent<Image>().color= tempColor;
		}
	}
	public void PausePressed(){
		if(gameManager.GetComponent<GameManager>().isPaused){
			Color tempColor = message.GetComponent<Image>().color;
			tempColor.a = 1.0f;

			message.GetComponent<Image>().color = tempColor;
			message.GetComponent<Image>().sprite = pause;
			message.GetComponent<Image>().SetNativeSize();
		}
	}
	IEnumerator CoroutineCountDownAnimation(float firstTerm, float tTerm, GameObject other)
	{
		yield return new WaitForSeconds(firstTerm);

		WaitForSeconds tWFS = new WaitForSeconds(tTerm);

		for(int index = 0; index < countdown.Length; index++)
		{
			message.GetComponent<Image>().sprite = countdown[index];
			message.GetComponent<Image>().SetNativeSize();
			yield return tWFS;
		}
	}
}
