﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveManager : MonoBehaviour {
	private float EnemySpeed;
	private int dir;
	private Vector3 EnemyPos1;
	private Vector3 EnemyPos2;
	private Vector3 EnemyPos3;
	private Vector3 returnSpot = new Vector3(162.8f,-42f,0f);
	public Vector3 posDiff = new Vector3(0f,6f,0f);
	// Use this for initialization
	void Start () {
		EnemyInit();
	}
	
	// Update is called once per frame
	void Update () {
		EnemyMove();
	}
	void EnemyMove(){
		if(this.transform.localPosition.x > -157f){
			this.transform.localPosition += Vector3.left * EnemySpeed *30* Time.deltaTime;
			if(dir == 1){
				this.transform.localPosition += Vector3.up*EnemySpeed*30*Time.deltaTime;
			}
			if(dir == -1){
				this.transform.localPosition += Vector3.down*EnemySpeed*30*Time.deltaTime;
			}
			if(this.transform.localPosition.y >= EnemyPos2.y){
				dir = -1;
			}
			if(this.transform.localPosition.y <= EnemyPos3.y){
				dir = 1;
			}
		}
		else{
			this.transform.localPosition = returnSpot;
		}
	}
	void EnemyInit(){
		//isActive = true;
		EnemySpeed = 0.5f;
		EnemyPos1 = this.transform.localPosition;
		EnemyPos2 = EnemyPos1+posDiff;
		EnemyPos3 = EnemyPos1-posDiff;
		dir = 1;
	}
}
