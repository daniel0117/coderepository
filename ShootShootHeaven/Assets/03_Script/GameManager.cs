﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	//public GameObject Message;
	public bool isGameOver = false;
	public bool enemyVictory = false;
	public bool isPaused = false;
	public bool isPlay = false;
	public int score = 0;

	public int highscore = 0;
	public Text highscoreText;
	public string highScoreKey = "HighScore";
	public GameObject Character;
	public Vector3 StartingPos = new Vector3(103.6f,3.9f,0);
	public Text scoreText; 
	void SetCountText(){
        
    }

	// Use this for initialization
	void Start () {
		GameManagerInit();
	}
	
	// Update is called once per frame
	void Update () {
		//UpdateScore();
	}

	private void GameManagerInit()
	{
		highscore = PlayerPrefs.GetInt(highScoreKey,0);
		highscoreText.text = "Highscore : " + highscore.ToString();
	}

	public void GameOver()
	{
		isGameOver = true;
		UpdateHighscore();
		Time.timeScale = 0;
	}
	
	public void Pause(){
		isPaused = true;
		//Message.GetComponent<MessageManager>().PausePressed();
		Time.timeScale = 0;
	}
	public void Continue(){
		if(!enemyVictory){
			isPaused = false;
			isPlay = true;
			//Message.GetComponent<MessageManager>().PlayPressed();
			Time.timeScale = 1.0f;
		}
	}
	public void Replay(){
		UpdateHighscore();
		Time.timeScale = 1.0f;
		Character.GetComponent<CharacterManager>().CharacterPosition.transform.localPosition = StartingPos;
		SceneManager.LoadScene("GameScene");
	}
	public void StartingPage(){
		UpdateHighscore();
		Time.timeScale = 1;
		SceneManager.LoadScene("LoadingPage");
	}
	public void UpdateHighscore(){
		if(highscore < score){
			Debug.Log("UpdateHighScore");
			highscore = score;
			PlayerPrefs.SetInt(highScoreKey, highscore);
        	PlayerPrefs.Save();
			highscoreText.text = "Highscore : " + highscore.ToString();
		}
	}
	public void UpdateScore(){
		scoreText.text = "Score : " +score.ToString();
	}

	//PlayerPrefs.
}
