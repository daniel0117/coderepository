﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; 
using UnityEngine.UI;

public class BulletManager : MonoBehaviour {

	public Sprite[] explosion;
	public GameObject gameManager;


	public bool isActive;
	public Vector3 DIR;
    public float acceleration = 1f;
	public GameObject Bullets;
	public float mass = 1f;
	public float timer = 0f;
	// Use this for initialization
	public float t;
	void Start () {
		//startTime = Time.time;
	}
	
	// Update is called once per frame
	void FixedUpdate() 
	{	if(isActive && !gameManager.GetComponent<GameManager>().isGameOver){
			timer+=Time.deltaTime;
			MissileFire();
		}
	}
	public void MissileFire()
	{
		if(!isActive) { return; }
		DIR += Vector3.down * acceleration * mass;
		this.transform.localPosition += DIR;
		this.transform.eulerAngles = new Vector3(0,0,(float)((Mathf.Atan(DIR.y/DIR.x)*180f/Math.PI)));
		if(this.transform.localPosition.y < -115 ){
			this.gameObject.SetActive(false);
		}
	}

	void OnTriggerEnter2D(Collider2D other) 
    {
         if (transform.gameObject.CompareTag("Finish"))
                {
					if(!other.gameObject.CompareTag("MainCamera") && !other.gameObject.CompareTag("Finish")){
						this.isActive = false;
						transform.gameObject.tag = "NotFinish";
						other.gameObject.GetComponent<EnemyManager>().isActive = false;
						other.gameObject.GetComponent<EnemyManager>().isHit = true;
						other.gameObject.GetComponent<EnemyManager>().fallDir = DIR;
						StartCoroutine(CoroutineExplosionAnimation(0, 0.09f, other.gameObject));
						other.gameObject.GetComponent<EnemyManager>().CallCoroutineCloudeFade();

						gameManager.GetComponent<GameManager>().score +=10;
						gameManager.GetComponent<GameManager>().UpdateScore();
						
					
				 } 
                }
    }

	IEnumerator CoroutineExplosionAnimation(float firstTerm, float tTerm, GameObject other)
	{
		yield return new WaitForSeconds(firstTerm);

		WaitForSeconds tWFS = new WaitForSeconds(tTerm);

		for(int index = 0; index < explosion.Length; index++)
		{
			transform.gameObject.GetComponent<Image>().sprite = explosion[index];
			transform.gameObject.GetComponent<Image>().SetNativeSize();
			//Debug.Log(tTerm);
			yield return tWFS;
		}
		
		//other.gameObject.SetActive(false);
        transform.gameObject.SetActive(false);
	}
	
}
