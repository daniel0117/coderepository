﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FireEX : MonoBehaviour {
	private Rigidbody2D rb2d;
	public float acceleration  = 20f;
	public float initialVelocity = 5f;
	public float angle = 45f;
	public float radians;
	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
 		radians = angle * (float)Math.PI / 180f;
		transform.position += Vector3.right* initialVelocity *  (float)System.Math.Cos(radians) * Time.deltaTime;
		transform.position += Vector3.up* (initialVelocity * (float)System.Math.Sin(radians) -(acceleration*Time.time)) * Time.deltaTime;
		Debug.Log(initialVelocity * (float)System.Math.Sin(radians) -(acceleration*Time.time));
	}

}
/* 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConstantVelocity : MonoBehaviour {
	private Rigidbody2D rb2d;
    private GameObject Compare;
    public float speed = 150;
    public Text countText;
    public Text winText;
	private int count;
    private int count2;
    //mobile components
   private Vector3 touchPosition;
   private Vector3 direction;
   private float moveSpeed = 10f;


    void SetCountText(){
        countText.text = "UFO1 COUNT : " +count.ToString();
        //if(count == 4 && count2== 4)
          //  winText.text = "STALEMATE";
        if(count == 12)
            winText.text = "UFO1 WINS";
    }
	void Start(){
      
		Debug.Log("I'm Here");
		rb2d = GetComponent<Rigidbody2D>();
		count = 0;
        winText.text = "";
        SetCountText();
	
	}

	void Update()
	{   

        if(Input.touchCount > 0){
           Touch touch = Input.GetTouch(0);
           touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
           touchPosition.z = 0;
           direction = touchPosition - transform.position;
           rb2d.velocity = new Vector2(direction.x, direction.y) * 5;

           if(touch.phase == TouchPhase.Ended)
                rb2d.velocity = Vector2.zero;

        }
		/* 
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
		Vector2 movement = new Vector2(moveHorizontal,moveVertical);
		
		rb2d.velocity = (movement * speed);
    
        Compare = GameObject.FindGameObjectWithTag("Respawn");
        count2 = Compare.GetComponent<PlayerControler>().count;
         if (Input.GetKey ("w")) {
            transform.position += Vector3.up * speed * Time.deltaTime;
         }
         if (Input.GetKey ("s")) {
             transform.position += Vector3.down * speed * Time.deltaTime;
         }
         if (Input.GetKey ("d")) {
             transform.position += Vector3.right * speed * Time.deltaTime;
         }
         if (Input.GetKey ("a")) {
             transform.position += Vector3.left * speed * Time.deltaTime;
         }
           

	}
	  
	  void OnTriggerEnter2D(Collider2D other) 
    {
         if (transform.CompareTag("Player") && other.gameObject.CompareTag("PickUp"))
                {
                     other.gameObject.SetActive(false);
                     count = count+1;
                     SetCountText();
                }
    }
    
}
*/