﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
public class EnemyManager : MonoBehaviour {
	public Sprite[] dissipate;
	public bool isActive = false;
	public bool isHit = false;
	public bool isCloudeFade = false;
	public float EnemySpeed;
	public GameObject Bullets;
	public  Vector3 fallDir;
	private Vector3 EnemyPos1;
	private Vector3 EnemyPos2;
	private Vector3 EnemyPos3;
	public Vector3 posDiff = new Vector3(0f,12f,0f);
	public int dir;
	public float acceleration = 0.05f;
	public float mass = 1f;
	public GameObject gameManager;
	void Start () {
		EnemyInit();
	}
	void FixedUpdate () {
		if(!gameManager.GetComponent<GameManager>().isGameOver){
			if(!isActive) {
				if(isHit)
					ExplodeDie();
				return;
			}
			else
			{
				EnemyMove();
			}
		}
	}

	void ExplodeDie(){
		fallDir += Vector3.down * acceleration * mass;
		this.transform.position += fallDir;
		
		if(this.transform.localPosition.y < -300 && isCloudeFade){
			this.gameObject.SetActive(false);
		}
	}

	public void CallCoroutineCloudeFade()
	{
        StartCoroutine(CoroutineCloudFade(0,0.1f, transform.Find("EnemyCloud").GetComponent<Image>().color, transform.Find("EnemyCloud")));
	}

	IEnumerator CoroutineCloudFade(float firstTerm, float tTerm, Color tmpColor,Transform cloud)
	{
		yield return new WaitForSeconds(firstTerm);
		//cloud.parent = Bullets.transform;
		cloud.SetParent(Bullets.transform, true);
		WaitForSeconds tWFS = new WaitForSeconds(tTerm);
		Color tempColor = cloud.GetComponent<Image>().color;

		for(float index = tmpColor.a; index >= -0.2f; index-=0.2f) 
		{
			tempColor.a = (float)index;
			cloud.GetComponent<Image>().color = tempColor;
			//Debug.Log(index);
			yield return tWFS;
		}
		//Debug.Log("DONE D'OH");
		isCloudeFade = true;
		cloud.gameObject.SetActive(false);
	}
	void EnemyMove(){
		if(this.transform.localPosition.x < 140f){
			this.transform.localPosition += Vector3.right * EnemySpeed *30* Time.deltaTime;
			if(dir == 1){
				this.transform.localPosition += Vector3.up*EnemySpeed*30*Time.deltaTime;
			}
			if(dir == -1){
				this.transform.localPosition += Vector3.down*EnemySpeed*30*Time.deltaTime;
			}
			if(this.transform.localPosition.y >= EnemyPos2.y){
				dir = -1;
			}
			if(this.transform.localPosition.y <= EnemyPos3.y){
				dir = 1;
			}
		}
		else{
			//gameManager.GetComponent<GameManager>().enemyVictory = true;
			gameManager.GetComponent<GameManager>().GameOver();
			this.transform.gameObject.SetActive(false);
		}
	}
	void EnemyInit(){
		//isActive = true;
		EnemySpeed = 0.5f;
		EnemyPos1 = this.transform.localPosition;
		EnemyPos2 = EnemyPos1+posDiff;
		EnemyPos3 = EnemyPos1-posDiff;
		dir = 1;
	}

	
}
