﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// MessageManager
struct MessageManager_t336836297;
// BulletManager
struct BulletManager_t1336418627;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t701940803;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t1884415901;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t768590915;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t3913627115;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t2311174851;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_t3841783507;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_t235587086;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_t240936516;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t2748928575;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_t2508470592;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Transform
struct Transform_t3600365921;
// EnemyManager
struct EnemyManager_t913934216;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// BulletGenerator
struct BulletGenerator_t1131765196;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.String
struct String_t;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;




#ifndef U3CMODULEU3E_T692745543_H
#define U3CMODULEU3E_T692745543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745543 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745543_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CCOROUTINECOUNTDOWNANIMATIONU3EC__ITERATOR0_T3654333551_H
#define U3CCOROUTINECOUNTDOWNANIMATIONU3EC__ITERATOR0_T3654333551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageManager/<CoroutineCountDownAnimation>c__Iterator0
struct  U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551  : public RuntimeObject
{
public:
	// System.Single MessageManager/<CoroutineCountDownAnimation>c__Iterator0::firstTerm
	float ___firstTerm_0;
	// System.Single MessageManager/<CoroutineCountDownAnimation>c__Iterator0::tTerm
	float ___tTerm_1;
	// UnityEngine.WaitForSeconds MessageManager/<CoroutineCountDownAnimation>c__Iterator0::<tWFS>__0
	WaitForSeconds_t1699091251 * ___U3CtWFSU3E__0_2;
	// System.Int32 MessageManager/<CoroutineCountDownAnimation>c__Iterator0::<index>__1
	int32_t ___U3CindexU3E__1_3;
	// MessageManager MessageManager/<CoroutineCountDownAnimation>c__Iterator0::$this
	MessageManager_t336836297 * ___U24this_4;
	// System.Object MessageManager/<CoroutineCountDownAnimation>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean MessageManager/<CoroutineCountDownAnimation>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 MessageManager/<CoroutineCountDownAnimation>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_firstTerm_0() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___firstTerm_0)); }
	inline float get_firstTerm_0() const { return ___firstTerm_0; }
	inline float* get_address_of_firstTerm_0() { return &___firstTerm_0; }
	inline void set_firstTerm_0(float value)
	{
		___firstTerm_0 = value;
	}

	inline static int32_t get_offset_of_tTerm_1() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___tTerm_1)); }
	inline float get_tTerm_1() const { return ___tTerm_1; }
	inline float* get_address_of_tTerm_1() { return &___tTerm_1; }
	inline void set_tTerm_1(float value)
	{
		___tTerm_1 = value;
	}

	inline static int32_t get_offset_of_U3CtWFSU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U3CtWFSU3E__0_2)); }
	inline WaitForSeconds_t1699091251 * get_U3CtWFSU3E__0_2() const { return ___U3CtWFSU3E__0_2; }
	inline WaitForSeconds_t1699091251 ** get_address_of_U3CtWFSU3E__0_2() { return &___U3CtWFSU3E__0_2; }
	inline void set_U3CtWFSU3E__0_2(WaitForSeconds_t1699091251 * value)
	{
		___U3CtWFSU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtWFSU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E__1_3() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U3CindexU3E__1_3)); }
	inline int32_t get_U3CindexU3E__1_3() const { return ___U3CindexU3E__1_3; }
	inline int32_t* get_address_of_U3CindexU3E__1_3() { return &___U3CindexU3E__1_3; }
	inline void set_U3CindexU3E__1_3(int32_t value)
	{
		___U3CindexU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U24this_4)); }
	inline MessageManager_t336836297 * get_U24this_4() const { return ___U24this_4; }
	inline MessageManager_t336836297 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MessageManager_t336836297 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOROUTINECOUNTDOWNANIMATIONU3EC__ITERATOR0_T3654333551_H
#ifndef U3CCOROUTINEEXPLOSIONANIMATIONU3EC__ITERATOR0_T2727895679_H
#define U3CCOROUTINEEXPLOSIONANIMATIONU3EC__ITERATOR0_T2727895679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BulletManager/<CoroutineExplosionAnimation>c__Iterator0
struct  U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679  : public RuntimeObject
{
public:
	// System.Single BulletManager/<CoroutineExplosionAnimation>c__Iterator0::firstTerm
	float ___firstTerm_0;
	// System.Single BulletManager/<CoroutineExplosionAnimation>c__Iterator0::tTerm
	float ___tTerm_1;
	// UnityEngine.WaitForSeconds BulletManager/<CoroutineExplosionAnimation>c__Iterator0::<tWFS>__0
	WaitForSeconds_t1699091251 * ___U3CtWFSU3E__0_2;
	// System.Int32 BulletManager/<CoroutineExplosionAnimation>c__Iterator0::<index>__1
	int32_t ___U3CindexU3E__1_3;
	// BulletManager BulletManager/<CoroutineExplosionAnimation>c__Iterator0::$this
	BulletManager_t1336418627 * ___U24this_4;
	// System.Object BulletManager/<CoroutineExplosionAnimation>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean BulletManager/<CoroutineExplosionAnimation>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 BulletManager/<CoroutineExplosionAnimation>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_firstTerm_0() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___firstTerm_0)); }
	inline float get_firstTerm_0() const { return ___firstTerm_0; }
	inline float* get_address_of_firstTerm_0() { return &___firstTerm_0; }
	inline void set_firstTerm_0(float value)
	{
		___firstTerm_0 = value;
	}

	inline static int32_t get_offset_of_tTerm_1() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___tTerm_1)); }
	inline float get_tTerm_1() const { return ___tTerm_1; }
	inline float* get_address_of_tTerm_1() { return &___tTerm_1; }
	inline void set_tTerm_1(float value)
	{
		___tTerm_1 = value;
	}

	inline static int32_t get_offset_of_U3CtWFSU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U3CtWFSU3E__0_2)); }
	inline WaitForSeconds_t1699091251 * get_U3CtWFSU3E__0_2() const { return ___U3CtWFSU3E__0_2; }
	inline WaitForSeconds_t1699091251 ** get_address_of_U3CtWFSU3E__0_2() { return &___U3CtWFSU3E__0_2; }
	inline void set_U3CtWFSU3E__0_2(WaitForSeconds_t1699091251 * value)
	{
		___U3CtWFSU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtWFSU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E__1_3() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U3CindexU3E__1_3)); }
	inline int32_t get_U3CindexU3E__1_3() const { return ___U3CindexU3E__1_3; }
	inline int32_t* get_address_of_U3CindexU3E__1_3() { return &___U3CindexU3E__1_3; }
	inline void set_U3CindexU3E__1_3(int32_t value)
	{
		___U3CindexU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U24this_4)); }
	inline BulletManager_t1336418627 * get_U24this_4() const { return ___U24this_4; }
	inline BulletManager_t1336418627 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(BulletManager_t1336418627 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOROUTINEEXPLOSIONANIMATIONU3EC__ITERATOR0_T2727895679_H
#ifndef BASEVERTEXEFFECT_T2675891272_H
#define BASEVERTEXEFFECT_T2675891272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2675891272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2675891272_H
#ifndef REFLECTIONMETHODSCACHE_T2103211062_H
#define REFLECTIONMETHODSCACHE_T2103211062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t2103211062  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t701940803 * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t1884415901 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t768590915 * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t3913627115 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t2311174851 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_t3841783507 * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3D_0)); }
	inline Raycast3DCallback_t701940803 * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t701940803 ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t701940803 * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t1884415901 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t1884415901 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t1884415901 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast2D_2)); }
	inline Raycast2DCallback_t768590915 * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_t768590915 ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_t768590915 * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t3913627115 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t3913627115 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t3913627115 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_t2311174851 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_t3841783507 * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_t3841783507 ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_t3841783507 * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_t2103211062_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t2103211062 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t2103211062 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t2103211062 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t2103211062 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_T2103211062_H
#ifndef LAYOUTUTILITY_T2745813735_H
#define LAYOUTUTILITY_T2745813735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t2745813735  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t2745813735_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache0
	Func_2_t235587086 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache1
	Func_2_t235587086 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache2
	Func_2_t235587086 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache3
	Func_2_t235587086 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache4
	Func_2_t235587086 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache5
	Func_2_t235587086 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache6
	Func_2_t235587086 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache7
	Func_2_t235587086 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T2745813735_H
#ifndef LAYOUTREBUILDER_T541313304_H
#define LAYOUTREBUILDER_T541313304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t541313304  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t3704657025 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304, ___m_ToRebuild_0)); }
	inline RectTransform_t3704657025 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t3704657025 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t3704657025 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t541313304_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_t240936516 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mg$cache0
	ReapplyDrivenProperties_t1258266594 * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache0
	Predicate_1_t2748928575 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache1
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache2
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache3
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache4
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_t240936516 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_t240936516 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_t240936516 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t1258266594 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t1258266594 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t2748928575 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t2748928575 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t2748928575 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T541313304_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t3704657025 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___rectTransform_0)); }
	inline RectTransform_t3704657025 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t3704657025 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#ifndef RECTANGULARVERTEXCLIPPER_T626611136_H
#define RECTANGULARVERTEXCLIPPER_T626611136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RectangularVertexClipper
struct  RectangularVertexClipper_t626611136  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_WorldCorners
	Vector3U5BU5D_t1718750761* ___m_WorldCorners_0;
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_CanvasCorners
	Vector3U5BU5D_t1718750761* ___m_CanvasCorners_1;

public:
	inline static int32_t get_offset_of_m_WorldCorners_0() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t626611136, ___m_WorldCorners_0)); }
	inline Vector3U5BU5D_t1718750761* get_m_WorldCorners_0() const { return ___m_WorldCorners_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_WorldCorners_0() { return &___m_WorldCorners_0; }
	inline void set_m_WorldCorners_0(Vector3U5BU5D_t1718750761* value)
	{
		___m_WorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorldCorners_0), value);
	}

	inline static int32_t get_offset_of_m_CanvasCorners_1() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t626611136, ___m_CanvasCorners_1)); }
	inline Vector3U5BU5D_t1718750761* get_m_CanvasCorners_1() const { return ___m_CanvasCorners_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_CanvasCorners_1() { return &___m_CanvasCorners_1; }
	inline void set_m_CanvasCorners_1(Vector3U5BU5D_t1718750761* value)
	{
		___m_CanvasCorners_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasCorners_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGULARVERTEXCLIPPER_T626611136_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef AXIS_T3613393006_H
#define AXIS_T3613393006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Axis
struct  Axis_t3613393006 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t3613393006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T3613393006_H
#ifndef SCALEMODE_T2604066427_H
#define SCALEMODE_T2604066427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/ScaleMode
struct  ScaleMode_t2604066427 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScaleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScaleMode_t2604066427, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEMODE_T2604066427_H
#ifndef CORNER_T1493259673_H
#define CORNER_T1493259673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Corner
struct  Corner_t1493259673 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Corner::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Corner_t1493259673, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_T1493259673_H
#ifndef U3CCOROUTINECLOUDFADEU3EC__ITERATOR0_T2338107655_H
#define U3CCOROUTINECLOUDFADEU3EC__ITERATOR0_T2338107655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyManager/<CoroutineCloudFade>c__Iterator0
struct  U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655  : public RuntimeObject
{
public:
	// System.Single EnemyManager/<CoroutineCloudFade>c__Iterator0::firstTerm
	float ___firstTerm_0;
	// UnityEngine.Transform EnemyManager/<CoroutineCloudFade>c__Iterator0::cloud
	Transform_t3600365921 * ___cloud_1;
	// System.Single EnemyManager/<CoroutineCloudFade>c__Iterator0::tTerm
	float ___tTerm_2;
	// UnityEngine.WaitForSeconds EnemyManager/<CoroutineCloudFade>c__Iterator0::<tWFS>__0
	WaitForSeconds_t1699091251 * ___U3CtWFSU3E__0_3;
	// UnityEngine.Color EnemyManager/<CoroutineCloudFade>c__Iterator0::<tempColor>__0
	Color_t2555686324  ___U3CtempColorU3E__0_4;
	// UnityEngine.Color EnemyManager/<CoroutineCloudFade>c__Iterator0::tmpColor
	Color_t2555686324  ___tmpColor_5;
	// System.Single EnemyManager/<CoroutineCloudFade>c__Iterator0::<index>__1
	float ___U3CindexU3E__1_6;
	// EnemyManager EnemyManager/<CoroutineCloudFade>c__Iterator0::$this
	EnemyManager_t913934216 * ___U24this_7;
	// System.Object EnemyManager/<CoroutineCloudFade>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean EnemyManager/<CoroutineCloudFade>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 EnemyManager/<CoroutineCloudFade>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_firstTerm_0() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___firstTerm_0)); }
	inline float get_firstTerm_0() const { return ___firstTerm_0; }
	inline float* get_address_of_firstTerm_0() { return &___firstTerm_0; }
	inline void set_firstTerm_0(float value)
	{
		___firstTerm_0 = value;
	}

	inline static int32_t get_offset_of_cloud_1() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___cloud_1)); }
	inline Transform_t3600365921 * get_cloud_1() const { return ___cloud_1; }
	inline Transform_t3600365921 ** get_address_of_cloud_1() { return &___cloud_1; }
	inline void set_cloud_1(Transform_t3600365921 * value)
	{
		___cloud_1 = value;
		Il2CppCodeGenWriteBarrier((&___cloud_1), value);
	}

	inline static int32_t get_offset_of_tTerm_2() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___tTerm_2)); }
	inline float get_tTerm_2() const { return ___tTerm_2; }
	inline float* get_address_of_tTerm_2() { return &___tTerm_2; }
	inline void set_tTerm_2(float value)
	{
		___tTerm_2 = value;
	}

	inline static int32_t get_offset_of_U3CtWFSU3E__0_3() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U3CtWFSU3E__0_3)); }
	inline WaitForSeconds_t1699091251 * get_U3CtWFSU3E__0_3() const { return ___U3CtWFSU3E__0_3; }
	inline WaitForSeconds_t1699091251 ** get_address_of_U3CtWFSU3E__0_3() { return &___U3CtWFSU3E__0_3; }
	inline void set_U3CtWFSU3E__0_3(WaitForSeconds_t1699091251 * value)
	{
		___U3CtWFSU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtWFSU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CtempColorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U3CtempColorU3E__0_4)); }
	inline Color_t2555686324  get_U3CtempColorU3E__0_4() const { return ___U3CtempColorU3E__0_4; }
	inline Color_t2555686324 * get_address_of_U3CtempColorU3E__0_4() { return &___U3CtempColorU3E__0_4; }
	inline void set_U3CtempColorU3E__0_4(Color_t2555686324  value)
	{
		___U3CtempColorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_tmpColor_5() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___tmpColor_5)); }
	inline Color_t2555686324  get_tmpColor_5() const { return ___tmpColor_5; }
	inline Color_t2555686324 * get_address_of_tmpColor_5() { return &___tmpColor_5; }
	inline void set_tmpColor_5(Color_t2555686324  value)
	{
		___tmpColor_5 = value;
	}

	inline static int32_t get_offset_of_U3CindexU3E__1_6() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U3CindexU3E__1_6)); }
	inline float get_U3CindexU3E__1_6() const { return ___U3CindexU3E__1_6; }
	inline float* get_address_of_U3CindexU3E__1_6() { return &___U3CindexU3E__1_6; }
	inline void set_U3CindexU3E__1_6(float value)
	{
		___U3CindexU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U24this_7)); }
	inline EnemyManager_t913934216 * get_U24this_7() const { return ___U24this_7; }
	inline EnemyManager_t913934216 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(EnemyManager_t913934216 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOROUTINECLOUDFADEU3EC__ITERATOR0_T2338107655_H
#ifndef ASPECTMODE_T3417192999_H
#define ASPECTMODE_T3417192999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter/AspectMode
struct  AspectMode_t3417192999 
{
public:
	// System.Int32 UnityEngine.UI.AspectRatioFitter/AspectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectMode_t3417192999, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTMODE_T3417192999_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255365  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef CONSTRAINT_T814224393_H
#define CONSTRAINT_T814224393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Constraint
struct  Constraint_t814224393 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Constraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Constraint_t814224393, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T814224393_H
#ifndef FITMODE_T3267881214_H
#define FITMODE_T3267881214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter/FitMode
struct  FitMode_t3267881214 
{
public:
	// System.Int32 UnityEngine.UI.ContentSizeFitter/FitMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FitMode_t3267881214, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FITMODE_T3267881214_H
#ifndef SCREENMATCHMODE_T3675272090_H
#define SCREENMATCHMODE_T3675272090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/ScreenMatchMode
struct  ScreenMatchMode_t3675272090 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScreenMatchMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenMatchMode_t3675272090, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENMATCHMODE_T3675272090_H
#ifndef UNIT_T2218508340_H
#define UNIT_T2218508340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/Unit
struct  Unit_t2218508340 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/Unit::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Unit_t2218508340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T2218508340_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t2311174851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifndef RAYCAST3DCALLBACK_T701940803_H
#define RAYCAST3DCALLBACK_T701940803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t701940803  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T701940803_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#define GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t3841783507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifndef RAYCAST2DCALLBACK_T768590915_H
#define RAYCAST2DCALLBACK_T768590915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t768590915  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T768590915_H
#ifndef RAYCASTALLCALLBACK_T1884415901_H
#define RAYCASTALLCALLBACK_T1884415901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t1884415901  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T1884415901_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#define GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t3913627115  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef FIREEX_T4238753029_H
#define FIREEX_T4238753029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FireEX
struct  FireEX_t4238753029  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody2D FireEX::rb2d
	Rigidbody2D_t939494601 * ___rb2d_2;
	// System.Single FireEX::acceleration
	float ___acceleration_3;
	// System.Single FireEX::initialVelocity
	float ___initialVelocity_4;
	// System.Single FireEX::angle
	float ___angle_5;
	// System.Single FireEX::radians
	float ___radians_6;

public:
	inline static int32_t get_offset_of_rb2d_2() { return static_cast<int32_t>(offsetof(FireEX_t4238753029, ___rb2d_2)); }
	inline Rigidbody2D_t939494601 * get_rb2d_2() const { return ___rb2d_2; }
	inline Rigidbody2D_t939494601 ** get_address_of_rb2d_2() { return &___rb2d_2; }
	inline void set_rb2d_2(Rigidbody2D_t939494601 * value)
	{
		___rb2d_2 = value;
		Il2CppCodeGenWriteBarrier((&___rb2d_2), value);
	}

	inline static int32_t get_offset_of_acceleration_3() { return static_cast<int32_t>(offsetof(FireEX_t4238753029, ___acceleration_3)); }
	inline float get_acceleration_3() const { return ___acceleration_3; }
	inline float* get_address_of_acceleration_3() { return &___acceleration_3; }
	inline void set_acceleration_3(float value)
	{
		___acceleration_3 = value;
	}

	inline static int32_t get_offset_of_initialVelocity_4() { return static_cast<int32_t>(offsetof(FireEX_t4238753029, ___initialVelocity_4)); }
	inline float get_initialVelocity_4() const { return ___initialVelocity_4; }
	inline float* get_address_of_initialVelocity_4() { return &___initialVelocity_4; }
	inline void set_initialVelocity_4(float value)
	{
		___initialVelocity_4 = value;
	}

	inline static int32_t get_offset_of_angle_5() { return static_cast<int32_t>(offsetof(FireEX_t4238753029, ___angle_5)); }
	inline float get_angle_5() const { return ___angle_5; }
	inline float* get_address_of_angle_5() { return &___angle_5; }
	inline void set_angle_5(float value)
	{
		___angle_5 = value;
	}

	inline static int32_t get_offset_of_radians_6() { return static_cast<int32_t>(offsetof(FireEX_t4238753029, ___radians_6)); }
	inline float get_radians_6() const { return ___radians_6; }
	inline float* get_address_of_radians_6() { return &___radians_6; }
	inline void set_radians_6(float value)
	{
		___radians_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREEX_T4238753029_H
#ifndef CHARACTERMANAGER_T2849944402_H
#define CHARACTERMANAGER_T2849944402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterManager
struct  CharacterManager_t2849944402  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CharacterManager::isActive
	bool ___isActive_2;
	// System.Int32 CharacterManager::direction
	int32_t ___direction_3;
	// UnityEngine.GameObject CharacterManager::gameManager
	GameObject_t1113636619 * ___gameManager_4;
	// System.Single CharacterManager::characterSpeed
	float ___characterSpeed_5;
	// UnityEngine.GameObject CharacterManager::CharacterPosition
	GameObject_t1113636619 * ___CharacterPosition_6;
	// UnityEngine.Vector3 CharacterManager::StartingPos
	Vector3_t3722313464  ___StartingPos_7;
	// UnityEngine.Vector3 CharacterManager::YposMax
	Vector3_t3722313464  ___YposMax_8;
	// UnityEngine.Vector3 CharacterManager::YposMin
	Vector3_t3722313464  ___YposMin_9;
	// UnityEngine.GameObject CharacterManager::Shooter
	GameObject_t1113636619 * ___Shooter_10;

public:
	inline static int32_t get_offset_of_isActive_2() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___isActive_2)); }
	inline bool get_isActive_2() const { return ___isActive_2; }
	inline bool* get_address_of_isActive_2() { return &___isActive_2; }
	inline void set_isActive_2(bool value)
	{
		___isActive_2 = value;
	}

	inline static int32_t get_offset_of_direction_3() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___direction_3)); }
	inline int32_t get_direction_3() const { return ___direction_3; }
	inline int32_t* get_address_of_direction_3() { return &___direction_3; }
	inline void set_direction_3(int32_t value)
	{
		___direction_3 = value;
	}

	inline static int32_t get_offset_of_gameManager_4() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___gameManager_4)); }
	inline GameObject_t1113636619 * get_gameManager_4() const { return ___gameManager_4; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_4() { return &___gameManager_4; }
	inline void set_gameManager_4(GameObject_t1113636619 * value)
	{
		___gameManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_4), value);
	}

	inline static int32_t get_offset_of_characterSpeed_5() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___characterSpeed_5)); }
	inline float get_characterSpeed_5() const { return ___characterSpeed_5; }
	inline float* get_address_of_characterSpeed_5() { return &___characterSpeed_5; }
	inline void set_characterSpeed_5(float value)
	{
		___characterSpeed_5 = value;
	}

	inline static int32_t get_offset_of_CharacterPosition_6() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___CharacterPosition_6)); }
	inline GameObject_t1113636619 * get_CharacterPosition_6() const { return ___CharacterPosition_6; }
	inline GameObject_t1113636619 ** get_address_of_CharacterPosition_6() { return &___CharacterPosition_6; }
	inline void set_CharacterPosition_6(GameObject_t1113636619 * value)
	{
		___CharacterPosition_6 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterPosition_6), value);
	}

	inline static int32_t get_offset_of_StartingPos_7() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___StartingPos_7)); }
	inline Vector3_t3722313464  get_StartingPos_7() const { return ___StartingPos_7; }
	inline Vector3_t3722313464 * get_address_of_StartingPos_7() { return &___StartingPos_7; }
	inline void set_StartingPos_7(Vector3_t3722313464  value)
	{
		___StartingPos_7 = value;
	}

	inline static int32_t get_offset_of_YposMax_8() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___YposMax_8)); }
	inline Vector3_t3722313464  get_YposMax_8() const { return ___YposMax_8; }
	inline Vector3_t3722313464 * get_address_of_YposMax_8() { return &___YposMax_8; }
	inline void set_YposMax_8(Vector3_t3722313464  value)
	{
		___YposMax_8 = value;
	}

	inline static int32_t get_offset_of_YposMin_9() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___YposMin_9)); }
	inline Vector3_t3722313464  get_YposMin_9() const { return ___YposMin_9; }
	inline Vector3_t3722313464 * get_address_of_YposMin_9() { return &___YposMin_9; }
	inline void set_YposMin_9(Vector3_t3722313464  value)
	{
		___YposMin_9 = value;
	}

	inline static int32_t get_offset_of_Shooter_10() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___Shooter_10)); }
	inline GameObject_t1113636619 * get_Shooter_10() const { return ___Shooter_10; }
	inline GameObject_t1113636619 ** get_address_of_Shooter_10() { return &___Shooter_10; }
	inline void set_Shooter_10(GameObject_t1113636619 * value)
	{
		___Shooter_10 = value;
		Il2CppCodeGenWriteBarrier((&___Shooter_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERMANAGER_T2849944402_H
#ifndef BULLETMANAGER_T1336418627_H
#define BULLETMANAGER_T1336418627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BulletManager
struct  BulletManager_t1336418627  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] BulletManager::explosion
	SpriteU5BU5D_t2581906349* ___explosion_2;
	// UnityEngine.GameObject BulletManager::gameManager
	GameObject_t1113636619 * ___gameManager_3;
	// System.Boolean BulletManager::isActive
	bool ___isActive_4;
	// UnityEngine.Vector3 BulletManager::DIR
	Vector3_t3722313464  ___DIR_5;
	// System.Single BulletManager::acceleration
	float ___acceleration_6;
	// UnityEngine.GameObject BulletManager::Bullets
	GameObject_t1113636619 * ___Bullets_7;
	// System.Single BulletManager::mass
	float ___mass_8;
	// System.Single BulletManager::timer
	float ___timer_9;
	// System.Single BulletManager::t
	float ___t_10;

public:
	inline static int32_t get_offset_of_explosion_2() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___explosion_2)); }
	inline SpriteU5BU5D_t2581906349* get_explosion_2() const { return ___explosion_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of_explosion_2() { return &___explosion_2; }
	inline void set_explosion_2(SpriteU5BU5D_t2581906349* value)
	{
		___explosion_2 = value;
		Il2CppCodeGenWriteBarrier((&___explosion_2), value);
	}

	inline static int32_t get_offset_of_gameManager_3() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___gameManager_3)); }
	inline GameObject_t1113636619 * get_gameManager_3() const { return ___gameManager_3; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_3() { return &___gameManager_3; }
	inline void set_gameManager_3(GameObject_t1113636619 * value)
	{
		___gameManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_3), value);
	}

	inline static int32_t get_offset_of_isActive_4() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___isActive_4)); }
	inline bool get_isActive_4() const { return ___isActive_4; }
	inline bool* get_address_of_isActive_4() { return &___isActive_4; }
	inline void set_isActive_4(bool value)
	{
		___isActive_4 = value;
	}

	inline static int32_t get_offset_of_DIR_5() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___DIR_5)); }
	inline Vector3_t3722313464  get_DIR_5() const { return ___DIR_5; }
	inline Vector3_t3722313464 * get_address_of_DIR_5() { return &___DIR_5; }
	inline void set_DIR_5(Vector3_t3722313464  value)
	{
		___DIR_5 = value;
	}

	inline static int32_t get_offset_of_acceleration_6() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___acceleration_6)); }
	inline float get_acceleration_6() const { return ___acceleration_6; }
	inline float* get_address_of_acceleration_6() { return &___acceleration_6; }
	inline void set_acceleration_6(float value)
	{
		___acceleration_6 = value;
	}

	inline static int32_t get_offset_of_Bullets_7() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___Bullets_7)); }
	inline GameObject_t1113636619 * get_Bullets_7() const { return ___Bullets_7; }
	inline GameObject_t1113636619 ** get_address_of_Bullets_7() { return &___Bullets_7; }
	inline void set_Bullets_7(GameObject_t1113636619 * value)
	{
		___Bullets_7 = value;
		Il2CppCodeGenWriteBarrier((&___Bullets_7), value);
	}

	inline static int32_t get_offset_of_mass_8() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___mass_8)); }
	inline float get_mass_8() const { return ___mass_8; }
	inline float* get_address_of_mass_8() { return &___mass_8; }
	inline void set_mass_8(float value)
	{
		___mass_8 = value;
	}

	inline static int32_t get_offset_of_timer_9() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___timer_9)); }
	inline float get_timer_9() const { return ___timer_9; }
	inline float* get_address_of_timer_9() { return &___timer_9; }
	inline void set_timer_9(float value)
	{
		___timer_9 = value;
	}

	inline static int32_t get_offset_of_t_10() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___t_10)); }
	inline float get_t_10() const { return ___t_10; }
	inline float* get_address_of_t_10() { return &___t_10; }
	inline void set_t_10(float value)
	{
		___t_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLETMANAGER_T1336418627_H
#ifndef MESSAGEMANAGER_T336836297_H
#define MESSAGEMANAGER_T336836297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageManager
struct  MessageManager_t336836297  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MessageManager::gameManager
	GameObject_t1113636619 * ___gameManager_2;
	// UnityEngine.GameObject MessageManager::message
	GameObject_t1113636619 * ___message_3;
	// UnityEngine.Sprite[] MessageManager::countdown
	SpriteU5BU5D_t2581906349* ___countdown_4;
	// UnityEngine.Sprite MessageManager::pause
	Sprite_t280657092 * ___pause_5;

public:
	inline static int32_t get_offset_of_gameManager_2() { return static_cast<int32_t>(offsetof(MessageManager_t336836297, ___gameManager_2)); }
	inline GameObject_t1113636619 * get_gameManager_2() const { return ___gameManager_2; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_2() { return &___gameManager_2; }
	inline void set_gameManager_2(GameObject_t1113636619 * value)
	{
		___gameManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_2), value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(MessageManager_t336836297, ___message_3)); }
	inline GameObject_t1113636619 * get_message_3() const { return ___message_3; }
	inline GameObject_t1113636619 ** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(GameObject_t1113636619 * value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier((&___message_3), value);
	}

	inline static int32_t get_offset_of_countdown_4() { return static_cast<int32_t>(offsetof(MessageManager_t336836297, ___countdown_4)); }
	inline SpriteU5BU5D_t2581906349* get_countdown_4() const { return ___countdown_4; }
	inline SpriteU5BU5D_t2581906349** get_address_of_countdown_4() { return &___countdown_4; }
	inline void set_countdown_4(SpriteU5BU5D_t2581906349* value)
	{
		___countdown_4 = value;
		Il2CppCodeGenWriteBarrier((&___countdown_4), value);
	}

	inline static int32_t get_offset_of_pause_5() { return static_cast<int32_t>(offsetof(MessageManager_t336836297, ___pause_5)); }
	inline Sprite_t280657092 * get_pause_5() const { return ___pause_5; }
	inline Sprite_t280657092 ** get_address_of_pause_5() { return &___pause_5; }
	inline void set_pause_5(Sprite_t280657092 * value)
	{
		___pause_5 = value;
		Il2CppCodeGenWriteBarrier((&___pause_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEMANAGER_T336836297_H
#ifndef LINERENDERMANAGER_T120866003_H
#define LINERENDERMANAGER_T120866003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LineRenderManager
struct  LineRenderManager_t120866003  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.LineRenderer LineRenderManager::lineRenderer
	LineRenderer_t3154350270 * ___lineRenderer_2;
	// UnityEngine.Vector2 LineRenderManager::lineVector01
	Vector2_t2156229523  ___lineVector01_3;
	// UnityEngine.Vector2 LineRenderManager::lineVector02
	Vector2_t2156229523  ___lineVector02_4;
	// UnityEngine.GameObject LineRenderManager::Character
	GameObject_t1113636619 * ___Character_5;
	// UnityEngine.GameObject LineRenderManager::Missile
	GameObject_t1113636619 * ___Missile_6;
	// System.Single LineRenderManager::ShootAngle
	float ___ShootAngle_7;
	// System.Single LineRenderManager::radians
	float ___radians_8;
	// System.Int32 LineRenderManager::direction
	int32_t ___direction_9;
	// System.Single LineRenderManager::radius
	float ___radius_10;
	// UnityEngine.GameObject LineRenderManager::gameManager
	GameObject_t1113636619 * ___gameManager_11;
	// System.Single LineRenderManager::mySetTime
	float ___mySetTime_12;
	// System.Single LineRenderManager::myTimer
	float ___myTimer_13;
	// BulletGenerator LineRenderManager::bulletGenerator
	BulletGenerator_t1131765196 * ___bulletGenerator_14;
	// UnityEngine.Vector2 LineRenderManager::Point2
	Vector2_t2156229523  ___Point2_15;

public:
	inline static int32_t get_offset_of_lineRenderer_2() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___lineRenderer_2)); }
	inline LineRenderer_t3154350270 * get_lineRenderer_2() const { return ___lineRenderer_2; }
	inline LineRenderer_t3154350270 ** get_address_of_lineRenderer_2() { return &___lineRenderer_2; }
	inline void set_lineRenderer_2(LineRenderer_t3154350270 * value)
	{
		___lineRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_2), value);
	}

	inline static int32_t get_offset_of_lineVector01_3() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___lineVector01_3)); }
	inline Vector2_t2156229523  get_lineVector01_3() const { return ___lineVector01_3; }
	inline Vector2_t2156229523 * get_address_of_lineVector01_3() { return &___lineVector01_3; }
	inline void set_lineVector01_3(Vector2_t2156229523  value)
	{
		___lineVector01_3 = value;
	}

	inline static int32_t get_offset_of_lineVector02_4() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___lineVector02_4)); }
	inline Vector2_t2156229523  get_lineVector02_4() const { return ___lineVector02_4; }
	inline Vector2_t2156229523 * get_address_of_lineVector02_4() { return &___lineVector02_4; }
	inline void set_lineVector02_4(Vector2_t2156229523  value)
	{
		___lineVector02_4 = value;
	}

	inline static int32_t get_offset_of_Character_5() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___Character_5)); }
	inline GameObject_t1113636619 * get_Character_5() const { return ___Character_5; }
	inline GameObject_t1113636619 ** get_address_of_Character_5() { return &___Character_5; }
	inline void set_Character_5(GameObject_t1113636619 * value)
	{
		___Character_5 = value;
		Il2CppCodeGenWriteBarrier((&___Character_5), value);
	}

	inline static int32_t get_offset_of_Missile_6() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___Missile_6)); }
	inline GameObject_t1113636619 * get_Missile_6() const { return ___Missile_6; }
	inline GameObject_t1113636619 ** get_address_of_Missile_6() { return &___Missile_6; }
	inline void set_Missile_6(GameObject_t1113636619 * value)
	{
		___Missile_6 = value;
		Il2CppCodeGenWriteBarrier((&___Missile_6), value);
	}

	inline static int32_t get_offset_of_ShootAngle_7() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___ShootAngle_7)); }
	inline float get_ShootAngle_7() const { return ___ShootAngle_7; }
	inline float* get_address_of_ShootAngle_7() { return &___ShootAngle_7; }
	inline void set_ShootAngle_7(float value)
	{
		___ShootAngle_7 = value;
	}

	inline static int32_t get_offset_of_radians_8() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___radians_8)); }
	inline float get_radians_8() const { return ___radians_8; }
	inline float* get_address_of_radians_8() { return &___radians_8; }
	inline void set_radians_8(float value)
	{
		___radians_8 = value;
	}

	inline static int32_t get_offset_of_direction_9() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___direction_9)); }
	inline int32_t get_direction_9() const { return ___direction_9; }
	inline int32_t* get_address_of_direction_9() { return &___direction_9; }
	inline void set_direction_9(int32_t value)
	{
		___direction_9 = value;
	}

	inline static int32_t get_offset_of_radius_10() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___radius_10)); }
	inline float get_radius_10() const { return ___radius_10; }
	inline float* get_address_of_radius_10() { return &___radius_10; }
	inline void set_radius_10(float value)
	{
		___radius_10 = value;
	}

	inline static int32_t get_offset_of_gameManager_11() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___gameManager_11)); }
	inline GameObject_t1113636619 * get_gameManager_11() const { return ___gameManager_11; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_11() { return &___gameManager_11; }
	inline void set_gameManager_11(GameObject_t1113636619 * value)
	{
		___gameManager_11 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_11), value);
	}

	inline static int32_t get_offset_of_mySetTime_12() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___mySetTime_12)); }
	inline float get_mySetTime_12() const { return ___mySetTime_12; }
	inline float* get_address_of_mySetTime_12() { return &___mySetTime_12; }
	inline void set_mySetTime_12(float value)
	{
		___mySetTime_12 = value;
	}

	inline static int32_t get_offset_of_myTimer_13() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___myTimer_13)); }
	inline float get_myTimer_13() const { return ___myTimer_13; }
	inline float* get_address_of_myTimer_13() { return &___myTimer_13; }
	inline void set_myTimer_13(float value)
	{
		___myTimer_13 = value;
	}

	inline static int32_t get_offset_of_bulletGenerator_14() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___bulletGenerator_14)); }
	inline BulletGenerator_t1131765196 * get_bulletGenerator_14() const { return ___bulletGenerator_14; }
	inline BulletGenerator_t1131765196 ** get_address_of_bulletGenerator_14() { return &___bulletGenerator_14; }
	inline void set_bulletGenerator_14(BulletGenerator_t1131765196 * value)
	{
		___bulletGenerator_14 = value;
		Il2CppCodeGenWriteBarrier((&___bulletGenerator_14), value);
	}

	inline static int32_t get_offset_of_Point2_15() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___Point2_15)); }
	inline Vector2_t2156229523  get_Point2_15() const { return ___Point2_15; }
	inline Vector2_t2156229523 * get_address_of_Point2_15() { return &___Point2_15; }
	inline void set_Point2_15(Vector2_t2156229523  value)
	{
		___Point2_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINERENDERMANAGER_T120866003_H
#ifndef GAMEMANAGER_T1536523654_H
#define GAMEMANAGER_T1536523654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t1536523654  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GameManager::isGameOver
	bool ___isGameOver_2;
	// System.Boolean GameManager::enemyVictory
	bool ___enemyVictory_3;
	// System.Boolean GameManager::isPaused
	bool ___isPaused_4;
	// System.Boolean GameManager::isPlay
	bool ___isPlay_5;
	// System.Int32 GameManager::score
	int32_t ___score_6;
	// System.Int32 GameManager::highscore
	int32_t ___highscore_7;
	// UnityEngine.UI.Text GameManager::highscoreText
	Text_t1901882714 * ___highscoreText_8;
	// System.String GameManager::highScoreKey
	String_t* ___highScoreKey_9;
	// UnityEngine.GameObject GameManager::Character
	GameObject_t1113636619 * ___Character_10;
	// UnityEngine.Vector3 GameManager::StartingPos
	Vector3_t3722313464  ___StartingPos_11;
	// UnityEngine.UI.Text GameManager::scoreText
	Text_t1901882714 * ___scoreText_12;

public:
	inline static int32_t get_offset_of_isGameOver_2() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___isGameOver_2)); }
	inline bool get_isGameOver_2() const { return ___isGameOver_2; }
	inline bool* get_address_of_isGameOver_2() { return &___isGameOver_2; }
	inline void set_isGameOver_2(bool value)
	{
		___isGameOver_2 = value;
	}

	inline static int32_t get_offset_of_enemyVictory_3() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___enemyVictory_3)); }
	inline bool get_enemyVictory_3() const { return ___enemyVictory_3; }
	inline bool* get_address_of_enemyVictory_3() { return &___enemyVictory_3; }
	inline void set_enemyVictory_3(bool value)
	{
		___enemyVictory_3 = value;
	}

	inline static int32_t get_offset_of_isPaused_4() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___isPaused_4)); }
	inline bool get_isPaused_4() const { return ___isPaused_4; }
	inline bool* get_address_of_isPaused_4() { return &___isPaused_4; }
	inline void set_isPaused_4(bool value)
	{
		___isPaused_4 = value;
	}

	inline static int32_t get_offset_of_isPlay_5() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___isPlay_5)); }
	inline bool get_isPlay_5() const { return ___isPlay_5; }
	inline bool* get_address_of_isPlay_5() { return &___isPlay_5; }
	inline void set_isPlay_5(bool value)
	{
		___isPlay_5 = value;
	}

	inline static int32_t get_offset_of_score_6() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___score_6)); }
	inline int32_t get_score_6() const { return ___score_6; }
	inline int32_t* get_address_of_score_6() { return &___score_6; }
	inline void set_score_6(int32_t value)
	{
		___score_6 = value;
	}

	inline static int32_t get_offset_of_highscore_7() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___highscore_7)); }
	inline int32_t get_highscore_7() const { return ___highscore_7; }
	inline int32_t* get_address_of_highscore_7() { return &___highscore_7; }
	inline void set_highscore_7(int32_t value)
	{
		___highscore_7 = value;
	}

	inline static int32_t get_offset_of_highscoreText_8() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___highscoreText_8)); }
	inline Text_t1901882714 * get_highscoreText_8() const { return ___highscoreText_8; }
	inline Text_t1901882714 ** get_address_of_highscoreText_8() { return &___highscoreText_8; }
	inline void set_highscoreText_8(Text_t1901882714 * value)
	{
		___highscoreText_8 = value;
		Il2CppCodeGenWriteBarrier((&___highscoreText_8), value);
	}

	inline static int32_t get_offset_of_highScoreKey_9() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___highScoreKey_9)); }
	inline String_t* get_highScoreKey_9() const { return ___highScoreKey_9; }
	inline String_t** get_address_of_highScoreKey_9() { return &___highScoreKey_9; }
	inline void set_highScoreKey_9(String_t* value)
	{
		___highScoreKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___highScoreKey_9), value);
	}

	inline static int32_t get_offset_of_Character_10() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Character_10)); }
	inline GameObject_t1113636619 * get_Character_10() const { return ___Character_10; }
	inline GameObject_t1113636619 ** get_address_of_Character_10() { return &___Character_10; }
	inline void set_Character_10(GameObject_t1113636619 * value)
	{
		___Character_10 = value;
		Il2CppCodeGenWriteBarrier((&___Character_10), value);
	}

	inline static int32_t get_offset_of_StartingPos_11() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___StartingPos_11)); }
	inline Vector3_t3722313464  get_StartingPos_11() const { return ___StartingPos_11; }
	inline Vector3_t3722313464 * get_address_of_StartingPos_11() { return &___StartingPos_11; }
	inline void set_StartingPos_11(Vector3_t3722313464  value)
	{
		___StartingPos_11 = value;
	}

	inline static int32_t get_offset_of_scoreText_12() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___scoreText_12)); }
	inline Text_t1901882714 * get_scoreText_12() const { return ___scoreText_12; }
	inline Text_t1901882714 ** get_address_of_scoreText_12() { return &___scoreText_12; }
	inline void set_scoreText_12(Text_t1901882714 * value)
	{
		___scoreText_12 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T1536523654_H
#ifndef ENEMYMANAGER_T913934216_H
#define ENEMYMANAGER_T913934216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyManager
struct  EnemyManager_t913934216  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] EnemyManager::dissipate
	SpriteU5BU5D_t2581906349* ___dissipate_2;
	// System.Boolean EnemyManager::isActive
	bool ___isActive_3;
	// System.Boolean EnemyManager::isHit
	bool ___isHit_4;
	// System.Boolean EnemyManager::isCloudeFade
	bool ___isCloudeFade_5;
	// System.Single EnemyManager::EnemySpeed
	float ___EnemySpeed_6;
	// UnityEngine.GameObject EnemyManager::Bullets
	GameObject_t1113636619 * ___Bullets_7;
	// UnityEngine.Vector3 EnemyManager::fallDir
	Vector3_t3722313464  ___fallDir_8;
	// UnityEngine.Vector3 EnemyManager::EnemyPos1
	Vector3_t3722313464  ___EnemyPos1_9;
	// UnityEngine.Vector3 EnemyManager::EnemyPos2
	Vector3_t3722313464  ___EnemyPos2_10;
	// UnityEngine.Vector3 EnemyManager::EnemyPos3
	Vector3_t3722313464  ___EnemyPos3_11;
	// UnityEngine.Vector3 EnemyManager::posDiff
	Vector3_t3722313464  ___posDiff_12;
	// System.Int32 EnemyManager::dir
	int32_t ___dir_13;
	// System.Single EnemyManager::acceleration
	float ___acceleration_14;
	// System.Single EnemyManager::mass
	float ___mass_15;
	// UnityEngine.GameObject EnemyManager::gameManager
	GameObject_t1113636619 * ___gameManager_16;

public:
	inline static int32_t get_offset_of_dissipate_2() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___dissipate_2)); }
	inline SpriteU5BU5D_t2581906349* get_dissipate_2() const { return ___dissipate_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of_dissipate_2() { return &___dissipate_2; }
	inline void set_dissipate_2(SpriteU5BU5D_t2581906349* value)
	{
		___dissipate_2 = value;
		Il2CppCodeGenWriteBarrier((&___dissipate_2), value);
	}

	inline static int32_t get_offset_of_isActive_3() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___isActive_3)); }
	inline bool get_isActive_3() const { return ___isActive_3; }
	inline bool* get_address_of_isActive_3() { return &___isActive_3; }
	inline void set_isActive_3(bool value)
	{
		___isActive_3 = value;
	}

	inline static int32_t get_offset_of_isHit_4() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___isHit_4)); }
	inline bool get_isHit_4() const { return ___isHit_4; }
	inline bool* get_address_of_isHit_4() { return &___isHit_4; }
	inline void set_isHit_4(bool value)
	{
		___isHit_4 = value;
	}

	inline static int32_t get_offset_of_isCloudeFade_5() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___isCloudeFade_5)); }
	inline bool get_isCloudeFade_5() const { return ___isCloudeFade_5; }
	inline bool* get_address_of_isCloudeFade_5() { return &___isCloudeFade_5; }
	inline void set_isCloudeFade_5(bool value)
	{
		___isCloudeFade_5 = value;
	}

	inline static int32_t get_offset_of_EnemySpeed_6() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___EnemySpeed_6)); }
	inline float get_EnemySpeed_6() const { return ___EnemySpeed_6; }
	inline float* get_address_of_EnemySpeed_6() { return &___EnemySpeed_6; }
	inline void set_EnemySpeed_6(float value)
	{
		___EnemySpeed_6 = value;
	}

	inline static int32_t get_offset_of_Bullets_7() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___Bullets_7)); }
	inline GameObject_t1113636619 * get_Bullets_7() const { return ___Bullets_7; }
	inline GameObject_t1113636619 ** get_address_of_Bullets_7() { return &___Bullets_7; }
	inline void set_Bullets_7(GameObject_t1113636619 * value)
	{
		___Bullets_7 = value;
		Il2CppCodeGenWriteBarrier((&___Bullets_7), value);
	}

	inline static int32_t get_offset_of_fallDir_8() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___fallDir_8)); }
	inline Vector3_t3722313464  get_fallDir_8() const { return ___fallDir_8; }
	inline Vector3_t3722313464 * get_address_of_fallDir_8() { return &___fallDir_8; }
	inline void set_fallDir_8(Vector3_t3722313464  value)
	{
		___fallDir_8 = value;
	}

	inline static int32_t get_offset_of_EnemyPos1_9() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___EnemyPos1_9)); }
	inline Vector3_t3722313464  get_EnemyPos1_9() const { return ___EnemyPos1_9; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos1_9() { return &___EnemyPos1_9; }
	inline void set_EnemyPos1_9(Vector3_t3722313464  value)
	{
		___EnemyPos1_9 = value;
	}

	inline static int32_t get_offset_of_EnemyPos2_10() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___EnemyPos2_10)); }
	inline Vector3_t3722313464  get_EnemyPos2_10() const { return ___EnemyPos2_10; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos2_10() { return &___EnemyPos2_10; }
	inline void set_EnemyPos2_10(Vector3_t3722313464  value)
	{
		___EnemyPos2_10 = value;
	}

	inline static int32_t get_offset_of_EnemyPos3_11() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___EnemyPos3_11)); }
	inline Vector3_t3722313464  get_EnemyPos3_11() const { return ___EnemyPos3_11; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos3_11() { return &___EnemyPos3_11; }
	inline void set_EnemyPos3_11(Vector3_t3722313464  value)
	{
		___EnemyPos3_11 = value;
	}

	inline static int32_t get_offset_of_posDiff_12() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___posDiff_12)); }
	inline Vector3_t3722313464  get_posDiff_12() const { return ___posDiff_12; }
	inline Vector3_t3722313464 * get_address_of_posDiff_12() { return &___posDiff_12; }
	inline void set_posDiff_12(Vector3_t3722313464  value)
	{
		___posDiff_12 = value;
	}

	inline static int32_t get_offset_of_dir_13() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___dir_13)); }
	inline int32_t get_dir_13() const { return ___dir_13; }
	inline int32_t* get_address_of_dir_13() { return &___dir_13; }
	inline void set_dir_13(int32_t value)
	{
		___dir_13 = value;
	}

	inline static int32_t get_offset_of_acceleration_14() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___acceleration_14)); }
	inline float get_acceleration_14() const { return ___acceleration_14; }
	inline float* get_address_of_acceleration_14() { return &___acceleration_14; }
	inline void set_acceleration_14(float value)
	{
		___acceleration_14 = value;
	}

	inline static int32_t get_offset_of_mass_15() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___mass_15)); }
	inline float get_mass_15() const { return ___mass_15; }
	inline float* get_address_of_mass_15() { return &___mass_15; }
	inline void set_mass_15(float value)
	{
		___mass_15 = value;
	}

	inline static int32_t get_offset_of_gameManager_16() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___gameManager_16)); }
	inline GameObject_t1113636619 * get_gameManager_16() const { return ___gameManager_16; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_16() { return &___gameManager_16; }
	inline void set_gameManager_16(GameObject_t1113636619 * value)
	{
		___gameManager_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYMANAGER_T913934216_H
#ifndef ENEMYGENERATOR_T2446750330_H
#define ENEMYGENERATOR_T2446750330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyGenerator
struct  EnemyGenerator_t2446750330  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] EnemyGenerator::enemySprite
	SpriteU5BU5D_t2581906349* ___enemySprite_2;
	// UnityEngine.GameObject EnemyGenerator::gameManager
	GameObject_t1113636619 * ___gameManager_3;
	// UnityEngine.GameObject EnemyGenerator::oldEnemy
	GameObject_t1113636619 * ___oldEnemy_4;
	// UnityEngine.GameObject EnemyGenerator::newEnemy
	GameObject_t1113636619 * ___newEnemy_5;
	// UnityEngine.GameObject EnemyGenerator::Bullets
	GameObject_t1113636619 * ___Bullets_6;
	// UnityEngine.Vector3 EnemyGenerator::Position
	Vector3_t3722313464  ___Position_7;
	// System.Single EnemyGenerator::spawnTerm
	float ___spawnTerm_8;
	// System.Int32 EnemyGenerator::KillCount
	int32_t ___KillCount_9;

public:
	inline static int32_t get_offset_of_enemySprite_2() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___enemySprite_2)); }
	inline SpriteU5BU5D_t2581906349* get_enemySprite_2() const { return ___enemySprite_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of_enemySprite_2() { return &___enemySprite_2; }
	inline void set_enemySprite_2(SpriteU5BU5D_t2581906349* value)
	{
		___enemySprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___enemySprite_2), value);
	}

	inline static int32_t get_offset_of_gameManager_3() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___gameManager_3)); }
	inline GameObject_t1113636619 * get_gameManager_3() const { return ___gameManager_3; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_3() { return &___gameManager_3; }
	inline void set_gameManager_3(GameObject_t1113636619 * value)
	{
		___gameManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_3), value);
	}

	inline static int32_t get_offset_of_oldEnemy_4() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___oldEnemy_4)); }
	inline GameObject_t1113636619 * get_oldEnemy_4() const { return ___oldEnemy_4; }
	inline GameObject_t1113636619 ** get_address_of_oldEnemy_4() { return &___oldEnemy_4; }
	inline void set_oldEnemy_4(GameObject_t1113636619 * value)
	{
		___oldEnemy_4 = value;
		Il2CppCodeGenWriteBarrier((&___oldEnemy_4), value);
	}

	inline static int32_t get_offset_of_newEnemy_5() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___newEnemy_5)); }
	inline GameObject_t1113636619 * get_newEnemy_5() const { return ___newEnemy_5; }
	inline GameObject_t1113636619 ** get_address_of_newEnemy_5() { return &___newEnemy_5; }
	inline void set_newEnemy_5(GameObject_t1113636619 * value)
	{
		___newEnemy_5 = value;
		Il2CppCodeGenWriteBarrier((&___newEnemy_5), value);
	}

	inline static int32_t get_offset_of_Bullets_6() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___Bullets_6)); }
	inline GameObject_t1113636619 * get_Bullets_6() const { return ___Bullets_6; }
	inline GameObject_t1113636619 ** get_address_of_Bullets_6() { return &___Bullets_6; }
	inline void set_Bullets_6(GameObject_t1113636619 * value)
	{
		___Bullets_6 = value;
		Il2CppCodeGenWriteBarrier((&___Bullets_6), value);
	}

	inline static int32_t get_offset_of_Position_7() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___Position_7)); }
	inline Vector3_t3722313464  get_Position_7() const { return ___Position_7; }
	inline Vector3_t3722313464 * get_address_of_Position_7() { return &___Position_7; }
	inline void set_Position_7(Vector3_t3722313464  value)
	{
		___Position_7 = value;
	}

	inline static int32_t get_offset_of_spawnTerm_8() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___spawnTerm_8)); }
	inline float get_spawnTerm_8() const { return ___spawnTerm_8; }
	inline float* get_address_of_spawnTerm_8() { return &___spawnTerm_8; }
	inline void set_spawnTerm_8(float value)
	{
		___spawnTerm_8 = value;
	}

	inline static int32_t get_offset_of_KillCount_9() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___KillCount_9)); }
	inline int32_t get_KillCount_9() const { return ___KillCount_9; }
	inline int32_t* get_address_of_KillCount_9() { return &___KillCount_9; }
	inline void set_KillCount_9(int32_t value)
	{
		___KillCount_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYGENERATOR_T2446750330_H
#ifndef BUTTONMANAGER_T2018100028_H
#define BUTTONMANAGER_T2018100028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonManager
struct  ButtonManager_t2018100028  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ButtonManager::Character
	GameObject_t1113636619 * ___Character_2;

public:
	inline static int32_t get_offset_of_Character_2() { return static_cast<int32_t>(offsetof(ButtonManager_t2018100028, ___Character_2)); }
	inline GameObject_t1113636619 * get_Character_2() const { return ___Character_2; }
	inline GameObject_t1113636619 ** get_address_of_Character_2() { return &___Character_2; }
	inline void set_Character_2(GameObject_t1113636619 * value)
	{
		___Character_2 = value;
		Il2CppCodeGenWriteBarrier((&___Character_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONMANAGER_T2018100028_H
#ifndef BULLETGENERATOR_T1131765196_H
#define BULLETGENERATOR_T1131765196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BulletGenerator
struct  BulletGenerator_t1131765196  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BulletGenerator::oldMissile
	GameObject_t1113636619 * ___oldMissile_2;
	// UnityEngine.GameObject BulletGenerator::lineRenderer
	GameObject_t1113636619 * ___lineRenderer_3;
	// UnityEngine.GameObject BulletGenerator::newMissile
	GameObject_t1113636619 * ___newMissile_4;
	// UnityEngine.GameObject BulletGenerator::Bullets
	GameObject_t1113636619 * ___Bullets_5;
	// UnityEngine.GameObject BulletGenerator::gameManager
	GameObject_t1113636619 * ___gameManager_6;
	// System.Single BulletGenerator::timeLeft
	float ___timeLeft_7;
	// System.Single BulletGenerator::initialVelocity
	float ___initialVelocity_8;
	// System.Single BulletGenerator::power
	float ___power_9;
	// System.Single BulletGenerator::acceleration
	float ___acceleration_10;
	// UnityEngine.UI.Text BulletGenerator::LocText
	Text_t1901882714 * ___LocText_11;
	// UnityEngine.UI.Text BulletGenerator::DirText
	Text_t1901882714 * ___DirText_12;
	// UnityEngine.UI.Text BulletGenerator::AngText
	Text_t1901882714 * ___AngText_13;
	// UnityEngine.UI.Text BulletGenerator::TimeText
	Text_t1901882714 * ___TimeText_14;
	// UnityEngine.Vector3 BulletGenerator::direction
	Vector3_t3722313464  ___direction_15;
	// System.Boolean BulletGenerator::missileExist
	bool ___missileExist_16;

public:
	inline static int32_t get_offset_of_oldMissile_2() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___oldMissile_2)); }
	inline GameObject_t1113636619 * get_oldMissile_2() const { return ___oldMissile_2; }
	inline GameObject_t1113636619 ** get_address_of_oldMissile_2() { return &___oldMissile_2; }
	inline void set_oldMissile_2(GameObject_t1113636619 * value)
	{
		___oldMissile_2 = value;
		Il2CppCodeGenWriteBarrier((&___oldMissile_2), value);
	}

	inline static int32_t get_offset_of_lineRenderer_3() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___lineRenderer_3)); }
	inline GameObject_t1113636619 * get_lineRenderer_3() const { return ___lineRenderer_3; }
	inline GameObject_t1113636619 ** get_address_of_lineRenderer_3() { return &___lineRenderer_3; }
	inline void set_lineRenderer_3(GameObject_t1113636619 * value)
	{
		___lineRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_3), value);
	}

	inline static int32_t get_offset_of_newMissile_4() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___newMissile_4)); }
	inline GameObject_t1113636619 * get_newMissile_4() const { return ___newMissile_4; }
	inline GameObject_t1113636619 ** get_address_of_newMissile_4() { return &___newMissile_4; }
	inline void set_newMissile_4(GameObject_t1113636619 * value)
	{
		___newMissile_4 = value;
		Il2CppCodeGenWriteBarrier((&___newMissile_4), value);
	}

	inline static int32_t get_offset_of_Bullets_5() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___Bullets_5)); }
	inline GameObject_t1113636619 * get_Bullets_5() const { return ___Bullets_5; }
	inline GameObject_t1113636619 ** get_address_of_Bullets_5() { return &___Bullets_5; }
	inline void set_Bullets_5(GameObject_t1113636619 * value)
	{
		___Bullets_5 = value;
		Il2CppCodeGenWriteBarrier((&___Bullets_5), value);
	}

	inline static int32_t get_offset_of_gameManager_6() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___gameManager_6)); }
	inline GameObject_t1113636619 * get_gameManager_6() const { return ___gameManager_6; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_6() { return &___gameManager_6; }
	inline void set_gameManager_6(GameObject_t1113636619 * value)
	{
		___gameManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_6), value);
	}

	inline static int32_t get_offset_of_timeLeft_7() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___timeLeft_7)); }
	inline float get_timeLeft_7() const { return ___timeLeft_7; }
	inline float* get_address_of_timeLeft_7() { return &___timeLeft_7; }
	inline void set_timeLeft_7(float value)
	{
		___timeLeft_7 = value;
	}

	inline static int32_t get_offset_of_initialVelocity_8() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___initialVelocity_8)); }
	inline float get_initialVelocity_8() const { return ___initialVelocity_8; }
	inline float* get_address_of_initialVelocity_8() { return &___initialVelocity_8; }
	inline void set_initialVelocity_8(float value)
	{
		___initialVelocity_8 = value;
	}

	inline static int32_t get_offset_of_power_9() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___power_9)); }
	inline float get_power_9() const { return ___power_9; }
	inline float* get_address_of_power_9() { return &___power_9; }
	inline void set_power_9(float value)
	{
		___power_9 = value;
	}

	inline static int32_t get_offset_of_acceleration_10() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___acceleration_10)); }
	inline float get_acceleration_10() const { return ___acceleration_10; }
	inline float* get_address_of_acceleration_10() { return &___acceleration_10; }
	inline void set_acceleration_10(float value)
	{
		___acceleration_10 = value;
	}

	inline static int32_t get_offset_of_LocText_11() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___LocText_11)); }
	inline Text_t1901882714 * get_LocText_11() const { return ___LocText_11; }
	inline Text_t1901882714 ** get_address_of_LocText_11() { return &___LocText_11; }
	inline void set_LocText_11(Text_t1901882714 * value)
	{
		___LocText_11 = value;
		Il2CppCodeGenWriteBarrier((&___LocText_11), value);
	}

	inline static int32_t get_offset_of_DirText_12() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___DirText_12)); }
	inline Text_t1901882714 * get_DirText_12() const { return ___DirText_12; }
	inline Text_t1901882714 ** get_address_of_DirText_12() { return &___DirText_12; }
	inline void set_DirText_12(Text_t1901882714 * value)
	{
		___DirText_12 = value;
		Il2CppCodeGenWriteBarrier((&___DirText_12), value);
	}

	inline static int32_t get_offset_of_AngText_13() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___AngText_13)); }
	inline Text_t1901882714 * get_AngText_13() const { return ___AngText_13; }
	inline Text_t1901882714 ** get_address_of_AngText_13() { return &___AngText_13; }
	inline void set_AngText_13(Text_t1901882714 * value)
	{
		___AngText_13 = value;
		Il2CppCodeGenWriteBarrier((&___AngText_13), value);
	}

	inline static int32_t get_offset_of_TimeText_14() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___TimeText_14)); }
	inline Text_t1901882714 * get_TimeText_14() const { return ___TimeText_14; }
	inline Text_t1901882714 ** get_address_of_TimeText_14() { return &___TimeText_14; }
	inline void set_TimeText_14(Text_t1901882714 * value)
	{
		___TimeText_14 = value;
		Il2CppCodeGenWriteBarrier((&___TimeText_14), value);
	}

	inline static int32_t get_offset_of_direction_15() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___direction_15)); }
	inline Vector3_t3722313464  get_direction_15() const { return ___direction_15; }
	inline Vector3_t3722313464 * get_address_of_direction_15() { return &___direction_15; }
	inline void set_direction_15(Vector3_t3722313464  value)
	{
		___direction_15 = value;
	}

	inline static int32_t get_offset_of_missileExist_16() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___missileExist_16)); }
	inline bool get_missileExist_16() const { return ___missileExist_16; }
	inline bool* get_address_of_missileExist_16() { return &___missileExist_16; }
	inline void set_missileExist_16(bool value)
	{
		___missileExist_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLETGENERATOR_T1131765196_H
#ifndef MOVEMANAGER_T918348315_H
#define MOVEMANAGER_T918348315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveManager
struct  MoveManager_t918348315  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoveManager::EnemySpeed
	float ___EnemySpeed_2;
	// System.Int32 MoveManager::dir
	int32_t ___dir_3;
	// UnityEngine.Vector3 MoveManager::EnemyPos1
	Vector3_t3722313464  ___EnemyPos1_4;
	// UnityEngine.Vector3 MoveManager::EnemyPos2
	Vector3_t3722313464  ___EnemyPos2_5;
	// UnityEngine.Vector3 MoveManager::EnemyPos3
	Vector3_t3722313464  ___EnemyPos3_6;
	// UnityEngine.Vector3 MoveManager::returnSpot
	Vector3_t3722313464  ___returnSpot_7;
	// UnityEngine.Vector3 MoveManager::posDiff
	Vector3_t3722313464  ___posDiff_8;

public:
	inline static int32_t get_offset_of_EnemySpeed_2() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___EnemySpeed_2)); }
	inline float get_EnemySpeed_2() const { return ___EnemySpeed_2; }
	inline float* get_address_of_EnemySpeed_2() { return &___EnemySpeed_2; }
	inline void set_EnemySpeed_2(float value)
	{
		___EnemySpeed_2 = value;
	}

	inline static int32_t get_offset_of_dir_3() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___dir_3)); }
	inline int32_t get_dir_3() const { return ___dir_3; }
	inline int32_t* get_address_of_dir_3() { return &___dir_3; }
	inline void set_dir_3(int32_t value)
	{
		___dir_3 = value;
	}

	inline static int32_t get_offset_of_EnemyPos1_4() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___EnemyPos1_4)); }
	inline Vector3_t3722313464  get_EnemyPos1_4() const { return ___EnemyPos1_4; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos1_4() { return &___EnemyPos1_4; }
	inline void set_EnemyPos1_4(Vector3_t3722313464  value)
	{
		___EnemyPos1_4 = value;
	}

	inline static int32_t get_offset_of_EnemyPos2_5() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___EnemyPos2_5)); }
	inline Vector3_t3722313464  get_EnemyPos2_5() const { return ___EnemyPos2_5; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos2_5() { return &___EnemyPos2_5; }
	inline void set_EnemyPos2_5(Vector3_t3722313464  value)
	{
		___EnemyPos2_5 = value;
	}

	inline static int32_t get_offset_of_EnemyPos3_6() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___EnemyPos3_6)); }
	inline Vector3_t3722313464  get_EnemyPos3_6() const { return ___EnemyPos3_6; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos3_6() { return &___EnemyPos3_6; }
	inline void set_EnemyPos3_6(Vector3_t3722313464  value)
	{
		___EnemyPos3_6 = value;
	}

	inline static int32_t get_offset_of_returnSpot_7() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___returnSpot_7)); }
	inline Vector3_t3722313464  get_returnSpot_7() const { return ___returnSpot_7; }
	inline Vector3_t3722313464 * get_address_of_returnSpot_7() { return &___returnSpot_7; }
	inline void set_returnSpot_7(Vector3_t3722313464  value)
	{
		___returnSpot_7 = value;
	}

	inline static int32_t get_offset_of_posDiff_8() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___posDiff_8)); }
	inline Vector3_t3722313464  get_posDiff_8() const { return ___posDiff_8; }
	inline Vector3_t3722313464 * get_address_of_posDiff_8() { return &___posDiff_8; }
	inline void set_posDiff_8(Vector3_t3722313464  value)
	{
		___posDiff_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMANAGER_T918348315_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_2)); }
	inline Graphic_t1660335611 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t1660335611 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef ASPECTRATIOFITTER_T3312407083_H
#define ASPECTRATIOFITTER_T3312407083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter
struct  AspectRatioFitter_t3312407083  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::m_AspectMode
	int32_t ___m_AspectMode_2;
	// System.Single UnityEngine.UI.AspectRatioFitter::m_AspectRatio
	float ___m_AspectRatio_3;
	// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::m_Rect
	RectTransform_t3704657025 * ___m_Rect_4;
	// System.Boolean UnityEngine.UI.AspectRatioFitter::m_DelayedSetDirty
	bool ___m_DelayedSetDirty_5;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.AspectRatioFitter::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_6;

public:
	inline static int32_t get_offset_of_m_AspectMode_2() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_AspectMode_2)); }
	inline int32_t get_m_AspectMode_2() const { return ___m_AspectMode_2; }
	inline int32_t* get_address_of_m_AspectMode_2() { return &___m_AspectMode_2; }
	inline void set_m_AspectMode_2(int32_t value)
	{
		___m_AspectMode_2 = value;
	}

	inline static int32_t get_offset_of_m_AspectRatio_3() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_AspectRatio_3)); }
	inline float get_m_AspectRatio_3() const { return ___m_AspectRatio_3; }
	inline float* get_address_of_m_AspectRatio_3() { return &___m_AspectRatio_3; }
	inline void set_m_AspectRatio_3(float value)
	{
		___m_AspectRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_Rect_4)); }
	inline RectTransform_t3704657025 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3704657025 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_DelayedSetDirty_5() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_DelayedSetDirty_5)); }
	inline bool get_m_DelayedSetDirty_5() const { return ___m_DelayedSetDirty_5; }
	inline bool* get_address_of_m_DelayedSetDirty_5() { return &___m_DelayedSetDirty_5; }
	inline void set_m_DelayedSetDirty_5(bool value)
	{
		___m_DelayedSetDirty_5 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_6() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_Tracker_6)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_6() const { return ___m_Tracker_6; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_6() { return &___m_Tracker_6; }
	inline void set_m_Tracker_6(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOFITTER_T3312407083_H
#ifndef LAYOUTELEMENT_T1785403678_H
#define LAYOUTELEMENT_T1785403678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutElement
struct  LayoutElement_t1785403678  : public UIBehaviour_t3495933518
{
public:
	// System.Boolean UnityEngine.UI.LayoutElement::m_IgnoreLayout
	bool ___m_IgnoreLayout_2;
	// System.Single UnityEngine.UI.LayoutElement::m_MinWidth
	float ___m_MinWidth_3;
	// System.Single UnityEngine.UI.LayoutElement::m_MinHeight
	float ___m_MinHeight_4;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredWidth
	float ___m_PreferredWidth_5;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredHeight
	float ___m_PreferredHeight_6;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleWidth
	float ___m_FlexibleWidth_7;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleHeight
	float ___m_FlexibleHeight_8;
	// System.Int32 UnityEngine.UI.LayoutElement::m_LayoutPriority
	int32_t ___m_LayoutPriority_9;

public:
	inline static int32_t get_offset_of_m_IgnoreLayout_2() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_IgnoreLayout_2)); }
	inline bool get_m_IgnoreLayout_2() const { return ___m_IgnoreLayout_2; }
	inline bool* get_address_of_m_IgnoreLayout_2() { return &___m_IgnoreLayout_2; }
	inline void set_m_IgnoreLayout_2(bool value)
	{
		___m_IgnoreLayout_2 = value;
	}

	inline static int32_t get_offset_of_m_MinWidth_3() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_MinWidth_3)); }
	inline float get_m_MinWidth_3() const { return ___m_MinWidth_3; }
	inline float* get_address_of_m_MinWidth_3() { return &___m_MinWidth_3; }
	inline void set_m_MinWidth_3(float value)
	{
		___m_MinWidth_3 = value;
	}

	inline static int32_t get_offset_of_m_MinHeight_4() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_MinHeight_4)); }
	inline float get_m_MinHeight_4() const { return ___m_MinHeight_4; }
	inline float* get_address_of_m_MinHeight_4() { return &___m_MinHeight_4; }
	inline void set_m_MinHeight_4(float value)
	{
		___m_MinHeight_4 = value;
	}

	inline static int32_t get_offset_of_m_PreferredWidth_5() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_PreferredWidth_5)); }
	inline float get_m_PreferredWidth_5() const { return ___m_PreferredWidth_5; }
	inline float* get_address_of_m_PreferredWidth_5() { return &___m_PreferredWidth_5; }
	inline void set_m_PreferredWidth_5(float value)
	{
		___m_PreferredWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_PreferredHeight_6() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_PreferredHeight_6)); }
	inline float get_m_PreferredHeight_6() const { return ___m_PreferredHeight_6; }
	inline float* get_address_of_m_PreferredHeight_6() { return &___m_PreferredHeight_6; }
	inline void set_m_PreferredHeight_6(float value)
	{
		___m_PreferredHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleWidth_7() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_FlexibleWidth_7)); }
	inline float get_m_FlexibleWidth_7() const { return ___m_FlexibleWidth_7; }
	inline float* get_address_of_m_FlexibleWidth_7() { return &___m_FlexibleWidth_7; }
	inline void set_m_FlexibleWidth_7(float value)
	{
		___m_FlexibleWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleHeight_8() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_FlexibleHeight_8)); }
	inline float get_m_FlexibleHeight_8() const { return ___m_FlexibleHeight_8; }
	inline float* get_address_of_m_FlexibleHeight_8() { return &___m_FlexibleHeight_8; }
	inline void set_m_FlexibleHeight_8(float value)
	{
		___m_FlexibleHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_LayoutPriority_9() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_LayoutPriority_9)); }
	inline int32_t get_m_LayoutPriority_9() const { return ___m_LayoutPriority_9; }
	inline int32_t* get_address_of_m_LayoutPriority_9() { return &___m_LayoutPriority_9; }
	inline void set_m_LayoutPriority_9(int32_t value)
	{
		___m_LayoutPriority_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTELEMENT_T1785403678_H
#ifndef CANVASSCALER_T2767979955_H
#define CANVASSCALER_T2767979955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler
struct  CanvasScaler_t2767979955  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::m_UiScaleMode
	int32_t ___m_UiScaleMode_2;
	// System.Single UnityEngine.UI.CanvasScaler::m_ReferencePixelsPerUnit
	float ___m_ReferencePixelsPerUnit_3;
	// System.Single UnityEngine.UI.CanvasScaler::m_ScaleFactor
	float ___m_ScaleFactor_4;
	// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::m_ReferenceResolution
	Vector2_t2156229523  ___m_ReferenceResolution_5;
	// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::m_ScreenMatchMode
	int32_t ___m_ScreenMatchMode_6;
	// System.Single UnityEngine.UI.CanvasScaler::m_MatchWidthOrHeight
	float ___m_MatchWidthOrHeight_7;
	// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::m_PhysicalUnit
	int32_t ___m_PhysicalUnit_9;
	// System.Single UnityEngine.UI.CanvasScaler::m_FallbackScreenDPI
	float ___m_FallbackScreenDPI_10;
	// System.Single UnityEngine.UI.CanvasScaler::m_DefaultSpriteDPI
	float ___m_DefaultSpriteDPI_11;
	// System.Single UnityEngine.UI.CanvasScaler::m_DynamicPixelsPerUnit
	float ___m_DynamicPixelsPerUnit_12;
	// UnityEngine.Canvas UnityEngine.UI.CanvasScaler::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_13;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevScaleFactor
	float ___m_PrevScaleFactor_14;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevReferencePixelsPerUnit
	float ___m_PrevReferencePixelsPerUnit_15;

public:
	inline static int32_t get_offset_of_m_UiScaleMode_2() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_UiScaleMode_2)); }
	inline int32_t get_m_UiScaleMode_2() const { return ___m_UiScaleMode_2; }
	inline int32_t* get_address_of_m_UiScaleMode_2() { return &___m_UiScaleMode_2; }
	inline void set_m_UiScaleMode_2(int32_t value)
	{
		___m_UiScaleMode_2 = value;
	}

	inline static int32_t get_offset_of_m_ReferencePixelsPerUnit_3() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_ReferencePixelsPerUnit_3)); }
	inline float get_m_ReferencePixelsPerUnit_3() const { return ___m_ReferencePixelsPerUnit_3; }
	inline float* get_address_of_m_ReferencePixelsPerUnit_3() { return &___m_ReferencePixelsPerUnit_3; }
	inline void set_m_ReferencePixelsPerUnit_3(float value)
	{
		___m_ReferencePixelsPerUnit_3 = value;
	}

	inline static int32_t get_offset_of_m_ScaleFactor_4() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_ScaleFactor_4)); }
	inline float get_m_ScaleFactor_4() const { return ___m_ScaleFactor_4; }
	inline float* get_address_of_m_ScaleFactor_4() { return &___m_ScaleFactor_4; }
	inline void set_m_ScaleFactor_4(float value)
	{
		___m_ScaleFactor_4 = value;
	}

	inline static int32_t get_offset_of_m_ReferenceResolution_5() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_ReferenceResolution_5)); }
	inline Vector2_t2156229523  get_m_ReferenceResolution_5() const { return ___m_ReferenceResolution_5; }
	inline Vector2_t2156229523 * get_address_of_m_ReferenceResolution_5() { return &___m_ReferenceResolution_5; }
	inline void set_m_ReferenceResolution_5(Vector2_t2156229523  value)
	{
		___m_ReferenceResolution_5 = value;
	}

	inline static int32_t get_offset_of_m_ScreenMatchMode_6() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_ScreenMatchMode_6)); }
	inline int32_t get_m_ScreenMatchMode_6() const { return ___m_ScreenMatchMode_6; }
	inline int32_t* get_address_of_m_ScreenMatchMode_6() { return &___m_ScreenMatchMode_6; }
	inline void set_m_ScreenMatchMode_6(int32_t value)
	{
		___m_ScreenMatchMode_6 = value;
	}

	inline static int32_t get_offset_of_m_MatchWidthOrHeight_7() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_MatchWidthOrHeight_7)); }
	inline float get_m_MatchWidthOrHeight_7() const { return ___m_MatchWidthOrHeight_7; }
	inline float* get_address_of_m_MatchWidthOrHeight_7() { return &___m_MatchWidthOrHeight_7; }
	inline void set_m_MatchWidthOrHeight_7(float value)
	{
		___m_MatchWidthOrHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_PhysicalUnit_9() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_PhysicalUnit_9)); }
	inline int32_t get_m_PhysicalUnit_9() const { return ___m_PhysicalUnit_9; }
	inline int32_t* get_address_of_m_PhysicalUnit_9() { return &___m_PhysicalUnit_9; }
	inline void set_m_PhysicalUnit_9(int32_t value)
	{
		___m_PhysicalUnit_9 = value;
	}

	inline static int32_t get_offset_of_m_FallbackScreenDPI_10() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_FallbackScreenDPI_10)); }
	inline float get_m_FallbackScreenDPI_10() const { return ___m_FallbackScreenDPI_10; }
	inline float* get_address_of_m_FallbackScreenDPI_10() { return &___m_FallbackScreenDPI_10; }
	inline void set_m_FallbackScreenDPI_10(float value)
	{
		___m_FallbackScreenDPI_10 = value;
	}

	inline static int32_t get_offset_of_m_DefaultSpriteDPI_11() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_DefaultSpriteDPI_11)); }
	inline float get_m_DefaultSpriteDPI_11() const { return ___m_DefaultSpriteDPI_11; }
	inline float* get_address_of_m_DefaultSpriteDPI_11() { return &___m_DefaultSpriteDPI_11; }
	inline void set_m_DefaultSpriteDPI_11(float value)
	{
		___m_DefaultSpriteDPI_11 = value;
	}

	inline static int32_t get_offset_of_m_DynamicPixelsPerUnit_12() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_DynamicPixelsPerUnit_12)); }
	inline float get_m_DynamicPixelsPerUnit_12() const { return ___m_DynamicPixelsPerUnit_12; }
	inline float* get_address_of_m_DynamicPixelsPerUnit_12() { return &___m_DynamicPixelsPerUnit_12; }
	inline void set_m_DynamicPixelsPerUnit_12(float value)
	{
		___m_DynamicPixelsPerUnit_12 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_Canvas_13)); }
	inline Canvas_t3310196443 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_t3310196443 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_13), value);
	}

	inline static int32_t get_offset_of_m_PrevScaleFactor_14() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_PrevScaleFactor_14)); }
	inline float get_m_PrevScaleFactor_14() const { return ___m_PrevScaleFactor_14; }
	inline float* get_address_of_m_PrevScaleFactor_14() { return &___m_PrevScaleFactor_14; }
	inline void set_m_PrevScaleFactor_14(float value)
	{
		___m_PrevScaleFactor_14 = value;
	}

	inline static int32_t get_offset_of_m_PrevReferencePixelsPerUnit_15() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_PrevReferencePixelsPerUnit_15)); }
	inline float get_m_PrevReferencePixelsPerUnit_15() const { return ___m_PrevReferencePixelsPerUnit_15; }
	inline float* get_address_of_m_PrevReferencePixelsPerUnit_15() { return &___m_PrevReferencePixelsPerUnit_15; }
	inline void set_m_PrevReferencePixelsPerUnit_15(float value)
	{
		___m_PrevReferencePixelsPerUnit_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSCALER_T2767979955_H
#ifndef CONTENTSIZEFITTER_T3850442145_H
#define CONTENTSIZEFITTER_T3850442145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter
struct  ContentSizeFitter_t3850442145  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_HorizontalFit
	int32_t ___m_HorizontalFit_2;
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_VerticalFit
	int32_t ___m_VerticalFit_3;
	// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::m_Rect
	RectTransform_t3704657025 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ContentSizeFitter::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_5;

public:
	inline static int32_t get_offset_of_m_HorizontalFit_2() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_HorizontalFit_2)); }
	inline int32_t get_m_HorizontalFit_2() const { return ___m_HorizontalFit_2; }
	inline int32_t* get_address_of_m_HorizontalFit_2() { return &___m_HorizontalFit_2; }
	inline void set_m_HorizontalFit_2(int32_t value)
	{
		___m_HorizontalFit_2 = value;
	}

	inline static int32_t get_offset_of_m_VerticalFit_3() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_VerticalFit_3)); }
	inline int32_t get_m_VerticalFit_3() const { return ___m_VerticalFit_3; }
	inline int32_t* get_address_of_m_VerticalFit_3() { return &___m_VerticalFit_3; }
	inline void set_m_VerticalFit_3(int32_t value)
	{
		___m_VerticalFit_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_Rect_4)); }
	inline RectTransform_t3704657025 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3704657025 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTSIZEFITTER_T3850442145_H
#ifndef LAYOUTGROUP_T2436138090_H
#define LAYOUTGROUP_T2436138090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t2436138090  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t1369453676 * ___m_Padding_2;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_3;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3704657025 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_5;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2156229523  ___m_TotalMinSize_6;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2156229523  ___m_TotalPreferredSize_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2156229523  ___m_TotalFlexibleSize_8;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t881764471 * ___m_RectChildren_9;

public:
	inline static int32_t get_offset_of_m_Padding_2() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Padding_2)); }
	inline RectOffset_t1369453676 * get_m_Padding_2() const { return ___m_Padding_2; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_2() { return &___m_Padding_2; }
	inline void set_m_Padding_2(RectOffset_t1369453676 * value)
	{
		___m_Padding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_2), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_3() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_ChildAlignment_3)); }
	inline int32_t get_m_ChildAlignment_3() const { return ___m_ChildAlignment_3; }
	inline int32_t* get_address_of_m_ChildAlignment_3() { return &___m_ChildAlignment_3; }
	inline void set_m_ChildAlignment_3(int32_t value)
	{
		___m_ChildAlignment_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Rect_4)); }
	inline RectTransform_t3704657025 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3704657025 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalMinSize_6)); }
	inline Vector2_t2156229523  get_m_TotalMinSize_6() const { return ___m_TotalMinSize_6; }
	inline Vector2_t2156229523 * get_address_of_m_TotalMinSize_6() { return &___m_TotalMinSize_6; }
	inline void set_m_TotalMinSize_6(Vector2_t2156229523  value)
	{
		___m_TotalMinSize_6 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalPreferredSize_7)); }
	inline Vector2_t2156229523  get_m_TotalPreferredSize_7() const { return ___m_TotalPreferredSize_7; }
	inline Vector2_t2156229523 * get_address_of_m_TotalPreferredSize_7() { return &___m_TotalPreferredSize_7; }
	inline void set_m_TotalPreferredSize_7(Vector2_t2156229523  value)
	{
		___m_TotalPreferredSize_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalFlexibleSize_8)); }
	inline Vector2_t2156229523  get_m_TotalFlexibleSize_8() const { return ___m_TotalFlexibleSize_8; }
	inline Vector2_t2156229523 * get_address_of_m_TotalFlexibleSize_8() { return &___m_TotalFlexibleSize_8; }
	inline void set_m_TotalFlexibleSize_8(Vector2_t2156229523  value)
	{
		___m_TotalFlexibleSize_8 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_RectChildren_9)); }
	inline List_1_t881764471 * get_m_RectChildren_9() const { return ___m_RectChildren_9; }
	inline List_1_t881764471 ** get_address_of_m_RectChildren_9() { return &___m_RectChildren_9; }
	inline void set_m_RectChildren_9(List_1_t881764471 * value)
	{
		___m_RectChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T2436138090_H
#ifndef GRIDLAYOUTGROUP_T3046220461_H
#define GRIDLAYOUTGROUP_T3046220461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup
struct  GridLayoutGroup_t3046220461  : public LayoutGroup_t2436138090
{
public:
	// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::m_StartCorner
	int32_t ___m_StartCorner_10;
	// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::m_StartAxis
	int32_t ___m_StartAxis_11;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_CellSize
	Vector2_t2156229523  ___m_CellSize_12;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_Spacing
	Vector2_t2156229523  ___m_Spacing_13;
	// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::m_Constraint
	int32_t ___m_Constraint_14;
	// System.Int32 UnityEngine.UI.GridLayoutGroup::m_ConstraintCount
	int32_t ___m_ConstraintCount_15;

public:
	inline static int32_t get_offset_of_m_StartCorner_10() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_StartCorner_10)); }
	inline int32_t get_m_StartCorner_10() const { return ___m_StartCorner_10; }
	inline int32_t* get_address_of_m_StartCorner_10() { return &___m_StartCorner_10; }
	inline void set_m_StartCorner_10(int32_t value)
	{
		___m_StartCorner_10 = value;
	}

	inline static int32_t get_offset_of_m_StartAxis_11() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_StartAxis_11)); }
	inline int32_t get_m_StartAxis_11() const { return ___m_StartAxis_11; }
	inline int32_t* get_address_of_m_StartAxis_11() { return &___m_StartAxis_11; }
	inline void set_m_StartAxis_11(int32_t value)
	{
		___m_StartAxis_11 = value;
	}

	inline static int32_t get_offset_of_m_CellSize_12() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_CellSize_12)); }
	inline Vector2_t2156229523  get_m_CellSize_12() const { return ___m_CellSize_12; }
	inline Vector2_t2156229523 * get_address_of_m_CellSize_12() { return &___m_CellSize_12; }
	inline void set_m_CellSize_12(Vector2_t2156229523  value)
	{
		___m_CellSize_12 = value;
	}

	inline static int32_t get_offset_of_m_Spacing_13() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_Spacing_13)); }
	inline Vector2_t2156229523  get_m_Spacing_13() const { return ___m_Spacing_13; }
	inline Vector2_t2156229523 * get_address_of_m_Spacing_13() { return &___m_Spacing_13; }
	inline void set_m_Spacing_13(Vector2_t2156229523  value)
	{
		___m_Spacing_13 = value;
	}

	inline static int32_t get_offset_of_m_Constraint_14() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_Constraint_14)); }
	inline int32_t get_m_Constraint_14() const { return ___m_Constraint_14; }
	inline int32_t* get_address_of_m_Constraint_14() { return &___m_Constraint_14; }
	inline void set_m_Constraint_14(int32_t value)
	{
		___m_Constraint_14 = value;
	}

	inline static int32_t get_offset_of_m_ConstraintCount_15() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_ConstraintCount_15)); }
	inline int32_t get_m_ConstraintCount_15() const { return ___m_ConstraintCount_15; }
	inline int32_t* get_address_of_m_ConstraintCount_15() { return &___m_ConstraintCount_15; }
	inline void set_m_ConstraintCount_15(int32_t value)
	{
		___m_ConstraintCount_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDLAYOUTGROUP_T3046220461_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#define HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_t729725570  : public LayoutGroup_t2436138090
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_10;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_11;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_14;

public:
	inline static int32_t get_offset_of_m_Spacing_10() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_Spacing_10)); }
	inline float get_m_Spacing_10() const { return ___m_Spacing_10; }
	inline float* get_address_of_m_Spacing_10() { return &___m_Spacing_10; }
	inline void set_m_Spacing_10(float value)
	{
		___m_Spacing_10 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_11() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandWidth_11)); }
	inline bool get_m_ChildForceExpandWidth_11() const { return ___m_ChildForceExpandWidth_11; }
	inline bool* get_address_of_m_ChildForceExpandWidth_11() { return &___m_ChildForceExpandWidth_11; }
	inline void set_m_ChildForceExpandWidth_11(bool value)
	{
		___m_ChildForceExpandWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandHeight_12)); }
	inline bool get_m_ChildForceExpandHeight_12() const { return ___m_ChildForceExpandHeight_12; }
	inline bool* get_address_of_m_ChildForceExpandHeight_12() { return &___m_ChildForceExpandHeight_12; }
	inline void set_m_ChildForceExpandHeight_12(bool value)
	{
		___m_ChildForceExpandHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlWidth_13)); }
	inline bool get_m_ChildControlWidth_13() const { return ___m_ChildControlWidth_13; }
	inline bool* get_address_of_m_ChildControlWidth_13() { return &___m_ChildControlWidth_13; }
	inline void set_m_ChildControlWidth_13(bool value)
	{
		___m_ChildControlWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlHeight_14)); }
	inline bool get_m_ChildControlHeight_14() const { return ___m_ChildControlHeight_14; }
	inline bool* get_address_of_m_ChildControlHeight_14() { return &___m_ChildControlHeight_14; }
	inline void set_m_ChildControlHeight_14(bool value)
	{
		___m_ChildControlHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifndef POSITIONASUV1_T3991086357_H
#define POSITIONASUV1_T3991086357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3991086357  : public BaseMeshEffect_t2440176439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3991086357_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_3)); }
	inline Color_t2555686324  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2555686324  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_4)); }
	inline Vector2_t2156229523  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2156229523  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef VERTICALLAYOUTGROUP_T923838031_H
#define VERTICALLAYOUTGROUP_T923838031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_t923838031  : public HorizontalOrVerticalLayoutGroup_t729725570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_T923838031_H
#ifndef HORIZONTALLAYOUTGROUP_T2586782146_H
#define HORIZONTALLAYOUTGROUP_T2586782146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalLayoutGroup
struct  HorizontalLayoutGroup_t2586782146  : public HorizontalOrVerticalLayoutGroup_t729725570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALLAYOUTGROUP_T2586782146_H
#ifndef OUTLINE_T2536100125_H
#define OUTLINE_T2536100125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t2536100125  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T2536100125_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (RectangularVertexClipper_t626611136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[2] = 
{
	RectangularVertexClipper_t626611136::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t626611136::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (AspectRatioFitter_t3312407083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1901[5] = 
{
	AspectRatioFitter_t3312407083::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3312407083::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3312407083::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3312407083::get_offset_of_m_DelayedSetDirty_5(),
	AspectRatioFitter_t3312407083::get_offset_of_m_Tracker_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (AspectMode_t3417192999)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1902[6] = 
{
	AspectMode_t3417192999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (CanvasScaler_t2767979955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1903[14] = 
{
	CanvasScaler_t2767979955::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2767979955::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2767979955::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2767979955::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2767979955::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2767979955::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2767979955::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2767979955::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2767979955::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2767979955::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2767979955::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2767979955::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2767979955::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (ScaleMode_t2604066427)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1904[4] = 
{
	ScaleMode_t2604066427::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (ScreenMatchMode_t3675272090)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1905[4] = 
{
	ScreenMatchMode_t3675272090::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (Unit_t2218508340)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1906[6] = 
{
	Unit_t2218508340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (ContentSizeFitter_t3850442145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[4] = 
{
	ContentSizeFitter_t3850442145::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t3850442145::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (FitMode_t3267881214)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1908[4] = 
{
	FitMode_t3267881214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (GridLayoutGroup_t3046220461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[6] = 
{
	GridLayoutGroup_t3046220461::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t3046220461::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t3046220461::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t3046220461::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (Corner_t1493259673)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1910[5] = 
{
	Corner_t1493259673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (Axis_t3613393006)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1911[3] = 
{
	Axis_t3613393006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (Constraint_t814224393)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1912[4] = 
{
	Constraint_t814224393::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (HorizontalLayoutGroup_t2586782146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (HorizontalOrVerticalLayoutGroup_t729725570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[5] = 
{
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (LayoutElement_t1785403678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[8] = 
{
	LayoutElement_t1785403678::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t1785403678::get_offset_of_m_MinWidth_3(),
	LayoutElement_t1785403678::get_offset_of_m_MinHeight_4(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleHeight_8(),
	LayoutElement_t1785403678::get_offset_of_m_LayoutPriority_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (LayoutGroup_t2436138090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[8] = 
{
	LayoutGroup_t2436138090::get_offset_of_m_Padding_2(),
	LayoutGroup_t2436138090::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t2436138090::get_offset_of_m_Rect_4(),
	LayoutGroup_t2436138090::get_offset_of_m_Tracker_5(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t2436138090::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (LayoutRebuilder_t541313304), -1, sizeof(LayoutRebuilder_t541313304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1923[9] = 
{
	LayoutRebuilder_t541313304::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t541313304::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (LayoutUtility_t2745813735), -1, sizeof(LayoutUtility_t2745813735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1924[8] = 
{
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (VerticalLayoutGroup_t923838031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1929[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (ReflectionMethodsCache_t2103211062), -1, sizeof(ReflectionMethodsCache_t2103211062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1930[7] = 
{
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_t2103211062_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (GetRayIntersectionAllNonAllocCallback_t2311174851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (GetRaycastNonAllocCallback_t3841783507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1937[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1945[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (U3CModuleU3E_t692745543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (BulletGenerator_t1131765196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1948[15] = 
{
	BulletGenerator_t1131765196::get_offset_of_oldMissile_2(),
	BulletGenerator_t1131765196::get_offset_of_lineRenderer_3(),
	BulletGenerator_t1131765196::get_offset_of_newMissile_4(),
	BulletGenerator_t1131765196::get_offset_of_Bullets_5(),
	BulletGenerator_t1131765196::get_offset_of_gameManager_6(),
	BulletGenerator_t1131765196::get_offset_of_timeLeft_7(),
	BulletGenerator_t1131765196::get_offset_of_initialVelocity_8(),
	BulletGenerator_t1131765196::get_offset_of_power_9(),
	BulletGenerator_t1131765196::get_offset_of_acceleration_10(),
	BulletGenerator_t1131765196::get_offset_of_LocText_11(),
	BulletGenerator_t1131765196::get_offset_of_DirText_12(),
	BulletGenerator_t1131765196::get_offset_of_AngText_13(),
	BulletGenerator_t1131765196::get_offset_of_TimeText_14(),
	BulletGenerator_t1131765196::get_offset_of_direction_15(),
	BulletGenerator_t1131765196::get_offset_of_missileExist_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (BulletManager_t1336418627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1949[9] = 
{
	BulletManager_t1336418627::get_offset_of_explosion_2(),
	BulletManager_t1336418627::get_offset_of_gameManager_3(),
	BulletManager_t1336418627::get_offset_of_isActive_4(),
	BulletManager_t1336418627::get_offset_of_DIR_5(),
	BulletManager_t1336418627::get_offset_of_acceleration_6(),
	BulletManager_t1336418627::get_offset_of_Bullets_7(),
	BulletManager_t1336418627::get_offset_of_mass_8(),
	BulletManager_t1336418627::get_offset_of_timer_9(),
	BulletManager_t1336418627::get_offset_of_t_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1950[8] = 
{
	U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679::get_offset_of_firstTerm_0(),
	U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679::get_offset_of_tTerm_1(),
	U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679::get_offset_of_U3CtWFSU3E__0_2(),
	U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679::get_offset_of_U3CindexU3E__1_3(),
	U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679::get_offset_of_U24this_4(),
	U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679::get_offset_of_U24current_5(),
	U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679::get_offset_of_U24disposing_6(),
	U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (ButtonManager_t2018100028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[1] = 
{
	ButtonManager_t2018100028::get_offset_of_Character_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (CharacterManager_t2849944402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[9] = 
{
	CharacterManager_t2849944402::get_offset_of_isActive_2(),
	CharacterManager_t2849944402::get_offset_of_direction_3(),
	CharacterManager_t2849944402::get_offset_of_gameManager_4(),
	CharacterManager_t2849944402::get_offset_of_characterSpeed_5(),
	CharacterManager_t2849944402::get_offset_of_CharacterPosition_6(),
	CharacterManager_t2849944402::get_offset_of_StartingPos_7(),
	CharacterManager_t2849944402::get_offset_of_YposMax_8(),
	CharacterManager_t2849944402::get_offset_of_YposMin_9(),
	CharacterManager_t2849944402::get_offset_of_Shooter_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (EnemyGenerator_t2446750330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[8] = 
{
	EnemyGenerator_t2446750330::get_offset_of_enemySprite_2(),
	EnemyGenerator_t2446750330::get_offset_of_gameManager_3(),
	EnemyGenerator_t2446750330::get_offset_of_oldEnemy_4(),
	EnemyGenerator_t2446750330::get_offset_of_newEnemy_5(),
	EnemyGenerator_t2446750330::get_offset_of_Bullets_6(),
	EnemyGenerator_t2446750330::get_offset_of_Position_7(),
	EnemyGenerator_t2446750330::get_offset_of_spawnTerm_8(),
	EnemyGenerator_t2446750330::get_offset_of_KillCount_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (EnemyManager_t913934216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[15] = 
{
	EnemyManager_t913934216::get_offset_of_dissipate_2(),
	EnemyManager_t913934216::get_offset_of_isActive_3(),
	EnemyManager_t913934216::get_offset_of_isHit_4(),
	EnemyManager_t913934216::get_offset_of_isCloudeFade_5(),
	EnemyManager_t913934216::get_offset_of_EnemySpeed_6(),
	EnemyManager_t913934216::get_offset_of_Bullets_7(),
	EnemyManager_t913934216::get_offset_of_fallDir_8(),
	EnemyManager_t913934216::get_offset_of_EnemyPos1_9(),
	EnemyManager_t913934216::get_offset_of_EnemyPos2_10(),
	EnemyManager_t913934216::get_offset_of_EnemyPos3_11(),
	EnemyManager_t913934216::get_offset_of_posDiff_12(),
	EnemyManager_t913934216::get_offset_of_dir_13(),
	EnemyManager_t913934216::get_offset_of_acceleration_14(),
	EnemyManager_t913934216::get_offset_of_mass_15(),
	EnemyManager_t913934216::get_offset_of_gameManager_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[11] = 
{
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_firstTerm_0(),
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_cloud_1(),
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_tTerm_2(),
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_U3CtWFSU3E__0_3(),
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_U3CtempColorU3E__0_4(),
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_tmpColor_5(),
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_U3CindexU3E__1_6(),
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_U24this_7(),
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_U24current_8(),
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_U24disposing_9(),
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (GameManager_t1536523654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[11] = 
{
	GameManager_t1536523654::get_offset_of_isGameOver_2(),
	GameManager_t1536523654::get_offset_of_enemyVictory_3(),
	GameManager_t1536523654::get_offset_of_isPaused_4(),
	GameManager_t1536523654::get_offset_of_isPlay_5(),
	GameManager_t1536523654::get_offset_of_score_6(),
	GameManager_t1536523654::get_offset_of_highscore_7(),
	GameManager_t1536523654::get_offset_of_highscoreText_8(),
	GameManager_t1536523654::get_offset_of_highScoreKey_9(),
	GameManager_t1536523654::get_offset_of_Character_10(),
	GameManager_t1536523654::get_offset_of_StartingPos_11(),
	GameManager_t1536523654::get_offset_of_scoreText_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (LineRenderManager_t120866003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[14] = 
{
	LineRenderManager_t120866003::get_offset_of_lineRenderer_2(),
	LineRenderManager_t120866003::get_offset_of_lineVector01_3(),
	LineRenderManager_t120866003::get_offset_of_lineVector02_4(),
	LineRenderManager_t120866003::get_offset_of_Character_5(),
	LineRenderManager_t120866003::get_offset_of_Missile_6(),
	LineRenderManager_t120866003::get_offset_of_ShootAngle_7(),
	LineRenderManager_t120866003::get_offset_of_radians_8(),
	LineRenderManager_t120866003::get_offset_of_direction_9(),
	LineRenderManager_t120866003::get_offset_of_radius_10(),
	LineRenderManager_t120866003::get_offset_of_gameManager_11(),
	LineRenderManager_t120866003::get_offset_of_mySetTime_12(),
	LineRenderManager_t120866003::get_offset_of_myTimer_13(),
	LineRenderManager_t120866003::get_offset_of_bulletGenerator_14(),
	LineRenderManager_t120866003::get_offset_of_Point2_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (MessageManager_t336836297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[4] = 
{
	MessageManager_t336836297::get_offset_of_gameManager_2(),
	MessageManager_t336836297::get_offset_of_message_3(),
	MessageManager_t336836297::get_offset_of_countdown_4(),
	MessageManager_t336836297::get_offset_of_pause_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[8] = 
{
	U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551::get_offset_of_firstTerm_0(),
	U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551::get_offset_of_tTerm_1(),
	U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551::get_offset_of_U3CtWFSU3E__0_2(),
	U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551::get_offset_of_U3CindexU3E__1_3(),
	U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551::get_offset_of_U24this_4(),
	U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551::get_offset_of_U24current_5(),
	U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551::get_offset_of_U24disposing_6(),
	U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (MoveManager_t918348315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[7] = 
{
	MoveManager_t918348315::get_offset_of_EnemySpeed_2(),
	MoveManager_t918348315::get_offset_of_dir_3(),
	MoveManager_t918348315::get_offset_of_EnemyPos1_4(),
	MoveManager_t918348315::get_offset_of_EnemyPos2_5(),
	MoveManager_t918348315::get_offset_of_EnemyPos3_6(),
	MoveManager_t918348315::get_offset_of_returnSpot_7(),
	MoveManager_t918348315::get_offset_of_posDiff_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (FireEX_t4238753029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[5] = 
{
	FireEX_t4238753029::get_offset_of_rb2d_2(),
	FireEX_t4238753029::get_offset_of_acceleration_3(),
	FireEX_t4238753029::get_offset_of_initialVelocity_4(),
	FireEX_t4238753029::get_offset_of_angle_5(),
	FireEX_t4238753029::get_offset_of_radians_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
