﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// BulletGenerator
struct BulletGenerator_t1131765196;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Component
struct Component_t1923634451;
// System.String
struct String_t;
// GameManager
struct GameManager_t1536523654;
// BulletManager
struct BulletManager_t1336418627;
// System.String[]
struct StringU5BU5D_t1281789340;
// LineRenderManager
struct LineRenderManager_t120866003;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// EnemyManager
struct EnemyManager_t913934216;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// BulletManager/<CoroutineExplosionAnimation>c__Iterator0
struct U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// ButtonManager
struct ButtonManager_t2018100028;
// CharacterManager
struct CharacterManager_t2849944402;
// EnemyGenerator
struct EnemyGenerator_t2446750330;
// EnemyManager/<CoroutineCloudFade>c__Iterator0
struct U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655;
// FireEX
struct FireEX_t4238753029;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// MessageManager
struct MessageManager_t336836297;
// MessageManager/<CoroutineCountDownAnimation>c__Iterator0
struct U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551;
// MoveManager
struct MoveManager_t918348315;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;

extern String_t* _stringLiteral1926007183;
extern const uint32_t BulletGenerator_Start_m1018530826_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801_RuntimeMethod_var;
extern String_t* _stringLiteral2180951049;
extern String_t* _stringLiteral2428460039;
extern String_t* _stringLiteral3786318026;
extern String_t* _stringLiteral3452614535;
extern String_t* _stringLiteral2496649772;
extern const uint32_t BulletGenerator_Update_m3144833810_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisLineRenderManager_t120866003_m531631259_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var;
extern String_t* _stringLiteral1452322613;
extern String_t* _stringLiteral3911666897;
extern const uint32_t BulletGenerator_GenerateBullet_m1172315483_MetadataUsageId;
extern const uint32_t BulletManager_FixedUpdate_m1695692149_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const uint32_t BulletManager_MissileFire_m3246930753_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisEnemyManager_t913934216_m743859818_RuntimeMethod_var;
extern String_t* _stringLiteral2503331403;
extern const uint32_t BulletManager_OnTriggerEnter2D_m12608645_MetadataUsageId;
extern RuntimeClass* U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679_il2cpp_TypeInfo_var;
extern const uint32_t BulletManager_CoroutineExplosionAnimation_m683039687_MetadataUsageId;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var;
extern const uint32_t U3CCoroutineExplosionAnimationU3Ec__Iterator0_MoveNext_m459838247_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CCoroutineExplosionAnimationU3Ec__Iterator0_Reset_m3053651895_RuntimeMethod_var;
extern const uint32_t U3CCoroutineExplosionAnimationU3Ec__Iterator0_Reset_m3053651895_MetadataUsageId;
extern String_t* _stringLiteral3105844272;
extern const uint32_t ButtonManager_ButtonClick_m363216488_MetadataUsageId;
extern const uint32_t CharacterManager_Update_m1700690672_MetadataUsageId;
extern const uint32_t CharacterManager_CharacterMove_m97125875_MetadataUsageId;
extern const uint32_t CharacterManager_CharacterInit_m1829296805_MetadataUsageId;
extern const uint32_t EnemyGenerator_Update_m1945251961_MetadataUsageId;
extern const uint32_t EnemyGenerator_GenerateEnemy_m557526776_MetadataUsageId;
extern const uint32_t EnemyManager_FixedUpdate_m91462524_MetadataUsageId;
extern const uint32_t EnemyManager_ExplodeDie_m497504152_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern String_t* _stringLiteral1176917599;
extern const uint32_t EnemyManager_CallCoroutineCloudeFade_m1288317783_MetadataUsageId;
extern RuntimeClass* U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655_il2cpp_TypeInfo_var;
extern const uint32_t EnemyManager_CoroutineCloudFade_m2978522574_MetadataUsageId;
extern const uint32_t EnemyManager_EnemyMove_m86952508_MetadataUsageId;
extern const uint32_t EnemyManager_EnemyInit_m1387525382_MetadataUsageId;
extern const uint32_t U3CCoroutineCloudFadeU3Ec__Iterator0_MoveNext_m879295093_MetadataUsageId;
extern const RuntimeMethod* U3CCoroutineCloudFadeU3Ec__Iterator0_Reset_m94972674_RuntimeMethod_var;
extern const uint32_t U3CCoroutineCloudFadeU3Ec__Iterator0_Reset_m94972674_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var;
extern const uint32_t FireEX_Start_m1619158142_MetadataUsageId;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern const uint32_t FireEX_Update_m612822988_MetadataUsageId;
extern String_t* _stringLiteral167841699;
extern const uint32_t GameManager__ctor_m180891015_MetadataUsageId;
extern String_t* _stringLiteral309511058;
extern const uint32_t GameManager_GameManagerInit_m1907792971_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisCharacterManager_t2849944402_m3321491559_RuntimeMethod_var;
extern const uint32_t GameManager_Replay_m1504520858_MetadataUsageId;
extern String_t* _stringLiteral435698133;
extern const uint32_t GameManager_StartingPage_m2315722183_MetadataUsageId;
extern String_t* _stringLiteral817681;
extern const uint32_t GameManager_UpdateHighscore_m3829313171_MetadataUsageId;
extern String_t* _stringLiteral1944369699;
extern const uint32_t GameManager_UpdateScore_m509300891_MetadataUsageId;
extern const uint32_t LineRenderManager_Update_m3617602935_MetadataUsageId;
extern const uint32_t LineRenderManager_TaskOnClick_m1632601449_MetadataUsageId;
extern const uint32_t LineRenderManager_UpdatePosition_m280241269_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisLineRenderer_t3154350270_m1162201727_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisLineRenderer_t3154350270_m2920077551_RuntimeMethod_var;
extern const uint32_t LineRenderManager_LineRenderManagerInit_m2547092565_MetadataUsageId;
extern const uint32_t LineRenderManager_SetPosition_m851472669_MetadataUsageId;
extern const uint32_t LineRenderManager_LineRenderSetPos_m4160220108_MetadataUsageId;
extern const uint32_t MessageManager_PlayPressed_m542946951_MetadataUsageId;
extern const uint32_t MessageManager_PausePressed_m2940643355_MetadataUsageId;
extern RuntimeClass* U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551_il2cpp_TypeInfo_var;
extern const uint32_t MessageManager_CoroutineCountDownAnimation_m3397114711_MetadataUsageId;
extern const uint32_t U3CCoroutineCountDownAnimationU3Ec__Iterator0_MoveNext_m2633806077_MetadataUsageId;
extern const RuntimeMethod* U3CCoroutineCountDownAnimationU3Ec__Iterator0_Reset_m698207040_RuntimeMethod_var;
extern const uint32_t U3CCoroutineCountDownAnimationU3Ec__Iterator0_Reset_m698207040_MetadataUsageId;
extern const uint32_t MoveManager_EnemyMove_m400884860_MetadataUsageId;
extern const uint32_t MoveManager_EnemyInit_m4095141009_MetadataUsageId;

struct StringU5BU5D_t1281789340;
struct SpriteU5BU5D_t2581906349;


#ifndef U3CMODULEU3E_T692745543_H
#define U3CMODULEU3E_T692745543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745543 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745543_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CCOROUTINECOUNTDOWNANIMATIONU3EC__ITERATOR0_T3654333551_H
#define U3CCOROUTINECOUNTDOWNANIMATIONU3EC__ITERATOR0_T3654333551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageManager/<CoroutineCountDownAnimation>c__Iterator0
struct  U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551  : public RuntimeObject
{
public:
	// System.Single MessageManager/<CoroutineCountDownAnimation>c__Iterator0::firstTerm
	float ___firstTerm_0;
	// System.Single MessageManager/<CoroutineCountDownAnimation>c__Iterator0::tTerm
	float ___tTerm_1;
	// UnityEngine.WaitForSeconds MessageManager/<CoroutineCountDownAnimation>c__Iterator0::<tWFS>__0
	WaitForSeconds_t1699091251 * ___U3CtWFSU3E__0_2;
	// System.Int32 MessageManager/<CoroutineCountDownAnimation>c__Iterator0::<index>__1
	int32_t ___U3CindexU3E__1_3;
	// MessageManager MessageManager/<CoroutineCountDownAnimation>c__Iterator0::$this
	MessageManager_t336836297 * ___U24this_4;
	// System.Object MessageManager/<CoroutineCountDownAnimation>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean MessageManager/<CoroutineCountDownAnimation>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 MessageManager/<CoroutineCountDownAnimation>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_firstTerm_0() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___firstTerm_0)); }
	inline float get_firstTerm_0() const { return ___firstTerm_0; }
	inline float* get_address_of_firstTerm_0() { return &___firstTerm_0; }
	inline void set_firstTerm_0(float value)
	{
		___firstTerm_0 = value;
	}

	inline static int32_t get_offset_of_tTerm_1() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___tTerm_1)); }
	inline float get_tTerm_1() const { return ___tTerm_1; }
	inline float* get_address_of_tTerm_1() { return &___tTerm_1; }
	inline void set_tTerm_1(float value)
	{
		___tTerm_1 = value;
	}

	inline static int32_t get_offset_of_U3CtWFSU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U3CtWFSU3E__0_2)); }
	inline WaitForSeconds_t1699091251 * get_U3CtWFSU3E__0_2() const { return ___U3CtWFSU3E__0_2; }
	inline WaitForSeconds_t1699091251 ** get_address_of_U3CtWFSU3E__0_2() { return &___U3CtWFSU3E__0_2; }
	inline void set_U3CtWFSU3E__0_2(WaitForSeconds_t1699091251 * value)
	{
		___U3CtWFSU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtWFSU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E__1_3() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U3CindexU3E__1_3)); }
	inline int32_t get_U3CindexU3E__1_3() const { return ___U3CindexU3E__1_3; }
	inline int32_t* get_address_of_U3CindexU3E__1_3() { return &___U3CindexU3E__1_3; }
	inline void set_U3CindexU3E__1_3(int32_t value)
	{
		___U3CindexU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U24this_4)); }
	inline MessageManager_t336836297 * get_U24this_4() const { return ___U24this_4; }
	inline MessageManager_t336836297 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MessageManager_t336836297 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOROUTINECOUNTDOWNANIMATIONU3EC__ITERATOR0_T3654333551_H
#ifndef U3CCOROUTINEEXPLOSIONANIMATIONU3EC__ITERATOR0_T2727895679_H
#define U3CCOROUTINEEXPLOSIONANIMATIONU3EC__ITERATOR0_T2727895679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BulletManager/<CoroutineExplosionAnimation>c__Iterator0
struct  U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679  : public RuntimeObject
{
public:
	// System.Single BulletManager/<CoroutineExplosionAnimation>c__Iterator0::firstTerm
	float ___firstTerm_0;
	// System.Single BulletManager/<CoroutineExplosionAnimation>c__Iterator0::tTerm
	float ___tTerm_1;
	// UnityEngine.WaitForSeconds BulletManager/<CoroutineExplosionAnimation>c__Iterator0::<tWFS>__0
	WaitForSeconds_t1699091251 * ___U3CtWFSU3E__0_2;
	// System.Int32 BulletManager/<CoroutineExplosionAnimation>c__Iterator0::<index>__1
	int32_t ___U3CindexU3E__1_3;
	// BulletManager BulletManager/<CoroutineExplosionAnimation>c__Iterator0::$this
	BulletManager_t1336418627 * ___U24this_4;
	// System.Object BulletManager/<CoroutineExplosionAnimation>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean BulletManager/<CoroutineExplosionAnimation>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 BulletManager/<CoroutineExplosionAnimation>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_firstTerm_0() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___firstTerm_0)); }
	inline float get_firstTerm_0() const { return ___firstTerm_0; }
	inline float* get_address_of_firstTerm_0() { return &___firstTerm_0; }
	inline void set_firstTerm_0(float value)
	{
		___firstTerm_0 = value;
	}

	inline static int32_t get_offset_of_tTerm_1() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___tTerm_1)); }
	inline float get_tTerm_1() const { return ___tTerm_1; }
	inline float* get_address_of_tTerm_1() { return &___tTerm_1; }
	inline void set_tTerm_1(float value)
	{
		___tTerm_1 = value;
	}

	inline static int32_t get_offset_of_U3CtWFSU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U3CtWFSU3E__0_2)); }
	inline WaitForSeconds_t1699091251 * get_U3CtWFSU3E__0_2() const { return ___U3CtWFSU3E__0_2; }
	inline WaitForSeconds_t1699091251 ** get_address_of_U3CtWFSU3E__0_2() { return &___U3CtWFSU3E__0_2; }
	inline void set_U3CtWFSU3E__0_2(WaitForSeconds_t1699091251 * value)
	{
		___U3CtWFSU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtWFSU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E__1_3() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U3CindexU3E__1_3)); }
	inline int32_t get_U3CindexU3E__1_3() const { return ___U3CindexU3E__1_3; }
	inline int32_t* get_address_of_U3CindexU3E__1_3() { return &___U3CindexU3E__1_3; }
	inline void set_U3CindexU3E__1_3(int32_t value)
	{
		___U3CindexU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U24this_4)); }
	inline BulletManager_t1336418627 * get_U24this_4() const { return ___U24this_4; }
	inline BulletManager_t1336418627 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(BulletManager_t1336418627 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOROUTINEEXPLOSIONANIMATIONU3EC__ITERATOR0_T2727895679_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef U3CCOROUTINECLOUDFADEU3EC__ITERATOR0_T2338107655_H
#define U3CCOROUTINECLOUDFADEU3EC__ITERATOR0_T2338107655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyManager/<CoroutineCloudFade>c__Iterator0
struct  U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655  : public RuntimeObject
{
public:
	// System.Single EnemyManager/<CoroutineCloudFade>c__Iterator0::firstTerm
	float ___firstTerm_0;
	// UnityEngine.Transform EnemyManager/<CoroutineCloudFade>c__Iterator0::cloud
	Transform_t3600365921 * ___cloud_1;
	// System.Single EnemyManager/<CoroutineCloudFade>c__Iterator0::tTerm
	float ___tTerm_2;
	// UnityEngine.WaitForSeconds EnemyManager/<CoroutineCloudFade>c__Iterator0::<tWFS>__0
	WaitForSeconds_t1699091251 * ___U3CtWFSU3E__0_3;
	// UnityEngine.Color EnemyManager/<CoroutineCloudFade>c__Iterator0::<tempColor>__0
	Color_t2555686324  ___U3CtempColorU3E__0_4;
	// UnityEngine.Color EnemyManager/<CoroutineCloudFade>c__Iterator0::tmpColor
	Color_t2555686324  ___tmpColor_5;
	// System.Single EnemyManager/<CoroutineCloudFade>c__Iterator0::<index>__1
	float ___U3CindexU3E__1_6;
	// EnemyManager EnemyManager/<CoroutineCloudFade>c__Iterator0::$this
	EnemyManager_t913934216 * ___U24this_7;
	// System.Object EnemyManager/<CoroutineCloudFade>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean EnemyManager/<CoroutineCloudFade>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 EnemyManager/<CoroutineCloudFade>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_firstTerm_0() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___firstTerm_0)); }
	inline float get_firstTerm_0() const { return ___firstTerm_0; }
	inline float* get_address_of_firstTerm_0() { return &___firstTerm_0; }
	inline void set_firstTerm_0(float value)
	{
		___firstTerm_0 = value;
	}

	inline static int32_t get_offset_of_cloud_1() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___cloud_1)); }
	inline Transform_t3600365921 * get_cloud_1() const { return ___cloud_1; }
	inline Transform_t3600365921 ** get_address_of_cloud_1() { return &___cloud_1; }
	inline void set_cloud_1(Transform_t3600365921 * value)
	{
		___cloud_1 = value;
		Il2CppCodeGenWriteBarrier((&___cloud_1), value);
	}

	inline static int32_t get_offset_of_tTerm_2() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___tTerm_2)); }
	inline float get_tTerm_2() const { return ___tTerm_2; }
	inline float* get_address_of_tTerm_2() { return &___tTerm_2; }
	inline void set_tTerm_2(float value)
	{
		___tTerm_2 = value;
	}

	inline static int32_t get_offset_of_U3CtWFSU3E__0_3() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U3CtWFSU3E__0_3)); }
	inline WaitForSeconds_t1699091251 * get_U3CtWFSU3E__0_3() const { return ___U3CtWFSU3E__0_3; }
	inline WaitForSeconds_t1699091251 ** get_address_of_U3CtWFSU3E__0_3() { return &___U3CtWFSU3E__0_3; }
	inline void set_U3CtWFSU3E__0_3(WaitForSeconds_t1699091251 * value)
	{
		___U3CtWFSU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtWFSU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CtempColorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U3CtempColorU3E__0_4)); }
	inline Color_t2555686324  get_U3CtempColorU3E__0_4() const { return ___U3CtempColorU3E__0_4; }
	inline Color_t2555686324 * get_address_of_U3CtempColorU3E__0_4() { return &___U3CtempColorU3E__0_4; }
	inline void set_U3CtempColorU3E__0_4(Color_t2555686324  value)
	{
		___U3CtempColorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_tmpColor_5() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___tmpColor_5)); }
	inline Color_t2555686324  get_tmpColor_5() const { return ___tmpColor_5; }
	inline Color_t2555686324 * get_address_of_tmpColor_5() { return &___tmpColor_5; }
	inline void set_tmpColor_5(Color_t2555686324  value)
	{
		___tmpColor_5 = value;
	}

	inline static int32_t get_offset_of_U3CindexU3E__1_6() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U3CindexU3E__1_6)); }
	inline float get_U3CindexU3E__1_6() const { return ___U3CindexU3E__1_6; }
	inline float* get_address_of_U3CindexU3E__1_6() { return &___U3CindexU3E__1_6; }
	inline void set_U3CindexU3E__1_6(float value)
	{
		___U3CindexU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U24this_7)); }
	inline EnemyManager_t913934216 * get_U24this_7() const { return ___U24this_7; }
	inline EnemyManager_t913934216 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(EnemyManager_t913934216 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOROUTINECLOUDFADEU3EC__ITERATOR0_T2338107655_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef RIGIDBODY2D_T939494601_H
#define RIGIDBODY2D_T939494601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t939494601  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T939494601_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef LINERENDERER_T3154350270_H
#define LINERENDERER_T3154350270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LineRenderer
struct  LineRenderer_t3154350270  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINERENDERER_T3154350270_H
#ifndef BULLETGENERATOR_T1131765196_H
#define BULLETGENERATOR_T1131765196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BulletGenerator
struct  BulletGenerator_t1131765196  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BulletGenerator::oldMissile
	GameObject_t1113636619 * ___oldMissile_2;
	// UnityEngine.GameObject BulletGenerator::lineRenderer
	GameObject_t1113636619 * ___lineRenderer_3;
	// UnityEngine.GameObject BulletGenerator::newMissile
	GameObject_t1113636619 * ___newMissile_4;
	// UnityEngine.GameObject BulletGenerator::Bullets
	GameObject_t1113636619 * ___Bullets_5;
	// UnityEngine.GameObject BulletGenerator::gameManager
	GameObject_t1113636619 * ___gameManager_6;
	// System.Single BulletGenerator::timeLeft
	float ___timeLeft_7;
	// System.Single BulletGenerator::initialVelocity
	float ___initialVelocity_8;
	// System.Single BulletGenerator::power
	float ___power_9;
	// System.Single BulletGenerator::acceleration
	float ___acceleration_10;
	// UnityEngine.UI.Text BulletGenerator::LocText
	Text_t1901882714 * ___LocText_11;
	// UnityEngine.UI.Text BulletGenerator::DirText
	Text_t1901882714 * ___DirText_12;
	// UnityEngine.UI.Text BulletGenerator::AngText
	Text_t1901882714 * ___AngText_13;
	// UnityEngine.UI.Text BulletGenerator::TimeText
	Text_t1901882714 * ___TimeText_14;
	// UnityEngine.Vector3 BulletGenerator::direction
	Vector3_t3722313464  ___direction_15;
	// System.Boolean BulletGenerator::missileExist
	bool ___missileExist_16;

public:
	inline static int32_t get_offset_of_oldMissile_2() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___oldMissile_2)); }
	inline GameObject_t1113636619 * get_oldMissile_2() const { return ___oldMissile_2; }
	inline GameObject_t1113636619 ** get_address_of_oldMissile_2() { return &___oldMissile_2; }
	inline void set_oldMissile_2(GameObject_t1113636619 * value)
	{
		___oldMissile_2 = value;
		Il2CppCodeGenWriteBarrier((&___oldMissile_2), value);
	}

	inline static int32_t get_offset_of_lineRenderer_3() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___lineRenderer_3)); }
	inline GameObject_t1113636619 * get_lineRenderer_3() const { return ___lineRenderer_3; }
	inline GameObject_t1113636619 ** get_address_of_lineRenderer_3() { return &___lineRenderer_3; }
	inline void set_lineRenderer_3(GameObject_t1113636619 * value)
	{
		___lineRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_3), value);
	}

	inline static int32_t get_offset_of_newMissile_4() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___newMissile_4)); }
	inline GameObject_t1113636619 * get_newMissile_4() const { return ___newMissile_4; }
	inline GameObject_t1113636619 ** get_address_of_newMissile_4() { return &___newMissile_4; }
	inline void set_newMissile_4(GameObject_t1113636619 * value)
	{
		___newMissile_4 = value;
		Il2CppCodeGenWriteBarrier((&___newMissile_4), value);
	}

	inline static int32_t get_offset_of_Bullets_5() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___Bullets_5)); }
	inline GameObject_t1113636619 * get_Bullets_5() const { return ___Bullets_5; }
	inline GameObject_t1113636619 ** get_address_of_Bullets_5() { return &___Bullets_5; }
	inline void set_Bullets_5(GameObject_t1113636619 * value)
	{
		___Bullets_5 = value;
		Il2CppCodeGenWriteBarrier((&___Bullets_5), value);
	}

	inline static int32_t get_offset_of_gameManager_6() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___gameManager_6)); }
	inline GameObject_t1113636619 * get_gameManager_6() const { return ___gameManager_6; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_6() { return &___gameManager_6; }
	inline void set_gameManager_6(GameObject_t1113636619 * value)
	{
		___gameManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_6), value);
	}

	inline static int32_t get_offset_of_timeLeft_7() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___timeLeft_7)); }
	inline float get_timeLeft_7() const { return ___timeLeft_7; }
	inline float* get_address_of_timeLeft_7() { return &___timeLeft_7; }
	inline void set_timeLeft_7(float value)
	{
		___timeLeft_7 = value;
	}

	inline static int32_t get_offset_of_initialVelocity_8() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___initialVelocity_8)); }
	inline float get_initialVelocity_8() const { return ___initialVelocity_8; }
	inline float* get_address_of_initialVelocity_8() { return &___initialVelocity_8; }
	inline void set_initialVelocity_8(float value)
	{
		___initialVelocity_8 = value;
	}

	inline static int32_t get_offset_of_power_9() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___power_9)); }
	inline float get_power_9() const { return ___power_9; }
	inline float* get_address_of_power_9() { return &___power_9; }
	inline void set_power_9(float value)
	{
		___power_9 = value;
	}

	inline static int32_t get_offset_of_acceleration_10() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___acceleration_10)); }
	inline float get_acceleration_10() const { return ___acceleration_10; }
	inline float* get_address_of_acceleration_10() { return &___acceleration_10; }
	inline void set_acceleration_10(float value)
	{
		___acceleration_10 = value;
	}

	inline static int32_t get_offset_of_LocText_11() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___LocText_11)); }
	inline Text_t1901882714 * get_LocText_11() const { return ___LocText_11; }
	inline Text_t1901882714 ** get_address_of_LocText_11() { return &___LocText_11; }
	inline void set_LocText_11(Text_t1901882714 * value)
	{
		___LocText_11 = value;
		Il2CppCodeGenWriteBarrier((&___LocText_11), value);
	}

	inline static int32_t get_offset_of_DirText_12() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___DirText_12)); }
	inline Text_t1901882714 * get_DirText_12() const { return ___DirText_12; }
	inline Text_t1901882714 ** get_address_of_DirText_12() { return &___DirText_12; }
	inline void set_DirText_12(Text_t1901882714 * value)
	{
		___DirText_12 = value;
		Il2CppCodeGenWriteBarrier((&___DirText_12), value);
	}

	inline static int32_t get_offset_of_AngText_13() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___AngText_13)); }
	inline Text_t1901882714 * get_AngText_13() const { return ___AngText_13; }
	inline Text_t1901882714 ** get_address_of_AngText_13() { return &___AngText_13; }
	inline void set_AngText_13(Text_t1901882714 * value)
	{
		___AngText_13 = value;
		Il2CppCodeGenWriteBarrier((&___AngText_13), value);
	}

	inline static int32_t get_offset_of_TimeText_14() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___TimeText_14)); }
	inline Text_t1901882714 * get_TimeText_14() const { return ___TimeText_14; }
	inline Text_t1901882714 ** get_address_of_TimeText_14() { return &___TimeText_14; }
	inline void set_TimeText_14(Text_t1901882714 * value)
	{
		___TimeText_14 = value;
		Il2CppCodeGenWriteBarrier((&___TimeText_14), value);
	}

	inline static int32_t get_offset_of_direction_15() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___direction_15)); }
	inline Vector3_t3722313464  get_direction_15() const { return ___direction_15; }
	inline Vector3_t3722313464 * get_address_of_direction_15() { return &___direction_15; }
	inline void set_direction_15(Vector3_t3722313464  value)
	{
		___direction_15 = value;
	}

	inline static int32_t get_offset_of_missileExist_16() { return static_cast<int32_t>(offsetof(BulletGenerator_t1131765196, ___missileExist_16)); }
	inline bool get_missileExist_16() const { return ___missileExist_16; }
	inline bool* get_address_of_missileExist_16() { return &___missileExist_16; }
	inline void set_missileExist_16(bool value)
	{
		___missileExist_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLETGENERATOR_T1131765196_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef MOVEMANAGER_T918348315_H
#define MOVEMANAGER_T918348315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveManager
struct  MoveManager_t918348315  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoveManager::EnemySpeed
	float ___EnemySpeed_2;
	// System.Int32 MoveManager::dir
	int32_t ___dir_3;
	// UnityEngine.Vector3 MoveManager::EnemyPos1
	Vector3_t3722313464  ___EnemyPos1_4;
	// UnityEngine.Vector3 MoveManager::EnemyPos2
	Vector3_t3722313464  ___EnemyPos2_5;
	// UnityEngine.Vector3 MoveManager::EnemyPos3
	Vector3_t3722313464  ___EnemyPos3_6;
	// UnityEngine.Vector3 MoveManager::returnSpot
	Vector3_t3722313464  ___returnSpot_7;
	// UnityEngine.Vector3 MoveManager::posDiff
	Vector3_t3722313464  ___posDiff_8;

public:
	inline static int32_t get_offset_of_EnemySpeed_2() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___EnemySpeed_2)); }
	inline float get_EnemySpeed_2() const { return ___EnemySpeed_2; }
	inline float* get_address_of_EnemySpeed_2() { return &___EnemySpeed_2; }
	inline void set_EnemySpeed_2(float value)
	{
		___EnemySpeed_2 = value;
	}

	inline static int32_t get_offset_of_dir_3() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___dir_3)); }
	inline int32_t get_dir_3() const { return ___dir_3; }
	inline int32_t* get_address_of_dir_3() { return &___dir_3; }
	inline void set_dir_3(int32_t value)
	{
		___dir_3 = value;
	}

	inline static int32_t get_offset_of_EnemyPos1_4() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___EnemyPos1_4)); }
	inline Vector3_t3722313464  get_EnemyPos1_4() const { return ___EnemyPos1_4; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos1_4() { return &___EnemyPos1_4; }
	inline void set_EnemyPos1_4(Vector3_t3722313464  value)
	{
		___EnemyPos1_4 = value;
	}

	inline static int32_t get_offset_of_EnemyPos2_5() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___EnemyPos2_5)); }
	inline Vector3_t3722313464  get_EnemyPos2_5() const { return ___EnemyPos2_5; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos2_5() { return &___EnemyPos2_5; }
	inline void set_EnemyPos2_5(Vector3_t3722313464  value)
	{
		___EnemyPos2_5 = value;
	}

	inline static int32_t get_offset_of_EnemyPos3_6() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___EnemyPos3_6)); }
	inline Vector3_t3722313464  get_EnemyPos3_6() const { return ___EnemyPos3_6; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos3_6() { return &___EnemyPos3_6; }
	inline void set_EnemyPos3_6(Vector3_t3722313464  value)
	{
		___EnemyPos3_6 = value;
	}

	inline static int32_t get_offset_of_returnSpot_7() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___returnSpot_7)); }
	inline Vector3_t3722313464  get_returnSpot_7() const { return ___returnSpot_7; }
	inline Vector3_t3722313464 * get_address_of_returnSpot_7() { return &___returnSpot_7; }
	inline void set_returnSpot_7(Vector3_t3722313464  value)
	{
		___returnSpot_7 = value;
	}

	inline static int32_t get_offset_of_posDiff_8() { return static_cast<int32_t>(offsetof(MoveManager_t918348315, ___posDiff_8)); }
	inline Vector3_t3722313464  get_posDiff_8() const { return ___posDiff_8; }
	inline Vector3_t3722313464 * get_address_of_posDiff_8() { return &___posDiff_8; }
	inline void set_posDiff_8(Vector3_t3722313464  value)
	{
		___posDiff_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMANAGER_T918348315_H
#ifndef CHARACTERMANAGER_T2849944402_H
#define CHARACTERMANAGER_T2849944402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterManager
struct  CharacterManager_t2849944402  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CharacterManager::isActive
	bool ___isActive_2;
	// System.Int32 CharacterManager::direction
	int32_t ___direction_3;
	// UnityEngine.GameObject CharacterManager::gameManager
	GameObject_t1113636619 * ___gameManager_4;
	// System.Single CharacterManager::characterSpeed
	float ___characterSpeed_5;
	// UnityEngine.GameObject CharacterManager::CharacterPosition
	GameObject_t1113636619 * ___CharacterPosition_6;
	// UnityEngine.Vector3 CharacterManager::StartingPos
	Vector3_t3722313464  ___StartingPos_7;
	// UnityEngine.Vector3 CharacterManager::YposMax
	Vector3_t3722313464  ___YposMax_8;
	// UnityEngine.Vector3 CharacterManager::YposMin
	Vector3_t3722313464  ___YposMin_9;
	// UnityEngine.GameObject CharacterManager::Shooter
	GameObject_t1113636619 * ___Shooter_10;

public:
	inline static int32_t get_offset_of_isActive_2() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___isActive_2)); }
	inline bool get_isActive_2() const { return ___isActive_2; }
	inline bool* get_address_of_isActive_2() { return &___isActive_2; }
	inline void set_isActive_2(bool value)
	{
		___isActive_2 = value;
	}

	inline static int32_t get_offset_of_direction_3() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___direction_3)); }
	inline int32_t get_direction_3() const { return ___direction_3; }
	inline int32_t* get_address_of_direction_3() { return &___direction_3; }
	inline void set_direction_3(int32_t value)
	{
		___direction_3 = value;
	}

	inline static int32_t get_offset_of_gameManager_4() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___gameManager_4)); }
	inline GameObject_t1113636619 * get_gameManager_4() const { return ___gameManager_4; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_4() { return &___gameManager_4; }
	inline void set_gameManager_4(GameObject_t1113636619 * value)
	{
		___gameManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_4), value);
	}

	inline static int32_t get_offset_of_characterSpeed_5() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___characterSpeed_5)); }
	inline float get_characterSpeed_5() const { return ___characterSpeed_5; }
	inline float* get_address_of_characterSpeed_5() { return &___characterSpeed_5; }
	inline void set_characterSpeed_5(float value)
	{
		___characterSpeed_5 = value;
	}

	inline static int32_t get_offset_of_CharacterPosition_6() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___CharacterPosition_6)); }
	inline GameObject_t1113636619 * get_CharacterPosition_6() const { return ___CharacterPosition_6; }
	inline GameObject_t1113636619 ** get_address_of_CharacterPosition_6() { return &___CharacterPosition_6; }
	inline void set_CharacterPosition_6(GameObject_t1113636619 * value)
	{
		___CharacterPosition_6 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterPosition_6), value);
	}

	inline static int32_t get_offset_of_StartingPos_7() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___StartingPos_7)); }
	inline Vector3_t3722313464  get_StartingPos_7() const { return ___StartingPos_7; }
	inline Vector3_t3722313464 * get_address_of_StartingPos_7() { return &___StartingPos_7; }
	inline void set_StartingPos_7(Vector3_t3722313464  value)
	{
		___StartingPos_7 = value;
	}

	inline static int32_t get_offset_of_YposMax_8() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___YposMax_8)); }
	inline Vector3_t3722313464  get_YposMax_8() const { return ___YposMax_8; }
	inline Vector3_t3722313464 * get_address_of_YposMax_8() { return &___YposMax_8; }
	inline void set_YposMax_8(Vector3_t3722313464  value)
	{
		___YposMax_8 = value;
	}

	inline static int32_t get_offset_of_YposMin_9() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___YposMin_9)); }
	inline Vector3_t3722313464  get_YposMin_9() const { return ___YposMin_9; }
	inline Vector3_t3722313464 * get_address_of_YposMin_9() { return &___YposMin_9; }
	inline void set_YposMin_9(Vector3_t3722313464  value)
	{
		___YposMin_9 = value;
	}

	inline static int32_t get_offset_of_Shooter_10() { return static_cast<int32_t>(offsetof(CharacterManager_t2849944402, ___Shooter_10)); }
	inline GameObject_t1113636619 * get_Shooter_10() const { return ___Shooter_10; }
	inline GameObject_t1113636619 ** get_address_of_Shooter_10() { return &___Shooter_10; }
	inline void set_Shooter_10(GameObject_t1113636619 * value)
	{
		___Shooter_10 = value;
		Il2CppCodeGenWriteBarrier((&___Shooter_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERMANAGER_T2849944402_H
#ifndef FIREEX_T4238753029_H
#define FIREEX_T4238753029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FireEX
struct  FireEX_t4238753029  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody2D FireEX::rb2d
	Rigidbody2D_t939494601 * ___rb2d_2;
	// System.Single FireEX::acceleration
	float ___acceleration_3;
	// System.Single FireEX::initialVelocity
	float ___initialVelocity_4;
	// System.Single FireEX::angle
	float ___angle_5;
	// System.Single FireEX::radians
	float ___radians_6;

public:
	inline static int32_t get_offset_of_rb2d_2() { return static_cast<int32_t>(offsetof(FireEX_t4238753029, ___rb2d_2)); }
	inline Rigidbody2D_t939494601 * get_rb2d_2() const { return ___rb2d_2; }
	inline Rigidbody2D_t939494601 ** get_address_of_rb2d_2() { return &___rb2d_2; }
	inline void set_rb2d_2(Rigidbody2D_t939494601 * value)
	{
		___rb2d_2 = value;
		Il2CppCodeGenWriteBarrier((&___rb2d_2), value);
	}

	inline static int32_t get_offset_of_acceleration_3() { return static_cast<int32_t>(offsetof(FireEX_t4238753029, ___acceleration_3)); }
	inline float get_acceleration_3() const { return ___acceleration_3; }
	inline float* get_address_of_acceleration_3() { return &___acceleration_3; }
	inline void set_acceleration_3(float value)
	{
		___acceleration_3 = value;
	}

	inline static int32_t get_offset_of_initialVelocity_4() { return static_cast<int32_t>(offsetof(FireEX_t4238753029, ___initialVelocity_4)); }
	inline float get_initialVelocity_4() const { return ___initialVelocity_4; }
	inline float* get_address_of_initialVelocity_4() { return &___initialVelocity_4; }
	inline void set_initialVelocity_4(float value)
	{
		___initialVelocity_4 = value;
	}

	inline static int32_t get_offset_of_angle_5() { return static_cast<int32_t>(offsetof(FireEX_t4238753029, ___angle_5)); }
	inline float get_angle_5() const { return ___angle_5; }
	inline float* get_address_of_angle_5() { return &___angle_5; }
	inline void set_angle_5(float value)
	{
		___angle_5 = value;
	}

	inline static int32_t get_offset_of_radians_6() { return static_cast<int32_t>(offsetof(FireEX_t4238753029, ___radians_6)); }
	inline float get_radians_6() const { return ___radians_6; }
	inline float* get_address_of_radians_6() { return &___radians_6; }
	inline void set_radians_6(float value)
	{
		___radians_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREEX_T4238753029_H
#ifndef ENEMYGENERATOR_T2446750330_H
#define ENEMYGENERATOR_T2446750330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyGenerator
struct  EnemyGenerator_t2446750330  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] EnemyGenerator::enemySprite
	SpriteU5BU5D_t2581906349* ___enemySprite_2;
	// UnityEngine.GameObject EnemyGenerator::gameManager
	GameObject_t1113636619 * ___gameManager_3;
	// UnityEngine.GameObject EnemyGenerator::oldEnemy
	GameObject_t1113636619 * ___oldEnemy_4;
	// UnityEngine.GameObject EnemyGenerator::newEnemy
	GameObject_t1113636619 * ___newEnemy_5;
	// UnityEngine.GameObject EnemyGenerator::Bullets
	GameObject_t1113636619 * ___Bullets_6;
	// UnityEngine.Vector3 EnemyGenerator::Position
	Vector3_t3722313464  ___Position_7;
	// System.Single EnemyGenerator::spawnTerm
	float ___spawnTerm_8;
	// System.Int32 EnemyGenerator::KillCount
	int32_t ___KillCount_9;

public:
	inline static int32_t get_offset_of_enemySprite_2() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___enemySprite_2)); }
	inline SpriteU5BU5D_t2581906349* get_enemySprite_2() const { return ___enemySprite_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of_enemySprite_2() { return &___enemySprite_2; }
	inline void set_enemySprite_2(SpriteU5BU5D_t2581906349* value)
	{
		___enemySprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___enemySprite_2), value);
	}

	inline static int32_t get_offset_of_gameManager_3() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___gameManager_3)); }
	inline GameObject_t1113636619 * get_gameManager_3() const { return ___gameManager_3; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_3() { return &___gameManager_3; }
	inline void set_gameManager_3(GameObject_t1113636619 * value)
	{
		___gameManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_3), value);
	}

	inline static int32_t get_offset_of_oldEnemy_4() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___oldEnemy_4)); }
	inline GameObject_t1113636619 * get_oldEnemy_4() const { return ___oldEnemy_4; }
	inline GameObject_t1113636619 ** get_address_of_oldEnemy_4() { return &___oldEnemy_4; }
	inline void set_oldEnemy_4(GameObject_t1113636619 * value)
	{
		___oldEnemy_4 = value;
		Il2CppCodeGenWriteBarrier((&___oldEnemy_4), value);
	}

	inline static int32_t get_offset_of_newEnemy_5() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___newEnemy_5)); }
	inline GameObject_t1113636619 * get_newEnemy_5() const { return ___newEnemy_5; }
	inline GameObject_t1113636619 ** get_address_of_newEnemy_5() { return &___newEnemy_5; }
	inline void set_newEnemy_5(GameObject_t1113636619 * value)
	{
		___newEnemy_5 = value;
		Il2CppCodeGenWriteBarrier((&___newEnemy_5), value);
	}

	inline static int32_t get_offset_of_Bullets_6() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___Bullets_6)); }
	inline GameObject_t1113636619 * get_Bullets_6() const { return ___Bullets_6; }
	inline GameObject_t1113636619 ** get_address_of_Bullets_6() { return &___Bullets_6; }
	inline void set_Bullets_6(GameObject_t1113636619 * value)
	{
		___Bullets_6 = value;
		Il2CppCodeGenWriteBarrier((&___Bullets_6), value);
	}

	inline static int32_t get_offset_of_Position_7() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___Position_7)); }
	inline Vector3_t3722313464  get_Position_7() const { return ___Position_7; }
	inline Vector3_t3722313464 * get_address_of_Position_7() { return &___Position_7; }
	inline void set_Position_7(Vector3_t3722313464  value)
	{
		___Position_7 = value;
	}

	inline static int32_t get_offset_of_spawnTerm_8() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___spawnTerm_8)); }
	inline float get_spawnTerm_8() const { return ___spawnTerm_8; }
	inline float* get_address_of_spawnTerm_8() { return &___spawnTerm_8; }
	inline void set_spawnTerm_8(float value)
	{
		___spawnTerm_8 = value;
	}

	inline static int32_t get_offset_of_KillCount_9() { return static_cast<int32_t>(offsetof(EnemyGenerator_t2446750330, ___KillCount_9)); }
	inline int32_t get_KillCount_9() const { return ___KillCount_9; }
	inline int32_t* get_address_of_KillCount_9() { return &___KillCount_9; }
	inline void set_KillCount_9(int32_t value)
	{
		___KillCount_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYGENERATOR_T2446750330_H
#ifndef BUTTONMANAGER_T2018100028_H
#define BUTTONMANAGER_T2018100028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonManager
struct  ButtonManager_t2018100028  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ButtonManager::Character
	GameObject_t1113636619 * ___Character_2;

public:
	inline static int32_t get_offset_of_Character_2() { return static_cast<int32_t>(offsetof(ButtonManager_t2018100028, ___Character_2)); }
	inline GameObject_t1113636619 * get_Character_2() const { return ___Character_2; }
	inline GameObject_t1113636619 ** get_address_of_Character_2() { return &___Character_2; }
	inline void set_Character_2(GameObject_t1113636619 * value)
	{
		___Character_2 = value;
		Il2CppCodeGenWriteBarrier((&___Character_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONMANAGER_T2018100028_H
#ifndef ENEMYMANAGER_T913934216_H
#define ENEMYMANAGER_T913934216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyManager
struct  EnemyManager_t913934216  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] EnemyManager::dissipate
	SpriteU5BU5D_t2581906349* ___dissipate_2;
	// System.Boolean EnemyManager::isActive
	bool ___isActive_3;
	// System.Boolean EnemyManager::isHit
	bool ___isHit_4;
	// System.Boolean EnemyManager::isCloudeFade
	bool ___isCloudeFade_5;
	// System.Single EnemyManager::EnemySpeed
	float ___EnemySpeed_6;
	// UnityEngine.GameObject EnemyManager::Bullets
	GameObject_t1113636619 * ___Bullets_7;
	// UnityEngine.Vector3 EnemyManager::fallDir
	Vector3_t3722313464  ___fallDir_8;
	// UnityEngine.Vector3 EnemyManager::EnemyPos1
	Vector3_t3722313464  ___EnemyPos1_9;
	// UnityEngine.Vector3 EnemyManager::EnemyPos2
	Vector3_t3722313464  ___EnemyPos2_10;
	// UnityEngine.Vector3 EnemyManager::EnemyPos3
	Vector3_t3722313464  ___EnemyPos3_11;
	// UnityEngine.Vector3 EnemyManager::posDiff
	Vector3_t3722313464  ___posDiff_12;
	// System.Int32 EnemyManager::dir
	int32_t ___dir_13;
	// System.Single EnemyManager::acceleration
	float ___acceleration_14;
	// System.Single EnemyManager::mass
	float ___mass_15;
	// UnityEngine.GameObject EnemyManager::gameManager
	GameObject_t1113636619 * ___gameManager_16;

public:
	inline static int32_t get_offset_of_dissipate_2() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___dissipate_2)); }
	inline SpriteU5BU5D_t2581906349* get_dissipate_2() const { return ___dissipate_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of_dissipate_2() { return &___dissipate_2; }
	inline void set_dissipate_2(SpriteU5BU5D_t2581906349* value)
	{
		___dissipate_2 = value;
		Il2CppCodeGenWriteBarrier((&___dissipate_2), value);
	}

	inline static int32_t get_offset_of_isActive_3() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___isActive_3)); }
	inline bool get_isActive_3() const { return ___isActive_3; }
	inline bool* get_address_of_isActive_3() { return &___isActive_3; }
	inline void set_isActive_3(bool value)
	{
		___isActive_3 = value;
	}

	inline static int32_t get_offset_of_isHit_4() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___isHit_4)); }
	inline bool get_isHit_4() const { return ___isHit_4; }
	inline bool* get_address_of_isHit_4() { return &___isHit_4; }
	inline void set_isHit_4(bool value)
	{
		___isHit_4 = value;
	}

	inline static int32_t get_offset_of_isCloudeFade_5() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___isCloudeFade_5)); }
	inline bool get_isCloudeFade_5() const { return ___isCloudeFade_5; }
	inline bool* get_address_of_isCloudeFade_5() { return &___isCloudeFade_5; }
	inline void set_isCloudeFade_5(bool value)
	{
		___isCloudeFade_5 = value;
	}

	inline static int32_t get_offset_of_EnemySpeed_6() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___EnemySpeed_6)); }
	inline float get_EnemySpeed_6() const { return ___EnemySpeed_6; }
	inline float* get_address_of_EnemySpeed_6() { return &___EnemySpeed_6; }
	inline void set_EnemySpeed_6(float value)
	{
		___EnemySpeed_6 = value;
	}

	inline static int32_t get_offset_of_Bullets_7() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___Bullets_7)); }
	inline GameObject_t1113636619 * get_Bullets_7() const { return ___Bullets_7; }
	inline GameObject_t1113636619 ** get_address_of_Bullets_7() { return &___Bullets_7; }
	inline void set_Bullets_7(GameObject_t1113636619 * value)
	{
		___Bullets_7 = value;
		Il2CppCodeGenWriteBarrier((&___Bullets_7), value);
	}

	inline static int32_t get_offset_of_fallDir_8() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___fallDir_8)); }
	inline Vector3_t3722313464  get_fallDir_8() const { return ___fallDir_8; }
	inline Vector3_t3722313464 * get_address_of_fallDir_8() { return &___fallDir_8; }
	inline void set_fallDir_8(Vector3_t3722313464  value)
	{
		___fallDir_8 = value;
	}

	inline static int32_t get_offset_of_EnemyPos1_9() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___EnemyPos1_9)); }
	inline Vector3_t3722313464  get_EnemyPos1_9() const { return ___EnemyPos1_9; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos1_9() { return &___EnemyPos1_9; }
	inline void set_EnemyPos1_9(Vector3_t3722313464  value)
	{
		___EnemyPos1_9 = value;
	}

	inline static int32_t get_offset_of_EnemyPos2_10() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___EnemyPos2_10)); }
	inline Vector3_t3722313464  get_EnemyPos2_10() const { return ___EnemyPos2_10; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos2_10() { return &___EnemyPos2_10; }
	inline void set_EnemyPos2_10(Vector3_t3722313464  value)
	{
		___EnemyPos2_10 = value;
	}

	inline static int32_t get_offset_of_EnemyPos3_11() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___EnemyPos3_11)); }
	inline Vector3_t3722313464  get_EnemyPos3_11() const { return ___EnemyPos3_11; }
	inline Vector3_t3722313464 * get_address_of_EnemyPos3_11() { return &___EnemyPos3_11; }
	inline void set_EnemyPos3_11(Vector3_t3722313464  value)
	{
		___EnemyPos3_11 = value;
	}

	inline static int32_t get_offset_of_posDiff_12() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___posDiff_12)); }
	inline Vector3_t3722313464  get_posDiff_12() const { return ___posDiff_12; }
	inline Vector3_t3722313464 * get_address_of_posDiff_12() { return &___posDiff_12; }
	inline void set_posDiff_12(Vector3_t3722313464  value)
	{
		___posDiff_12 = value;
	}

	inline static int32_t get_offset_of_dir_13() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___dir_13)); }
	inline int32_t get_dir_13() const { return ___dir_13; }
	inline int32_t* get_address_of_dir_13() { return &___dir_13; }
	inline void set_dir_13(int32_t value)
	{
		___dir_13 = value;
	}

	inline static int32_t get_offset_of_acceleration_14() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___acceleration_14)); }
	inline float get_acceleration_14() const { return ___acceleration_14; }
	inline float* get_address_of_acceleration_14() { return &___acceleration_14; }
	inline void set_acceleration_14(float value)
	{
		___acceleration_14 = value;
	}

	inline static int32_t get_offset_of_mass_15() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___mass_15)); }
	inline float get_mass_15() const { return ___mass_15; }
	inline float* get_address_of_mass_15() { return &___mass_15; }
	inline void set_mass_15(float value)
	{
		___mass_15 = value;
	}

	inline static int32_t get_offset_of_gameManager_16() { return static_cast<int32_t>(offsetof(EnemyManager_t913934216, ___gameManager_16)); }
	inline GameObject_t1113636619 * get_gameManager_16() const { return ___gameManager_16; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_16() { return &___gameManager_16; }
	inline void set_gameManager_16(GameObject_t1113636619 * value)
	{
		___gameManager_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYMANAGER_T913934216_H
#ifndef LINERENDERMANAGER_T120866003_H
#define LINERENDERMANAGER_T120866003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LineRenderManager
struct  LineRenderManager_t120866003  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.LineRenderer LineRenderManager::lineRenderer
	LineRenderer_t3154350270 * ___lineRenderer_2;
	// UnityEngine.Vector2 LineRenderManager::lineVector01
	Vector2_t2156229523  ___lineVector01_3;
	// UnityEngine.Vector2 LineRenderManager::lineVector02
	Vector2_t2156229523  ___lineVector02_4;
	// UnityEngine.GameObject LineRenderManager::Character
	GameObject_t1113636619 * ___Character_5;
	// UnityEngine.GameObject LineRenderManager::Missile
	GameObject_t1113636619 * ___Missile_6;
	// System.Single LineRenderManager::ShootAngle
	float ___ShootAngle_7;
	// System.Single LineRenderManager::radians
	float ___radians_8;
	// System.Int32 LineRenderManager::direction
	int32_t ___direction_9;
	// System.Single LineRenderManager::radius
	float ___radius_10;
	// UnityEngine.GameObject LineRenderManager::gameManager
	GameObject_t1113636619 * ___gameManager_11;
	// System.Single LineRenderManager::mySetTime
	float ___mySetTime_12;
	// System.Single LineRenderManager::myTimer
	float ___myTimer_13;
	// BulletGenerator LineRenderManager::bulletGenerator
	BulletGenerator_t1131765196 * ___bulletGenerator_14;
	// UnityEngine.Vector2 LineRenderManager::Point2
	Vector2_t2156229523  ___Point2_15;

public:
	inline static int32_t get_offset_of_lineRenderer_2() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___lineRenderer_2)); }
	inline LineRenderer_t3154350270 * get_lineRenderer_2() const { return ___lineRenderer_2; }
	inline LineRenderer_t3154350270 ** get_address_of_lineRenderer_2() { return &___lineRenderer_2; }
	inline void set_lineRenderer_2(LineRenderer_t3154350270 * value)
	{
		___lineRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_2), value);
	}

	inline static int32_t get_offset_of_lineVector01_3() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___lineVector01_3)); }
	inline Vector2_t2156229523  get_lineVector01_3() const { return ___lineVector01_3; }
	inline Vector2_t2156229523 * get_address_of_lineVector01_3() { return &___lineVector01_3; }
	inline void set_lineVector01_3(Vector2_t2156229523  value)
	{
		___lineVector01_3 = value;
	}

	inline static int32_t get_offset_of_lineVector02_4() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___lineVector02_4)); }
	inline Vector2_t2156229523  get_lineVector02_4() const { return ___lineVector02_4; }
	inline Vector2_t2156229523 * get_address_of_lineVector02_4() { return &___lineVector02_4; }
	inline void set_lineVector02_4(Vector2_t2156229523  value)
	{
		___lineVector02_4 = value;
	}

	inline static int32_t get_offset_of_Character_5() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___Character_5)); }
	inline GameObject_t1113636619 * get_Character_5() const { return ___Character_5; }
	inline GameObject_t1113636619 ** get_address_of_Character_5() { return &___Character_5; }
	inline void set_Character_5(GameObject_t1113636619 * value)
	{
		___Character_5 = value;
		Il2CppCodeGenWriteBarrier((&___Character_5), value);
	}

	inline static int32_t get_offset_of_Missile_6() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___Missile_6)); }
	inline GameObject_t1113636619 * get_Missile_6() const { return ___Missile_6; }
	inline GameObject_t1113636619 ** get_address_of_Missile_6() { return &___Missile_6; }
	inline void set_Missile_6(GameObject_t1113636619 * value)
	{
		___Missile_6 = value;
		Il2CppCodeGenWriteBarrier((&___Missile_6), value);
	}

	inline static int32_t get_offset_of_ShootAngle_7() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___ShootAngle_7)); }
	inline float get_ShootAngle_7() const { return ___ShootAngle_7; }
	inline float* get_address_of_ShootAngle_7() { return &___ShootAngle_7; }
	inline void set_ShootAngle_7(float value)
	{
		___ShootAngle_7 = value;
	}

	inline static int32_t get_offset_of_radians_8() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___radians_8)); }
	inline float get_radians_8() const { return ___radians_8; }
	inline float* get_address_of_radians_8() { return &___radians_8; }
	inline void set_radians_8(float value)
	{
		___radians_8 = value;
	}

	inline static int32_t get_offset_of_direction_9() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___direction_9)); }
	inline int32_t get_direction_9() const { return ___direction_9; }
	inline int32_t* get_address_of_direction_9() { return &___direction_9; }
	inline void set_direction_9(int32_t value)
	{
		___direction_9 = value;
	}

	inline static int32_t get_offset_of_radius_10() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___radius_10)); }
	inline float get_radius_10() const { return ___radius_10; }
	inline float* get_address_of_radius_10() { return &___radius_10; }
	inline void set_radius_10(float value)
	{
		___radius_10 = value;
	}

	inline static int32_t get_offset_of_gameManager_11() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___gameManager_11)); }
	inline GameObject_t1113636619 * get_gameManager_11() const { return ___gameManager_11; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_11() { return &___gameManager_11; }
	inline void set_gameManager_11(GameObject_t1113636619 * value)
	{
		___gameManager_11 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_11), value);
	}

	inline static int32_t get_offset_of_mySetTime_12() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___mySetTime_12)); }
	inline float get_mySetTime_12() const { return ___mySetTime_12; }
	inline float* get_address_of_mySetTime_12() { return &___mySetTime_12; }
	inline void set_mySetTime_12(float value)
	{
		___mySetTime_12 = value;
	}

	inline static int32_t get_offset_of_myTimer_13() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___myTimer_13)); }
	inline float get_myTimer_13() const { return ___myTimer_13; }
	inline float* get_address_of_myTimer_13() { return &___myTimer_13; }
	inline void set_myTimer_13(float value)
	{
		___myTimer_13 = value;
	}

	inline static int32_t get_offset_of_bulletGenerator_14() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___bulletGenerator_14)); }
	inline BulletGenerator_t1131765196 * get_bulletGenerator_14() const { return ___bulletGenerator_14; }
	inline BulletGenerator_t1131765196 ** get_address_of_bulletGenerator_14() { return &___bulletGenerator_14; }
	inline void set_bulletGenerator_14(BulletGenerator_t1131765196 * value)
	{
		___bulletGenerator_14 = value;
		Il2CppCodeGenWriteBarrier((&___bulletGenerator_14), value);
	}

	inline static int32_t get_offset_of_Point2_15() { return static_cast<int32_t>(offsetof(LineRenderManager_t120866003, ___Point2_15)); }
	inline Vector2_t2156229523  get_Point2_15() const { return ___Point2_15; }
	inline Vector2_t2156229523 * get_address_of_Point2_15() { return &___Point2_15; }
	inline void set_Point2_15(Vector2_t2156229523  value)
	{
		___Point2_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINERENDERMANAGER_T120866003_H
#ifndef BULLETMANAGER_T1336418627_H
#define BULLETMANAGER_T1336418627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BulletManager
struct  BulletManager_t1336418627  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] BulletManager::explosion
	SpriteU5BU5D_t2581906349* ___explosion_2;
	// UnityEngine.GameObject BulletManager::gameManager
	GameObject_t1113636619 * ___gameManager_3;
	// System.Boolean BulletManager::isActive
	bool ___isActive_4;
	// UnityEngine.Vector3 BulletManager::DIR
	Vector3_t3722313464  ___DIR_5;
	// System.Single BulletManager::acceleration
	float ___acceleration_6;
	// UnityEngine.GameObject BulletManager::Bullets
	GameObject_t1113636619 * ___Bullets_7;
	// System.Single BulletManager::mass
	float ___mass_8;
	// System.Single BulletManager::timer
	float ___timer_9;
	// System.Single BulletManager::t
	float ___t_10;

public:
	inline static int32_t get_offset_of_explosion_2() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___explosion_2)); }
	inline SpriteU5BU5D_t2581906349* get_explosion_2() const { return ___explosion_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of_explosion_2() { return &___explosion_2; }
	inline void set_explosion_2(SpriteU5BU5D_t2581906349* value)
	{
		___explosion_2 = value;
		Il2CppCodeGenWriteBarrier((&___explosion_2), value);
	}

	inline static int32_t get_offset_of_gameManager_3() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___gameManager_3)); }
	inline GameObject_t1113636619 * get_gameManager_3() const { return ___gameManager_3; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_3() { return &___gameManager_3; }
	inline void set_gameManager_3(GameObject_t1113636619 * value)
	{
		___gameManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_3), value);
	}

	inline static int32_t get_offset_of_isActive_4() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___isActive_4)); }
	inline bool get_isActive_4() const { return ___isActive_4; }
	inline bool* get_address_of_isActive_4() { return &___isActive_4; }
	inline void set_isActive_4(bool value)
	{
		___isActive_4 = value;
	}

	inline static int32_t get_offset_of_DIR_5() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___DIR_5)); }
	inline Vector3_t3722313464  get_DIR_5() const { return ___DIR_5; }
	inline Vector3_t3722313464 * get_address_of_DIR_5() { return &___DIR_5; }
	inline void set_DIR_5(Vector3_t3722313464  value)
	{
		___DIR_5 = value;
	}

	inline static int32_t get_offset_of_acceleration_6() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___acceleration_6)); }
	inline float get_acceleration_6() const { return ___acceleration_6; }
	inline float* get_address_of_acceleration_6() { return &___acceleration_6; }
	inline void set_acceleration_6(float value)
	{
		___acceleration_6 = value;
	}

	inline static int32_t get_offset_of_Bullets_7() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___Bullets_7)); }
	inline GameObject_t1113636619 * get_Bullets_7() const { return ___Bullets_7; }
	inline GameObject_t1113636619 ** get_address_of_Bullets_7() { return &___Bullets_7; }
	inline void set_Bullets_7(GameObject_t1113636619 * value)
	{
		___Bullets_7 = value;
		Il2CppCodeGenWriteBarrier((&___Bullets_7), value);
	}

	inline static int32_t get_offset_of_mass_8() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___mass_8)); }
	inline float get_mass_8() const { return ___mass_8; }
	inline float* get_address_of_mass_8() { return &___mass_8; }
	inline void set_mass_8(float value)
	{
		___mass_8 = value;
	}

	inline static int32_t get_offset_of_timer_9() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___timer_9)); }
	inline float get_timer_9() const { return ___timer_9; }
	inline float* get_address_of_timer_9() { return &___timer_9; }
	inline void set_timer_9(float value)
	{
		___timer_9 = value;
	}

	inline static int32_t get_offset_of_t_10() { return static_cast<int32_t>(offsetof(BulletManager_t1336418627, ___t_10)); }
	inline float get_t_10() const { return ___t_10; }
	inline float* get_address_of_t_10() { return &___t_10; }
	inline void set_t_10(float value)
	{
		___t_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLETMANAGER_T1336418627_H
#ifndef GAMEMANAGER_T1536523654_H
#define GAMEMANAGER_T1536523654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t1536523654  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GameManager::isGameOver
	bool ___isGameOver_2;
	// System.Boolean GameManager::enemyVictory
	bool ___enemyVictory_3;
	// System.Boolean GameManager::isPaused
	bool ___isPaused_4;
	// System.Boolean GameManager::isPlay
	bool ___isPlay_5;
	// System.Int32 GameManager::score
	int32_t ___score_6;
	// System.Int32 GameManager::highscore
	int32_t ___highscore_7;
	// UnityEngine.UI.Text GameManager::highscoreText
	Text_t1901882714 * ___highscoreText_8;
	// System.String GameManager::highScoreKey
	String_t* ___highScoreKey_9;
	// UnityEngine.GameObject GameManager::Character
	GameObject_t1113636619 * ___Character_10;
	// UnityEngine.Vector3 GameManager::StartingPos
	Vector3_t3722313464  ___StartingPos_11;
	// UnityEngine.UI.Text GameManager::scoreText
	Text_t1901882714 * ___scoreText_12;

public:
	inline static int32_t get_offset_of_isGameOver_2() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___isGameOver_2)); }
	inline bool get_isGameOver_2() const { return ___isGameOver_2; }
	inline bool* get_address_of_isGameOver_2() { return &___isGameOver_2; }
	inline void set_isGameOver_2(bool value)
	{
		___isGameOver_2 = value;
	}

	inline static int32_t get_offset_of_enemyVictory_3() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___enemyVictory_3)); }
	inline bool get_enemyVictory_3() const { return ___enemyVictory_3; }
	inline bool* get_address_of_enemyVictory_3() { return &___enemyVictory_3; }
	inline void set_enemyVictory_3(bool value)
	{
		___enemyVictory_3 = value;
	}

	inline static int32_t get_offset_of_isPaused_4() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___isPaused_4)); }
	inline bool get_isPaused_4() const { return ___isPaused_4; }
	inline bool* get_address_of_isPaused_4() { return &___isPaused_4; }
	inline void set_isPaused_4(bool value)
	{
		___isPaused_4 = value;
	}

	inline static int32_t get_offset_of_isPlay_5() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___isPlay_5)); }
	inline bool get_isPlay_5() const { return ___isPlay_5; }
	inline bool* get_address_of_isPlay_5() { return &___isPlay_5; }
	inline void set_isPlay_5(bool value)
	{
		___isPlay_5 = value;
	}

	inline static int32_t get_offset_of_score_6() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___score_6)); }
	inline int32_t get_score_6() const { return ___score_6; }
	inline int32_t* get_address_of_score_6() { return &___score_6; }
	inline void set_score_6(int32_t value)
	{
		___score_6 = value;
	}

	inline static int32_t get_offset_of_highscore_7() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___highscore_7)); }
	inline int32_t get_highscore_7() const { return ___highscore_7; }
	inline int32_t* get_address_of_highscore_7() { return &___highscore_7; }
	inline void set_highscore_7(int32_t value)
	{
		___highscore_7 = value;
	}

	inline static int32_t get_offset_of_highscoreText_8() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___highscoreText_8)); }
	inline Text_t1901882714 * get_highscoreText_8() const { return ___highscoreText_8; }
	inline Text_t1901882714 ** get_address_of_highscoreText_8() { return &___highscoreText_8; }
	inline void set_highscoreText_8(Text_t1901882714 * value)
	{
		___highscoreText_8 = value;
		Il2CppCodeGenWriteBarrier((&___highscoreText_8), value);
	}

	inline static int32_t get_offset_of_highScoreKey_9() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___highScoreKey_9)); }
	inline String_t* get_highScoreKey_9() const { return ___highScoreKey_9; }
	inline String_t** get_address_of_highScoreKey_9() { return &___highScoreKey_9; }
	inline void set_highScoreKey_9(String_t* value)
	{
		___highScoreKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___highScoreKey_9), value);
	}

	inline static int32_t get_offset_of_Character_10() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Character_10)); }
	inline GameObject_t1113636619 * get_Character_10() const { return ___Character_10; }
	inline GameObject_t1113636619 ** get_address_of_Character_10() { return &___Character_10; }
	inline void set_Character_10(GameObject_t1113636619 * value)
	{
		___Character_10 = value;
		Il2CppCodeGenWriteBarrier((&___Character_10), value);
	}

	inline static int32_t get_offset_of_StartingPos_11() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___StartingPos_11)); }
	inline Vector3_t3722313464  get_StartingPos_11() const { return ___StartingPos_11; }
	inline Vector3_t3722313464 * get_address_of_StartingPos_11() { return &___StartingPos_11; }
	inline void set_StartingPos_11(Vector3_t3722313464  value)
	{
		___StartingPos_11 = value;
	}

	inline static int32_t get_offset_of_scoreText_12() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___scoreText_12)); }
	inline Text_t1901882714 * get_scoreText_12() const { return ___scoreText_12; }
	inline Text_t1901882714 ** get_address_of_scoreText_12() { return &___scoreText_12; }
	inline void set_scoreText_12(Text_t1901882714 * value)
	{
		___scoreText_12 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T1536523654_H
#ifndef MESSAGEMANAGER_T336836297_H
#define MESSAGEMANAGER_T336836297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageManager
struct  MessageManager_t336836297  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MessageManager::gameManager
	GameObject_t1113636619 * ___gameManager_2;
	// UnityEngine.GameObject MessageManager::message
	GameObject_t1113636619 * ___message_3;
	// UnityEngine.Sprite[] MessageManager::countdown
	SpriteU5BU5D_t2581906349* ___countdown_4;
	// UnityEngine.Sprite MessageManager::pause
	Sprite_t280657092 * ___pause_5;

public:
	inline static int32_t get_offset_of_gameManager_2() { return static_cast<int32_t>(offsetof(MessageManager_t336836297, ___gameManager_2)); }
	inline GameObject_t1113636619 * get_gameManager_2() const { return ___gameManager_2; }
	inline GameObject_t1113636619 ** get_address_of_gameManager_2() { return &___gameManager_2; }
	inline void set_gameManager_2(GameObject_t1113636619 * value)
	{
		___gameManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_2), value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(MessageManager_t336836297, ___message_3)); }
	inline GameObject_t1113636619 * get_message_3() const { return ___message_3; }
	inline GameObject_t1113636619 ** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(GameObject_t1113636619 * value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier((&___message_3), value);
	}

	inline static int32_t get_offset_of_countdown_4() { return static_cast<int32_t>(offsetof(MessageManager_t336836297, ___countdown_4)); }
	inline SpriteU5BU5D_t2581906349* get_countdown_4() const { return ___countdown_4; }
	inline SpriteU5BU5D_t2581906349** get_address_of_countdown_4() { return &___countdown_4; }
	inline void set_countdown_4(SpriteU5BU5D_t2581906349* value)
	{
		___countdown_4 = value;
		Il2CppCodeGenWriteBarrier((&___countdown_4), value);
	}

	inline static int32_t get_offset_of_pause_5() { return static_cast<int32_t>(offsetof(MessageManager_t336836297, ___pause_5)); }
	inline Sprite_t280657092 * get_pause_5() const { return ___pause_5; }
	inline Sprite_t280657092 ** get_address_of_pause_5() { return &___pause_5; }
	inline void set_pause_5(Sprite_t280657092 * value)
	{
		___pause_5 = value;
		Il2CppCodeGenWriteBarrier((&___pause_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEMANAGER_T336836297_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_8() const { return ___m_CanvasRenderer_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_8() { return &___m_CanvasRenderer_8; }
	inline void set_m_CanvasRenderer_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_29)); }
	inline Sprite_t280657092 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t280657092 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_30)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Sprite_t280657092 * m_Items[1];

public:
	inline Sprite_t280657092 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_t280657092 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Sprite_t280657092 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_t280657092 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m1135049463_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::set_tag(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject_set_tag_m2353670106 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<GameManager>()
#define GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(__this, method) ((  GameManager_t1536523654 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<BulletManager>()
#define GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801(__this, method) ((  BulletManager_t1336418627 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.String System.Single::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Single_ToString_m3947131094 (float* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m1809518182 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* p0, const RuntimeMethod* method);
// System.String UnityEngine.Vector3::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Vector3_ToString_m759076600 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<LineRenderManager>()
#define GameObject_GetComponent_TisLineRenderManager_t120866003_m531631259(__this, method) ((  LineRenderManager_t120866003 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1113636619_m3006960551(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Vector3_t3722313464 , Quaternion_t2301928331 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1135049463_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Transform_SetParent_m273471670 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, bool p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_one_m1629952498 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localScale_m3053443106 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void BulletManager::MissileFire()
extern "C" IL2CPP_METHOD_ATTR void BulletManager_MissileFire_m3246930753 (BulletManager_t1336418627 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_down_m3781355428 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_localPosition_m4234289348 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localPosition_m4128471975 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_eulerAngles_m135219616 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::CompareTag(System.String)
extern "C" IL2CPP_METHOD_ATTR bool GameObject_CompareTag_m3144439756 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<EnemyManager>()
#define GameObject_GetComponent_TisEnemyManager_t913934216_m743859818(__this, method) ((  EnemyManager_t913934216 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Collections.IEnumerator BulletManager::CoroutineExplosionAnimation(System.Single,System.Single,UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* BulletManager_CoroutineExplosionAnimation_m683039687 (BulletManager_t1336418627 * __this, float ___firstTerm0, float ___tTerm1, GameObject_t1113636619 * ___other2, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void EnemyManager::CallCoroutineCloudeFade()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager_CallCoroutineCloudeFade_m1288317783 (EnemyManager_t913934216 * __this, const RuntimeMethod* method);
// System.Void GameManager::UpdateScore()
extern "C" IL2CPP_METHOD_ATTR void GameManager_UpdateScore_m509300891 (GameManager_t1536523654 * __this, const RuntimeMethod* method);
// System.Void BulletManager/<CoroutineExplosionAnimation>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineExplosionAnimationU3Ec__Iterator0__ctor_m3449173907 (U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2670269651_m2486712510(__this, method) ((  Image_t2670269651 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Time_set_timeScale_m1127545364 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m1758133949 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void CharacterManager::CharacterInit()
extern "C" IL2CPP_METHOD_ATTR void CharacterManager_CharacterInit_m1829296805 (CharacterManager_t2849944402 * __this, const RuntimeMethod* method);
// System.Void CharacterManager::CharacterMove()
extern "C" IL2CPP_METHOD_ATTR void CharacterManager_CharacterMove_m97125875 (CharacterManager_t2849944402 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_up_m3584168373 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void EnemyGenerator::GenerateEnemy()
extern "C" IL2CPP_METHOD_ATTR void EnemyGenerator_GenerateEnemy_m557526776 (EnemyGenerator_t2446750330 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Random_Range_m2202990745 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void EnemyManager::EnemyInit()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager_EnemyInit_m1387525382 (EnemyManager_t913934216 * __this, const RuntimeMethod* method);
// System.Void EnemyManager::ExplodeDie()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager_ExplodeDie_m497504152 (EnemyManager_t913934216 * __this, const RuntimeMethod* method);
// System.Void EnemyManager::EnemyMove()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager_EnemyMove_m86952508 (EnemyManager_t913934216 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_Find_m1729760951 (Transform_t3600365921 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2670269651_m980647750(__this, method) ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Collections.IEnumerator EnemyManager::CoroutineCloudFade(System.Single,System.Single,UnityEngine.Color,UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* EnemyManager_CoroutineCloudFade_m2978522574 (EnemyManager_t913934216 * __this, float ___firstTerm0, float ___tTerm1, Color_t2555686324  ___tmpColor2, Transform_t3600365921 * ___cloud3, const RuntimeMethod* method);
// System.Void EnemyManager/<CoroutineCloudFade>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineCloudFadeU3Ec__Iterator0__ctor_m204138362 (U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_right_m1913784872 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void GameManager::GameOver()
extern "C" IL2CPP_METHOD_ATTR void GameManager_GameOver_m2317540222 (GameManager_t1536523654 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, method) ((  Rigidbody2D_t939494601 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Single UnityEngine.Time::get_time()
extern "C" IL2CPP_METHOD_ATTR float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void GameManager::GameManagerInit()
extern "C" IL2CPP_METHOD_ATTR void GameManager_GameManagerInit_m1907792971 (GameManager_t1536523654 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m1299643124 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.String System.Int32::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method);
// System.Void GameManager::UpdateHighscore()
extern "C" IL2CPP_METHOD_ATTR void GameManager_UpdateHighscore_m3829313171 (GameManager_t1536523654 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<CharacterManager>()
#define GameObject_GetComponent_TisCharacterManager_t2849944402_m3321491559(__this, method) ((  CharacterManager_t2849944402 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_m2842000469 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::Save()
extern "C" IL2CPP_METHOD_ATTR void PlayerPrefs_Save_m2701929462 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void LineRenderManager::LineRenderManagerInit()
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_LineRenderManagerInit_m2547092565 (LineRenderManager_t120866003 * __this, const RuntimeMethod* method);
// System.Void LineRenderManager::UpdatePosition()
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_UpdatePosition_m280241269 (LineRenderManager_t120866003 * __this, const RuntimeMethod* method);
// System.Void LineRenderManager::SetPosition()
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_SetPosition_m851472669 (LineRenderManager_t120866003 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Subtraction_m73004381 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_get_normalized_m2683665860 (Vector2_t2156229523 * __this, const RuntimeMethod* method);
// System.Void BulletGenerator::GenerateBullet(UnityEngine.Vector2,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void BulletGenerator_GenerateBullet_m1172315483 (BulletGenerator_t1131765196 * __this, Vector2_t2156229523  ___Direction0, Vector3_t3722313464  ___Position1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// System.Void LineRenderManager::LineRenderSetPos(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_LineRenderSetPos_m3403892388 (LineRenderManager_t120866003 * __this, Vector2_t2156229523  ___Pos10, Vector2_t2156229523  ___Pos21, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.LineRenderer>()
#define GameObject_GetComponent_TisLineRenderer_t3154350270_m1162201727(__this, method) ((  LineRenderer_t3154350270 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.LineRenderer>()
#define GameObject_AddComponent_TisLineRenderer_t3154350270_m2920077551(__this, method) ((  LineRenderer_t3154350270 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.LineRenderer::set_startWidth(System.Single)
extern "C" IL2CPP_METHOD_ATTR void LineRenderer_set_startWidth_m1093267133 (LineRenderer_t3154350270 * __this, float p0, const RuntimeMethod* method);
// System.Void UnityEngine.LineRenderer::set_endWidth(System.Single)
extern "C" IL2CPP_METHOD_ATTR void LineRenderer_set_endWidth_m4252049505 (LineRenderer_t3154350270 * __this, float p0, const RuntimeMethod* method);
// System.Void UnityEngine.LineRenderer::SetPosition(System.Int32,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void LineRenderer_SetPosition_m2111131184 (LineRenderer_t3154350270 * __this, int32_t p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Collections.IEnumerator MessageManager::CoroutineCountDownAnimation(System.Single,System.Single,UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MessageManager_CoroutineCountDownAnimation_m3397114711 (MessageManager_t336836297 * __this, float ___firstTerm0, float ___tTerm1, GameObject_t1113636619 * ___other2, const RuntimeMethod* method);
// System.Void MessageManager/<CoroutineCountDownAnimation>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineCountDownAnimationU3Ec__Iterator0__ctor_m1450384833 (U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * __this, const RuntimeMethod* method);
// System.Void MoveManager::EnemyInit()
extern "C" IL2CPP_METHOD_ATTR void MoveManager_EnemyInit_m4095141009 (MoveManager_t918348315 * __this, const RuntimeMethod* method);
// System.Void MoveManager::EnemyMove()
extern "C" IL2CPP_METHOD_ATTR void MoveManager_EnemyMove_m400884860 (MoveManager_t918348315 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_left_m2428419009 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BulletGenerator::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BulletGenerator__ctor_m3584105840 (BulletGenerator_t1131765196 * __this, const RuntimeMethod* method)
{
	{
		__this->set_timeLeft_7((3.0f));
		__this->set_initialVelocity_8((10.0f));
		__this->set_power_9((3.0f));
		__this->set_acceleration_10((10.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BulletGenerator::Start()
extern "C" IL2CPP_METHOD_ATTR void BulletGenerator_Start_m1018530826 (BulletGenerator_t1131765196 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BulletGenerator_Start_m1018530826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_missileExist_16((bool)0);
		GameObject_t1113636619 * L_0 = __this->get_oldMissile_2();
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m1369836730(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_set_tag_m2353670106(L_2, _stringLiteral1926007183, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BulletGenerator::Update()
extern "C" IL2CPP_METHOD_ATTR void BulletGenerator_Update_m3144833810 (BulletGenerator_t1131765196 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BulletGenerator_Update_m3144833810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		GameObject_t1113636619 * L_0 = __this->get_gameManager_6();
		NullCheck(L_0);
		GameManager_t1536523654 * L_1 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_2 = L_1->get_isPlay_5();
		if (!L_2)
		{
			goto IL_0130;
		}
	}
	{
		bool L_3 = __this->get_missileExist_16();
		if (!L_3)
		{
			goto IL_0130;
		}
	}
	{
		Text_t1901882714 * L_4 = __this->get_TimeText_14();
		GameObject_t1113636619 * L_5 = __this->get_newMissile_4();
		NullCheck(L_5);
		BulletManager_t1336418627 * L_6 = GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801(L_5, /*hidden argument*/GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801_RuntimeMethod_var);
		NullCheck(L_6);
		float* L_7 = L_6->get_address_of_timer_9();
		String_t* L_8 = Single_ToString_m3947131094((float*)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2180951049, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_9);
		Text_t1901882714 * L_10 = __this->get_DirText_12();
		StringU5BU5D_t1281789340* L_11 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t1281789340* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral2428460039);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2428460039);
		StringU5BU5D_t1281789340* L_13 = L_12;
		GameObject_t1113636619 * L_14 = __this->get_newMissile_4();
		NullCheck(L_14);
		BulletManager_t1336418627 * L_15 = GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801(L_14, /*hidden argument*/GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801_RuntimeMethod_var);
		NullCheck(L_15);
		Vector3_t3722313464 * L_16 = L_15->get_address_of_DIR_5();
		float L_17 = L_16->get_x_1();
		GameObject_t1113636619 * L_18 = __this->get_newMissile_4();
		NullCheck(L_18);
		Transform_t3600365921 * L_19 = GameObject_get_transform_m1369836730(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t3722313464  L_20 = Transform_get_position_m36019626(L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		float L_21 = (&V_0)->get_x_1();
		V_1 = ((float)il2cpp_codegen_add((float)L_17, (float)L_21));
		String_t* L_22 = Single_ToString_m3947131094((float*)(&V_1), /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_22);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_22);
		StringU5BU5D_t1281789340* L_23 = L_13;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3786318026);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3786318026);
		StringU5BU5D_t1281789340* L_24 = L_23;
		GameObject_t1113636619 * L_25 = __this->get_newMissile_4();
		NullCheck(L_25);
		BulletManager_t1336418627 * L_26 = GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801(L_25, /*hidden argument*/GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801_RuntimeMethod_var);
		NullCheck(L_26);
		Vector3_t3722313464 * L_27 = L_26->get_address_of_DIR_5();
		float L_28 = L_27->get_y_2();
		GameObject_t1113636619 * L_29 = __this->get_newMissile_4();
		NullCheck(L_29);
		Transform_t3600365921 * L_30 = GameObject_get_transform_m1369836730(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t3722313464  L_31 = Transform_get_position_m36019626(L_30, /*hidden argument*/NULL);
		V_2 = L_31;
		float L_32 = (&V_2)->get_y_2();
		V_3 = ((float)il2cpp_codegen_add((float)L_28, (float)L_32));
		String_t* L_33 = Single_ToString_m3947131094((float*)(&V_3), /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_33);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_33);
		StringU5BU5D_t1281789340* L_34 = L_24;
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, _stringLiteral3452614535);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614535);
		String_t* L_35 = String_Concat_m1809518182(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_35);
		Text_t1901882714 * L_36 = __this->get_LocText_11();
		GameObject_t1113636619 * L_37 = __this->get_newMissile_4();
		NullCheck(L_37);
		Transform_t3600365921 * L_38 = GameObject_get_transform_m1369836730(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector3_t3722313464  L_39 = Transform_get_position_m36019626(L_38, /*hidden argument*/NULL);
		V_4 = L_39;
		String_t* L_40 = Vector3_ToString_m759076600((Vector3_t3722313464 *)(&V_4), /*hidden argument*/NULL);
		String_t* L_41 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2496649772, L_40, /*hidden argument*/NULL);
		NullCheck(L_36);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_36, L_41);
	}

IL_0130:
	{
		return;
	}
}
// System.Void BulletGenerator::GenerateBullet(UnityEngine.Vector2,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void BulletGenerator_GenerateBullet_m1172315483 (BulletGenerator_t1131765196 * __this, Vector2_t2156229523  ___Direction0, Vector3_t3722313464  ___Position1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BulletGenerator_GenerateBullet_m1172315483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		__this->set_missileExist_16((bool)1);
		Text_t1901882714 * L_0 = __this->get_AngText_13();
		GameObject_t1113636619 * L_1 = __this->get_lineRenderer_3();
		NullCheck(L_1);
		LineRenderManager_t120866003 * L_2 = GameObject_GetComponent_TisLineRenderManager_t120866003_m531631259(L_1, /*hidden argument*/GameObject_GetComponent_TisLineRenderManager_t120866003_m531631259_RuntimeMethod_var);
		NullCheck(L_2);
		float L_3 = L_2->get_ShootAngle_7();
		V_0 = ((float)il2cpp_codegen_subtract((float)(90.0f), (float)L_3));
		String_t* L_4 = Single_ToString_m3947131094((float*)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1452322613, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		GameObject_t1113636619 * L_6 = __this->get_oldMissile_2();
		Vector3_t3722313464  L_7 = ___Position1;
		GameObject_t1113636619 * L_8 = __this->get_oldMissile_2();
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = GameObject_get_transform_m1369836730(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t2301928331  L_10 = Transform_get_rotation_m3502953881(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_11 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_6, L_7, L_10, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		__this->set_newMissile_4(L_11);
		GameObject_t1113636619 * L_12 = __this->get_newMissile_4();
		NullCheck(L_12);
		BulletManager_t1336418627 * L_13 = GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801(L_12, /*hidden argument*/GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801_RuntimeMethod_var);
		Vector2_t2156229523  L_14 = ___Direction0;
		float L_15 = __this->get_power_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_16 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		Vector3_t3722313464  L_17 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_13);
		L_13->set_DIR_5(L_17);
		GameObject_t1113636619 * L_18 = __this->get_newMissile_4();
		NullCheck(L_18);
		BulletManager_t1336418627 * L_19 = GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801(L_18, /*hidden argument*/GameObject_GetComponent_TisBulletManager_t1336418627_m3684731801_RuntimeMethod_var);
		NullCheck(L_19);
		L_19->set_isActive_4((bool)1);
		GameObject_t1113636619 * L_20 = __this->get_newMissile_4();
		NullCheck(L_20);
		Transform_t3600365921 * L_21 = GameObject_get_transform_m1369836730(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		GameObject_t1113636619 * L_22 = Component_get_gameObject_m442555142(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		GameObject_set_tag_m2353670106(L_22, _stringLiteral3911666897, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_23 = __this->get_newMissile_4();
		NullCheck(L_23);
		Transform_t3600365921 * L_24 = GameObject_get_transform_m1369836730(L_23, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_25 = __this->get_Bullets_5();
		NullCheck(L_25);
		Transform_t3600365921 * L_26 = GameObject_get_transform_m1369836730(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_SetParent_m273471670(L_24, L_26, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_27 = __this->get_newMissile_4();
		NullCheck(L_27);
		Transform_t3600365921 * L_28 = GameObject_get_transform_m1369836730(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_29 = Vector3_get_one_m1629952498(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_localScale_m3053443106(L_28, L_29, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BulletManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BulletManager__ctor_m3187396227 (BulletManager_t1336418627 * __this, const RuntimeMethod* method)
{
	{
		__this->set_acceleration_6((1.0f));
		__this->set_mass_8((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BulletManager::Start()
extern "C" IL2CPP_METHOD_ATTR void BulletManager_Start_m1118672579 (BulletManager_t1336418627 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void BulletManager::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void BulletManager_FixedUpdate_m1695692149 (BulletManager_t1336418627 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BulletManager_FixedUpdate_m1695692149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isActive_4();
		if (!L_0)
		{
			goto IL_0038;
		}
	}
	{
		GameObject_t1113636619 * L_1 = __this->get_gameManager_3();
		NullCheck(L_1);
		GameManager_t1536523654 * L_2 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_1, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_2);
		bool L_3 = L_2->get_isGameOver_2();
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		float L_4 = __this->get_timer_9();
		float L_5 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_9(((float)il2cpp_codegen_add((float)L_4, (float)L_5)));
		BulletManager_MissileFire_m3246930753(__this, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void BulletManager::MissileFire()
extern "C" IL2CPP_METHOD_ATTR void BulletManager_MissileFire_m3246930753 (BulletManager_t1336418627 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BulletManager_MissileFire_m3246930753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_isActive_4();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Vector3_t3722313464  L_1 = __this->get_DIR_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_get_down_m3781355428(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_acceleration_6();
		Vector3_t3722313464  L_4 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		float L_5 = __this->get_mass_8();
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		__this->set_DIR_5(L_7);
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_9 = L_8;
		NullCheck(L_9);
		Vector3_t3722313464  L_10 = Transform_get_localPosition_m4234289348(L_9, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = __this->get_DIR_5();
		Vector3_t3722313464  L_12 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localPosition_m4128471975(L_9, L_12, /*hidden argument*/NULL);
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464 * L_14 = __this->get_address_of_DIR_5();
		float L_15 = L_14->get_y_2();
		Vector3_t3722313464 * L_16 = __this->get_address_of_DIR_5();
		float L_17 = L_16->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_18 = atanf(((float)((float)L_15/(float)L_17)));
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m3353183577((&L_19), (0.0f), (0.0f), (((float)((float)((double)((double)(((double)((double)((float)il2cpp_codegen_multiply((float)L_18, (float)(180.0f))))))/(double)(3.1415926535897931)))))), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_eulerAngles_m135219616(L_13, L_19, /*hidden argument*/NULL);
		Transform_t3600365921 * L_20 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t3722313464  L_21 = Transform_get_localPosition_m4234289348(L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		float L_22 = (&V_0)->get_y_2();
		if ((!(((float)L_22) < ((float)(-115.0f)))))
		{
			goto IL_00c5;
		}
	}
	{
		GameObject_t1113636619 * L_23 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameObject_SetActive_m796801857(L_23, (bool)0, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		return;
	}
}
// System.Void BulletManager::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void BulletManager_OnTriggerEnter2D_m12608645 (BulletManager_t1336418627 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BulletManager_OnTriggerEnter2D_m12608645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GameObject_CompareTag_m3144439756(L_1, _stringLiteral3911666897, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00ee;
		}
	}
	{
		Collider2D_t2806799626 * L_3 = ___other0;
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_CompareTag_m3144439756(L_4, _stringLiteral1926007183, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_00ee;
		}
	}
	{
		Collider2D_t2806799626 * L_6 = ___other0;
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = GameObject_CompareTag_m3144439756(L_7, _stringLiteral3911666897, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_00ee;
		}
	}
	{
		__this->set_isActive_4((bool)0);
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_t1113636619 * L_10 = Component_get_gameObject_m442555142(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_set_tag_m2353670106(L_10, _stringLiteral2503331403, /*hidden argument*/NULL);
		Collider2D_t2806799626 * L_11 = ___other0;
		NullCheck(L_11);
		GameObject_t1113636619 * L_12 = Component_get_gameObject_m442555142(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		EnemyManager_t913934216 * L_13 = GameObject_GetComponent_TisEnemyManager_t913934216_m743859818(L_12, /*hidden argument*/GameObject_GetComponent_TisEnemyManager_t913934216_m743859818_RuntimeMethod_var);
		NullCheck(L_13);
		L_13->set_isActive_3((bool)0);
		Collider2D_t2806799626 * L_14 = ___other0;
		NullCheck(L_14);
		GameObject_t1113636619 * L_15 = Component_get_gameObject_m442555142(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		EnemyManager_t913934216 * L_16 = GameObject_GetComponent_TisEnemyManager_t913934216_m743859818(L_15, /*hidden argument*/GameObject_GetComponent_TisEnemyManager_t913934216_m743859818_RuntimeMethod_var);
		NullCheck(L_16);
		L_16->set_isHit_4((bool)1);
		Collider2D_t2806799626 * L_17 = ___other0;
		NullCheck(L_17);
		GameObject_t1113636619 * L_18 = Component_get_gameObject_m442555142(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		EnemyManager_t913934216 * L_19 = GameObject_GetComponent_TisEnemyManager_t913934216_m743859818(L_18, /*hidden argument*/GameObject_GetComponent_TisEnemyManager_t913934216_m743859818_RuntimeMethod_var);
		Vector3_t3722313464  L_20 = __this->get_DIR_5();
		NullCheck(L_19);
		L_19->set_fallDir_8(L_20);
		Collider2D_t2806799626 * L_21 = ___other0;
		NullCheck(L_21);
		GameObject_t1113636619 * L_22 = Component_get_gameObject_m442555142(L_21, /*hidden argument*/NULL);
		RuntimeObject* L_23 = BulletManager_CoroutineExplosionAnimation_m683039687(__this, (0.0f), (0.09f), L_22, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_23, /*hidden argument*/NULL);
		Collider2D_t2806799626 * L_24 = ___other0;
		NullCheck(L_24);
		GameObject_t1113636619 * L_25 = Component_get_gameObject_m442555142(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		EnemyManager_t913934216 * L_26 = GameObject_GetComponent_TisEnemyManager_t913934216_m743859818(L_25, /*hidden argument*/GameObject_GetComponent_TisEnemyManager_t913934216_m743859818_RuntimeMethod_var);
		NullCheck(L_26);
		EnemyManager_CallCoroutineCloudeFade_m1288317783(L_26, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_27 = __this->get_gameManager_3();
		NullCheck(L_27);
		GameManager_t1536523654 * L_28 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_27, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		GameManager_t1536523654 * L_29 = L_28;
		NullCheck(L_29);
		int32_t L_30 = L_29->get_score_6();
		NullCheck(L_29);
		L_29->set_score_6(((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)((int32_t)10))));
		GameObject_t1113636619 * L_31 = __this->get_gameManager_3();
		NullCheck(L_31);
		GameManager_t1536523654 * L_32 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_31, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_32);
		GameManager_UpdateScore_m509300891(L_32, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		return;
	}
}
// System.Collections.IEnumerator BulletManager::CoroutineExplosionAnimation(System.Single,System.Single,UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* BulletManager_CoroutineExplosionAnimation_m683039687 (BulletManager_t1336418627 * __this, float ___firstTerm0, float ___tTerm1, GameObject_t1113636619 * ___other2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BulletManager_CoroutineExplosionAnimation_m683039687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * V_0 = NULL;
	{
		U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * L_0 = (U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 *)il2cpp_codegen_object_new(U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679_il2cpp_TypeInfo_var);
		U3CCoroutineExplosionAnimationU3Ec__Iterator0__ctor_m3449173907(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * L_1 = V_0;
		float L_2 = ___firstTerm0;
		NullCheck(L_1);
		L_1->set_firstTerm_0(L_2);
		U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * L_3 = V_0;
		float L_4 = ___tTerm1;
		NullCheck(L_3);
		L_3->set_tTerm_1(L_4);
		U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U24this_4(__this);
		U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * L_6 = V_0;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BulletManager/<CoroutineExplosionAnimation>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineExplosionAnimationU3Ec__Iterator0__ctor_m3449173907 (U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean BulletManager/<CoroutineExplosionAnimation>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CCoroutineExplosionAnimationU3Ec__Iterator0_MoveNext_m459838247 (U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCoroutineExplosionAnimationU3Ec__Iterator0_MoveNext_m459838247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_004a;
			}
			case 2:
			{
				goto IL_00cd;
			}
		}
	}
	{
		goto IL_0110;
	}

IL_0025:
	{
		float L_2 = __this->get_firstTerm_0();
		WaitForSeconds_t1699091251 * L_3 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_5(L_3);
		bool L_4 = __this->get_U24disposing_6();
		if (L_4)
		{
			goto IL_0045;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0045:
	{
		goto IL_0112;
	}

IL_004a:
	{
		float L_5 = __this->get_tTerm_1();
		WaitForSeconds_t1699091251 * L_6 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U3CtWFSU3E__0_2(L_6);
		__this->set_U3CindexU3E__1_3(0);
		goto IL_00db;
	}

IL_0067:
	{
		BulletManager_t1336418627 * L_7 = __this->get_U24this_4();
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Image_t2670269651 * L_10 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_9, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		BulletManager_t1336418627 * L_11 = __this->get_U24this_4();
		NullCheck(L_11);
		SpriteU5BU5D_t2581906349* L_12 = L_11->get_explosion_2();
		int32_t L_13 = __this->get_U3CindexU3E__1_3();
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Sprite_t280657092 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_10);
		Image_set_sprite_m2369174689(L_10, L_15, /*hidden argument*/NULL);
		BulletManager_t1336418627 * L_16 = __this->get_U24this_4();
		NullCheck(L_16);
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		GameObject_t1113636619 * L_18 = Component_get_gameObject_m442555142(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Image_t2670269651 * L_19 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_18, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		NullCheck(L_19);
		VirtActionInvoker0::Invoke(45 /* System.Void UnityEngine.UI.Graphic::SetNativeSize() */, L_19);
		WaitForSeconds_t1699091251 * L_20 = __this->get_U3CtWFSU3E__0_2();
		__this->set_U24current_5(L_20);
		bool L_21 = __this->get_U24disposing_6();
		if (L_21)
		{
			goto IL_00c8;
		}
	}
	{
		__this->set_U24PC_7(2);
	}

IL_00c8:
	{
		goto IL_0112;
	}

IL_00cd:
	{
		int32_t L_22 = __this->get_U3CindexU3E__1_3();
		__this->set_U3CindexU3E__1_3(((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1)));
	}

IL_00db:
	{
		int32_t L_23 = __this->get_U3CindexU3E__1_3();
		BulletManager_t1336418627 * L_24 = __this->get_U24this_4();
		NullCheck(L_24);
		SpriteU5BU5D_t2581906349* L_25 = L_24->get_explosion_2();
		NullCheck(L_25);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_25)->max_length)))))))
		{
			goto IL_0067;
		}
	}
	{
		BulletManager_t1336418627 * L_26 = __this->get_U24this_4();
		NullCheck(L_26);
		Transform_t3600365921 * L_27 = Component_get_transform_m3162698980(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_t1113636619 * L_28 = Component_get_gameObject_m442555142(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		GameObject_SetActive_m796801857(L_28, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_7((-1));
	}

IL_0110:
	{
		return (bool)0;
	}

IL_0112:
	{
		return (bool)1;
	}
}
// System.Object BulletManager/<CoroutineExplosionAnimation>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCoroutineExplosionAnimationU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2094707529 (U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object BulletManager/<CoroutineExplosionAnimation>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCoroutineExplosionAnimationU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3114098654 (U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void BulletManager/<CoroutineExplosionAnimation>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineExplosionAnimationU3Ec__Iterator0_Dispose_m2580218645 (U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void BulletManager/<CoroutineExplosionAnimation>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineExplosionAnimationU3Ec__Iterator0_Reset_m3053651895 (U3CCoroutineExplosionAnimationU3Ec__Iterator0_t2727895679 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCoroutineExplosionAnimationU3Ec__Iterator0_Reset_m3053651895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CCoroutineExplosionAnimationU3Ec__Iterator0_Reset_m3053651895_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ButtonManager__ctor_m2848978668 (ButtonManager_t2018100028 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonManager::Start()
extern "C" IL2CPP_METHOD_ATTR void ButtonManager_Start_m1850864424 (ButtonManager_t2018100028 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ButtonManager::Update()
extern "C" IL2CPP_METHOD_ATTR void ButtonManager_Update_m2097256942 (ButtonManager_t2018100028 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ButtonManager::ButtonClick()
extern "C" IL2CPP_METHOD_ATTR void ButtonManager_ButtonClick_m363216488 (ButtonManager_t2018100028 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonManager_ButtonClick_m363216488_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral3105844272, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterManager__ctor_m2193233670 (CharacterManager_t2849944402 * __this, const RuntimeMethod* method)
{
	{
		__this->set_isActive_2((bool)1);
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (103.6f), (3.9f), (0.0f), /*hidden argument*/NULL);
		__this->set_StartingPos_7(L_0);
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3353183577((&L_1), (110.0f), (75.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_YposMax_8(L_1);
		Vector3_t3722313464  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m3353183577((&L_2), (110.0f), (-75.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_YposMin_9(L_2);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterManager::Start()
extern "C" IL2CPP_METHOD_ATTR void CharacterManager_Start_m3671098996 (CharacterManager_t2849944402 * __this, const RuntimeMethod* method)
{
	{
		CharacterManager_CharacterInit_m1829296805(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterManager::Update()
extern "C" IL2CPP_METHOD_ATTR void CharacterManager_Update_m1700690672 (CharacterManager_t2849944402 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterManager_Update_m1700690672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isActive_2();
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t1113636619 * L_1 = __this->get_gameManager_4();
		NullCheck(L_1);
		GameManager_t1536523654 * L_2 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_1, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_2);
		bool L_3 = L_2->get_isGameOver_2();
		if (L_3)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t1113636619 * L_4 = __this->get_gameManager_4();
		NullCheck(L_4);
		GameManager_t1536523654 * L_5 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_4, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_5);
		bool L_6 = L_5->get_isPaused_4();
		if (L_6)
		{
			goto IL_003b;
		}
	}
	{
		CharacterManager_CharacterMove_m97125875(__this, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void CharacterManager::CharacterMove()
extern "C" IL2CPP_METHOD_ATTR void CharacterManager_CharacterMove_m97125875 (CharacterManager_t2849944402 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterManager_CharacterMove_m97125875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_direction_3();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0046;
		}
	}
	{
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = L_1;
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_localPosition_m4234289348(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_characterSpeed_5();
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, (30.0f), /*hidden argument*/NULL);
		float L_8 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_3, L_9, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localPosition_m4128471975(L_2, L_10, /*hidden argument*/NULL);
	}

IL_0046:
	{
		int32_t L_11 = __this->get_direction_3();
		if ((!(((uint32_t)L_11) == ((uint32_t)(-1)))))
		{
			goto IL_008c;
		}
	}
	{
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_13 = L_12;
		NullCheck(L_13);
		Vector3_t3722313464  L_14 = Transform_get_localPosition_m4234289348(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_15 = Vector3_get_down_m3781355428(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = __this->get_characterSpeed_5();
		Vector3_t3722313464  L_17 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_17, (30.0f), /*hidden argument*/NULL);
		float L_19 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		Vector3_t3722313464  L_21 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_14, L_20, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localPosition_m4128471975(L_13, L_21, /*hidden argument*/NULL);
	}

IL_008c:
	{
		Transform_t3600365921 * L_22 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t3722313464  L_23 = Transform_get_localPosition_m4234289348(L_22, /*hidden argument*/NULL);
		V_0 = L_23;
		float L_24 = (&V_0)->get_y_2();
		if ((!(((float)L_24) < ((float)(-75.0f)))))
		{
			goto IL_00b0;
		}
	}
	{
		__this->set_direction_3(1);
	}

IL_00b0:
	{
		Transform_t3600365921 * L_25 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t3722313464  L_26 = Transform_get_localPosition_m4234289348(L_25, /*hidden argument*/NULL);
		V_1 = L_26;
		float L_27 = (&V_1)->get_y_2();
		if ((!(((float)L_27) > ((float)(75.0f)))))
		{
			goto IL_00d4;
		}
	}
	{
		__this->set_direction_3((-1));
	}

IL_00d4:
	{
		return;
	}
}
// System.Void CharacterManager::CharacterInit()
extern "C" IL2CPP_METHOD_ATTR void CharacterManager_CharacterInit_m1829296805 (CharacterManager_t2849944402 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterManager_CharacterInit_m1829296805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_gameManager_4();
		NullCheck(L_0);
		GameManager_t1536523654 * L_1 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_1);
		L_1->set_isPaused_4((bool)1);
		GameObject_t1113636619 * L_2 = __this->get_CharacterPosition_6();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730(L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = __this->get_StartingPos_7();
		NullCheck(L_3);
		Transform_set_localPosition_m4128471975(L_3, L_4, /*hidden argument*/NULL);
		__this->set_characterSpeed_5((1.2f));
		__this->set_direction_3(1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyGenerator::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EnemyGenerator__ctor_m1413812197 (EnemyGenerator_t2446750330 * __this, const RuntimeMethod* method)
{
	{
		__this->set_spawnTerm_8((5.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyGenerator::Start()
extern "C" IL2CPP_METHOD_ATTR void EnemyGenerator_Start_m913836027 (EnemyGenerator_t2446750330 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void EnemyGenerator::Update()
extern "C" IL2CPP_METHOD_ATTR void EnemyGenerator_Update_m1945251961 (EnemyGenerator_t2446750330 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyGenerator_Update_m1945251961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_gameManager_3();
		NullCheck(L_0);
		GameManager_t1536523654 * L_1 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_2 = L_1->get_isPaused_4();
		if (L_2)
		{
			goto IL_0102;
		}
	}
	{
		float L_3 = __this->get_spawnTerm_8();
		float L_4 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_spawnTerm_8(((float)il2cpp_codegen_subtract((float)L_3, (float)L_4)));
		float L_5 = __this->get_spawnTerm_8();
		if ((!(((float)L_5) < ((float)(0.0f)))))
		{
			goto IL_0102;
		}
	}
	{
		int32_t L_6 = __this->get_KillCount_9();
		__this->set_KillCount_9(((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1)));
		int32_t L_7 = __this->get_KillCount_9();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)10))))
		{
			goto IL_0063;
		}
	}
	{
		__this->set_spawnTerm_8((4.0f));
		EnemyGenerator_GenerateEnemy_m557526776(__this, /*hidden argument*/NULL);
	}

IL_0063:
	{
		int32_t L_8 = __this->get_KillCount_9();
		if ((((int32_t)L_8) > ((int32_t)((int32_t)16))))
		{
			goto IL_008e;
		}
	}
	{
		int32_t L_9 = __this->get_KillCount_9();
		if ((((int32_t)L_9) <= ((int32_t)((int32_t)10))))
		{
			goto IL_008e;
		}
	}
	{
		__this->set_spawnTerm_8((3.0f));
		EnemyGenerator_GenerateEnemy_m557526776(__this, /*hidden argument*/NULL);
	}

IL_008e:
	{
		int32_t L_10 = __this->get_KillCount_9();
		if ((((int32_t)L_10) > ((int32_t)((int32_t)24))))
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_11 = __this->get_KillCount_9();
		if ((((int32_t)L_11) <= ((int32_t)((int32_t)16))))
		{
			goto IL_00b9;
		}
	}
	{
		__this->set_spawnTerm_8((2.5f));
		EnemyGenerator_GenerateEnemy_m557526776(__this, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		int32_t L_12 = __this->get_KillCount_9();
		if ((((int32_t)L_12) > ((int32_t)((int32_t)30))))
		{
			goto IL_00e4;
		}
	}
	{
		int32_t L_13 = __this->get_KillCount_9();
		if ((((int32_t)L_13) <= ((int32_t)((int32_t)24))))
		{
			goto IL_00e4;
		}
	}
	{
		__this->set_spawnTerm_8((2.0f));
		EnemyGenerator_GenerateEnemy_m557526776(__this, /*hidden argument*/NULL);
	}

IL_00e4:
	{
		int32_t L_14 = __this->get_KillCount_9();
		if ((((int32_t)L_14) <= ((int32_t)((int32_t)30))))
		{
			goto IL_0102;
		}
	}
	{
		__this->set_spawnTerm_8((1.7f));
		EnemyGenerator_GenerateEnemy_m557526776(__this, /*hidden argument*/NULL);
	}

IL_0102:
	{
		return;
	}
}
// System.Void EnemyGenerator::GenerateEnemy()
extern "C" IL2CPP_METHOD_ATTR void EnemyGenerator_GenerateEnemy_m557526776 (EnemyGenerator_t2446750330 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyGenerator_GenerateEnemy_m557526776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = Random_Range_m2202990745(NULL /*static, unused*/, (-50.0f), (50.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3353183577((&L_1), (-150.0f), L_0, (0.0f), /*hidden argument*/NULL);
		__this->set_Position_7(L_1);
		GameObject_t1113636619 * L_2 = __this->get_oldEnemy_4();
		Vector3_t3722313464  L_3 = __this->get_Position_7();
		GameObject_t1113636619 * L_4 = __this->get_oldEnemy_4();
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = GameObject_get_transform_m1369836730(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Quaternion_t2301928331  L_6 = Transform_get_rotation_m3502953881(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_7 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_2, L_3, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		__this->set_newEnemy_5(L_7);
		GameObject_t1113636619 * L_8 = __this->get_newEnemy_5();
		NullCheck(L_8);
		EnemyManager_t913934216 * L_9 = GameObject_GetComponent_TisEnemyManager_t913934216_m743859818(L_8, /*hidden argument*/GameObject_GetComponent_TisEnemyManager_t913934216_m743859818_RuntimeMethod_var);
		NullCheck(L_9);
		L_9->set_isActive_3((bool)1);
		SpriteU5BU5D_t2581906349* L_10 = __this->get_enemySprite_2();
		NullCheck(L_10);
		int32_t L_11 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_11;
		GameObject_t1113636619 * L_12 = __this->get_newEnemy_5();
		NullCheck(L_12);
		Image_t2670269651 * L_13 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_14 = __this->get_enemySprite_2();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Sprite_t280657092 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_13);
		Image_set_sprite_m2369174689(L_13, L_17, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_18 = __this->get_newEnemy_5();
		NullCheck(L_18);
		Image_t2670269651 * L_19 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_18, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		NullCheck(L_19);
		VirtActionInvoker0::Invoke(45 /* System.Void UnityEngine.UI.Graphic::SetNativeSize() */, L_19);
		GameObject_t1113636619 * L_20 = __this->get_newEnemy_5();
		NullCheck(L_20);
		Transform_t3600365921 * L_21 = GameObject_get_transform_m1369836730(L_20, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_22 = __this->get_Bullets_6();
		NullCheck(L_22);
		Transform_t3600365921 * L_23 = GameObject_get_transform_m1369836730(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_SetParent_m273471670(L_21, L_23, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_24 = __this->get_newEnemy_5();
		NullCheck(L_24);
		GameObject_SetActive_m796801857(L_24, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager__ctor_m2955977553 (EnemyManager_t913934216 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (0.0f), (12.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_posDiff_12(L_0);
		__this->set_acceleration_14((0.05f));
		__this->set_mass_15((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyManager::Start()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager_Start_m1431650715 (EnemyManager_t913934216 * __this, const RuntimeMethod* method)
{
	{
		EnemyManager_EnemyInit_m1387525382(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyManager::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager_FixedUpdate_m91462524 (EnemyManager_t913934216 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyManager_FixedUpdate_m91462524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_gameManager_16();
		NullCheck(L_0);
		GameManager_t1536523654 * L_1 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_2 = L_1->get_isGameOver_2();
		if (L_2)
		{
			goto IL_0038;
		}
	}
	{
		bool L_3 = __this->get_isActive_3();
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		bool L_4 = __this->get_isHit_4();
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		EnemyManager_ExplodeDie_m497504152(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}

IL_0032:
	{
		EnemyManager_EnemyMove_m86952508(__this, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void EnemyManager::ExplodeDie()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager_ExplodeDie_m497504152 (EnemyManager_t913934216 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyManager_ExplodeDie_m497504152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_fallDir_8();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_down_m3781355428(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_acceleration_14();
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_mass_15();
		Vector3_t3722313464  L_5 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		__this->set_fallDir_8(L_6);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_8 = L_7;
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = __this->get_fallDir_8();
		Vector3_t3722313464  L_11 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_position_m3387557959(L_8, L_11, /*hidden argument*/NULL);
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3722313464  L_13 = Transform_get_localPosition_m4234289348(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		float L_14 = (&V_0)->get_y_2();
		if ((!(((float)L_14) < ((float)(-300.0f)))))
		{
			goto IL_007c;
		}
	}
	{
		bool L_15 = __this->get_isCloudeFade_5();
		if (!L_15)
		{
			goto IL_007c;
		}
	}
	{
		GameObject_t1113636619 * L_16 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_SetActive_m796801857(L_16, (bool)0, /*hidden argument*/NULL);
	}

IL_007c:
	{
		return;
	}
}
// System.Void EnemyManager::CallCoroutineCloudeFade()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager_CallCoroutineCloudeFade_m1288317783 (EnemyManager_t913934216 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyManager_CallCoroutineCloudeFade_m1288317783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_Find_m1729760951(L_0, _stringLiteral1176917599, /*hidden argument*/NULL);
		NullCheck(L_1);
		Image_t2670269651 * L_2 = Component_GetComponent_TisImage_t2670269651_m980647750(L_1, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		NullCheck(L_2);
		Color_t2555686324  L_3 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_2);
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = Transform_Find_m1729760951(L_4, _stringLiteral1176917599, /*hidden argument*/NULL);
		RuntimeObject* L_6 = EnemyManager_CoroutineCloudFade_m2978522574(__this, (0.0f), (0.1f), L_3, L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator EnemyManager::CoroutineCloudFade(System.Single,System.Single,UnityEngine.Color,UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* EnemyManager_CoroutineCloudFade_m2978522574 (EnemyManager_t913934216 * __this, float ___firstTerm0, float ___tTerm1, Color_t2555686324  ___tmpColor2, Transform_t3600365921 * ___cloud3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyManager_CoroutineCloudFade_m2978522574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * V_0 = NULL;
	{
		U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * L_0 = (U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 *)il2cpp_codegen_object_new(U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655_il2cpp_TypeInfo_var);
		U3CCoroutineCloudFadeU3Ec__Iterator0__ctor_m204138362(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * L_1 = V_0;
		float L_2 = ___firstTerm0;
		NullCheck(L_1);
		L_1->set_firstTerm_0(L_2);
		U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * L_3 = V_0;
		Transform_t3600365921 * L_4 = ___cloud3;
		NullCheck(L_3);
		L_3->set_cloud_1(L_4);
		U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * L_5 = V_0;
		float L_6 = ___tTerm1;
		NullCheck(L_5);
		L_5->set_tTerm_2(L_6);
		U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * L_7 = V_0;
		Color_t2555686324  L_8 = ___tmpColor2;
		NullCheck(L_7);
		L_7->set_tmpColor_5(L_8);
		U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * L_9 = V_0;
		NullCheck(L_9);
		L_9->set_U24this_7(__this);
		U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * L_10 = V_0;
		return L_10;
	}
}
// System.Void EnemyManager::EnemyMove()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager_EnemyMove_m86952508 (EnemyManager_t913934216 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyManager_EnemyMove_m86952508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_localPosition_m4234289348(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		if ((!(((float)L_2) < ((float)(140.0f)))))
		{
			goto IL_013c;
		}
	}
	{
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = L_3;
		NullCheck(L_4);
		Vector3_t3722313464  L_5 = Transform_get_localPosition_m4234289348(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_EnemySpeed_6();
		Vector3_t3722313464  L_8 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_8, (30.0f), /*hidden argument*/NULL);
		float L_10 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_5, L_11, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localPosition_m4128471975(L_4, L_12, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_dir_13();
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_009d;
		}
	}
	{
		Transform_t3600365921 * L_14 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_15 = L_14;
		NullCheck(L_15);
		Vector3_t3722313464  L_16 = Transform_get_localPosition_m4234289348(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_17 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = __this->get_EnemySpeed_6();
		Vector3_t3722313464  L_19 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_19, (30.0f), /*hidden argument*/NULL);
		float L_21 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_22 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		Vector3_t3722313464  L_23 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_16, L_22, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localPosition_m4128471975(L_15, L_23, /*hidden argument*/NULL);
	}

IL_009d:
	{
		int32_t L_24 = __this->get_dir_13();
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_00e3;
		}
	}
	{
		Transform_t3600365921 * L_25 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_26 = L_25;
		NullCheck(L_26);
		Vector3_t3722313464  L_27 = Transform_get_localPosition_m4234289348(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_28 = Vector3_get_down_m3781355428(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_29 = __this->get_EnemySpeed_6();
		Vector3_t3722313464  L_30 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		Vector3_t3722313464  L_31 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_30, (30.0f), /*hidden argument*/NULL);
		float L_32 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_33 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		Vector3_t3722313464  L_34 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_27, L_33, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_localPosition_m4128471975(L_26, L_34, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		Transform_t3600365921 * L_35 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t3722313464  L_36 = Transform_get_localPosition_m4234289348(L_35, /*hidden argument*/NULL);
		V_1 = L_36;
		float L_37 = (&V_1)->get_y_2();
		Vector3_t3722313464 * L_38 = __this->get_address_of_EnemyPos2_10();
		float L_39 = L_38->get_y_2();
		if ((!(((float)L_37) >= ((float)L_39))))
		{
			goto IL_010d;
		}
	}
	{
		__this->set_dir_13((-1));
	}

IL_010d:
	{
		Transform_t3600365921 * L_40 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t3722313464  L_41 = Transform_get_localPosition_m4234289348(L_40, /*hidden argument*/NULL);
		V_2 = L_41;
		float L_42 = (&V_2)->get_y_2();
		Vector3_t3722313464 * L_43 = __this->get_address_of_EnemyPos3_11();
		float L_44 = L_43->get_y_2();
		if ((!(((float)L_42) <= ((float)L_44))))
		{
			goto IL_0137;
		}
	}
	{
		__this->set_dir_13(1);
	}

IL_0137:
	{
		goto IL_015d;
	}

IL_013c:
	{
		GameObject_t1113636619 * L_45 = __this->get_gameManager_16();
		NullCheck(L_45);
		GameManager_t1536523654 * L_46 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_45, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_46);
		GameManager_GameOver_m2317540222(L_46, /*hidden argument*/NULL);
		Transform_t3600365921 * L_47 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		GameObject_t1113636619 * L_48 = Component_get_gameObject_m442555142(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		GameObject_SetActive_m796801857(L_48, (bool)0, /*hidden argument*/NULL);
	}

IL_015d:
	{
		return;
	}
}
// System.Void EnemyManager::EnemyInit()
extern "C" IL2CPP_METHOD_ATTR void EnemyManager_EnemyInit_m1387525382 (EnemyManager_t913934216 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyManager_EnemyInit_m1387525382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_EnemySpeed_6((0.5f));
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_localPosition_m4234289348(L_0, /*hidden argument*/NULL);
		__this->set_EnemyPos1_9(L_1);
		Vector3_t3722313464  L_2 = __this->get_EnemyPos1_9();
		Vector3_t3722313464  L_3 = __this->get_posDiff_12();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_EnemyPos2_10(L_4);
		Vector3_t3722313464  L_5 = __this->get_EnemyPos1_9();
		Vector3_t3722313464  L_6 = __this->get_posDiff_12();
		Vector3_t3722313464  L_7 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		__this->set_EnemyPos3_11(L_7);
		__this->set_dir_13(1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyManager/<CoroutineCloudFade>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineCloudFadeU3Ec__Iterator0__ctor_m204138362 (U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean EnemyManager/<CoroutineCloudFade>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CCoroutineCloudFadeU3Ec__Iterator0_MoveNext_m879295093 (U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCoroutineCloudFadeU3Ec__Iterator0_MoveNext_m879295093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_004a;
			}
			case 2:
			{
				goto IL_00eb;
			}
		}
	}
	{
		goto IL_0131;
	}

IL_0025:
	{
		float L_2 = __this->get_firstTerm_0();
		WaitForSeconds_t1699091251 * L_3 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_8(L_3);
		bool L_4 = __this->get_U24disposing_9();
		if (L_4)
		{
			goto IL_0045;
		}
	}
	{
		__this->set_U24PC_10(1);
	}

IL_0045:
	{
		goto IL_0133;
	}

IL_004a:
	{
		Transform_t3600365921 * L_5 = __this->get_cloud_1();
		EnemyManager_t913934216 * L_6 = __this->get_U24this_7();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = L_6->get_Bullets_7();
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = GameObject_get_transform_m1369836730(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_SetParent_m273471670(L_5, L_8, (bool)1, /*hidden argument*/NULL);
		float L_9 = __this->get_tTerm_2();
		WaitForSeconds_t1699091251 * L_10 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_10, L_9, /*hidden argument*/NULL);
		__this->set_U3CtWFSU3E__0_3(L_10);
		Transform_t3600365921 * L_11 = __this->get_cloud_1();
		NullCheck(L_11);
		Image_t2670269651 * L_12 = Component_GetComponent_TisImage_t2670269651_m980647750(L_11, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		NullCheck(L_12);
		Color_t2555686324  L_13 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_12);
		__this->set_U3CtempColorU3E__0_4(L_13);
		Color_t2555686324 * L_14 = __this->get_address_of_tmpColor_5();
		float L_15 = L_14->get_a_3();
		__this->set_U3CindexU3E__1_6(L_15);
		goto IL_00fd;
	}

IL_00a3:
	{
		Color_t2555686324 * L_16 = __this->get_address_of_U3CtempColorU3E__0_4();
		float L_17 = __this->get_U3CindexU3E__1_6();
		L_16->set_a_3((((float)((float)L_17))));
		Transform_t3600365921 * L_18 = __this->get_cloud_1();
		NullCheck(L_18);
		Image_t2670269651 * L_19 = Component_GetComponent_TisImage_t2670269651_m980647750(L_18, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color_t2555686324  L_20 = __this->get_U3CtempColorU3E__0_4();
		NullCheck(L_19);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_19, L_20);
		WaitForSeconds_t1699091251 * L_21 = __this->get_U3CtWFSU3E__0_3();
		__this->set_U24current_8(L_21);
		bool L_22 = __this->get_U24disposing_9();
		if (L_22)
		{
			goto IL_00e6;
		}
	}
	{
		__this->set_U24PC_10(2);
	}

IL_00e6:
	{
		goto IL_0133;
	}

IL_00eb:
	{
		float L_23 = __this->get_U3CindexU3E__1_6();
		__this->set_U3CindexU3E__1_6(((float)il2cpp_codegen_subtract((float)L_23, (float)(0.2f))));
	}

IL_00fd:
	{
		float L_24 = __this->get_U3CindexU3E__1_6();
		if ((((float)L_24) >= ((float)(-0.2f))))
		{
			goto IL_00a3;
		}
	}
	{
		EnemyManager_t913934216 * L_25 = __this->get_U24this_7();
		NullCheck(L_25);
		L_25->set_isCloudeFade_5((bool)1);
		Transform_t3600365921 * L_26 = __this->get_cloud_1();
		NullCheck(L_26);
		GameObject_t1113636619 * L_27 = Component_get_gameObject_m442555142(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m796801857(L_27, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_10((-1));
	}

IL_0131:
	{
		return (bool)0;
	}

IL_0133:
	{
		return (bool)1;
	}
}
// System.Object EnemyManager/<CoroutineCloudFade>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCoroutineCloudFadeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m450653611 (U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Object EnemyManager/<CoroutineCloudFade>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCoroutineCloudFadeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4134908704 (U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Void EnemyManager/<CoroutineCloudFade>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineCloudFadeU3Ec__Iterator0_Dispose_m2828755656 (U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_9((bool)1);
		__this->set_U24PC_10((-1));
		return;
	}
}
// System.Void EnemyManager/<CoroutineCloudFade>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineCloudFadeU3Ec__Iterator0_Reset_m94972674 (U3CCoroutineCloudFadeU3Ec__Iterator0_t2338107655 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCoroutineCloudFadeU3Ec__Iterator0_Reset_m94972674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CCoroutineCloudFadeU3Ec__Iterator0_Reset_m94972674_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FireEX::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FireEX__ctor_m2703300390 (FireEX_t4238753029 * __this, const RuntimeMethod* method)
{
	{
		__this->set_acceleration_3((20.0f));
		__this->set_initialVelocity_4((5.0f));
		__this->set_angle_5((45.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FireEX::Start()
extern "C" IL2CPP_METHOD_ATTR void FireEX_Start_m1619158142 (FireEX_t4238753029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FireEX_Start_m1619158142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t939494601 * L_0 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		__this->set_rb2d_2(L_0);
		return;
	}
}
// System.Void FireEX::Update()
extern "C" IL2CPP_METHOD_ATTR void FireEX_Update_m612822988 (FireEX_t4238753029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FireEX_Update_m612822988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_angle_5();
		__this->set_radians_6(((float)((float)((float)il2cpp_codegen_multiply((float)L_0, (float)(3.14159274f)))/(float)(180.0f))));
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = L_1;
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_initialVelocity_4();
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_radians_6();
		double L_8 = cos((((double)((double)L_7))));
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, (((float)((float)L_8))), /*hidden argument*/NULL);
		float L_10 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_3, L_11, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3387557959(L_2, L_12, /*hidden argument*/NULL);
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_14 = L_13;
		NullCheck(L_14);
		Vector3_t3722313464  L_15 = Transform_get_position_m36019626(L_14, /*hidden argument*/NULL);
		Vector3_t3722313464  L_16 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_17 = __this->get_initialVelocity_4();
		float L_18 = __this->get_radians_6();
		double L_19 = sin((((double)((double)L_18))));
		float L_20 = __this->get_acceleration_3();
		float L_21 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_22 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_16, ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_17, (float)(((float)((float)L_19))))), (float)((float)il2cpp_codegen_multiply((float)L_20, (float)L_21)))), /*hidden argument*/NULL);
		float L_23 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_24 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		Vector3_t3722313464  L_25 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_15, L_24, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_position_m3387557959(L_14, L_25, /*hidden argument*/NULL);
		float L_26 = __this->get_initialVelocity_4();
		float L_27 = __this->get_radians_6();
		double L_28 = sin((((double)((double)L_27))));
		float L_29 = __this->get_acceleration_3();
		float L_30 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_31 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_26, (float)(((float)((float)L_28))))), (float)((float)il2cpp_codegen_multiply((float)L_29, (float)L_30))));
		RuntimeObject * L_32 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_31);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GameManager__ctor_m180891015 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager__ctor_m180891015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_highScoreKey_9(_stringLiteral167841699);
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (103.6f), (3.9f), (0.0f), /*hidden argument*/NULL);
		__this->set_StartingPos_11(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::SetCountText()
extern "C" IL2CPP_METHOD_ATTR void GameManager_SetCountText_m3505865510 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void GameManager::Start()
extern "C" IL2CPP_METHOD_ATTR void GameManager_Start_m2734446095 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	{
		GameManager_GameManagerInit_m1907792971(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Update()
extern "C" IL2CPP_METHOD_ATTR void GameManager_Update_m1981238775 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void GameManager::GameManagerInit()
extern "C" IL2CPP_METHOD_ATTR void GameManager_GameManagerInit_m1907792971 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_GameManagerInit_m1907792971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_highScoreKey_9();
		int32_t L_1 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		__this->set_highscore_7(L_1);
		Text_t1901882714 * L_2 = __this->get_highscoreText_8();
		int32_t* L_3 = __this->get_address_of_highscore_7();
		String_t* L_4 = Int32_ToString_m141394615((int32_t*)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral309511058, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_5);
		return;
	}
}
// System.Void GameManager::GameOver()
extern "C" IL2CPP_METHOD_ATTR void GameManager_GameOver_m2317540222 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	{
		__this->set_isGameOver_2((bool)1);
		GameManager_UpdateHighscore_m3829313171(__this, /*hidden argument*/NULL);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Pause()
extern "C" IL2CPP_METHOD_ATTR void GameManager_Pause_m1123958121 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	{
		__this->set_isPaused_4((bool)1);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Continue()
extern "C" IL2CPP_METHOD_ATTR void GameManager_Continue_m805598998 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_enemyVictory_3();
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		__this->set_isPaused_4((bool)0);
		__this->set_isPlay_5((bool)1);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void GameManager::Replay()
extern "C" IL2CPP_METHOD_ATTR void GameManager_Replay_m1504520858 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Replay_m1504520858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameManager_UpdateHighscore_m3829313171(__this, /*hidden argument*/NULL);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = __this->get_Character_10();
		NullCheck(L_0);
		CharacterManager_t2849944402 * L_1 = GameObject_GetComponent_TisCharacterManager_t2849944402_m3321491559(L_0, /*hidden argument*/GameObject_GetComponent_TisCharacterManager_t2849944402_m3321491559_RuntimeMethod_var);
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = L_1->get_CharacterPosition_6();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730(L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = __this->get_StartingPos_11();
		NullCheck(L_3);
		Transform_set_localPosition_m4128471975(L_3, L_4, /*hidden argument*/NULL);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral3105844272, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::StartingPage()
extern "C" IL2CPP_METHOD_ATTR void GameManager_StartingPage_m2315722183 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_StartingPage_m2315722183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameManager_UpdateHighscore_m3829313171(__this, /*hidden argument*/NULL);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral435698133, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::UpdateHighscore()
extern "C" IL2CPP_METHOD_ATTR void GameManager_UpdateHighscore_m3829313171 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_UpdateHighscore_m3829313171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_highscore_7();
		int32_t L_1 = __this->get_score_6();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral817681, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_score_6();
		__this->set_highscore_7(L_2);
		String_t* L_3 = __this->get_highScoreKey_9();
		int32_t L_4 = __this->get_highscore_7();
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		PlayerPrefs_Save_m2701929462(NULL /*static, unused*/, /*hidden argument*/NULL);
		Text_t1901882714 * L_5 = __this->get_highscoreText_8();
		int32_t* L_6 = __this->get_address_of_highscore_7();
		String_t* L_7 = Int32_ToString_m141394615((int32_t*)L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral309511058, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_8);
	}

IL_0063:
	{
		return;
	}
}
// System.Void GameManager::UpdateScore()
extern "C" IL2CPP_METHOD_ATTR void GameManager_UpdateScore_m509300891 (GameManager_t1536523654 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_UpdateScore_m509300891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t1901882714 * L_0 = __this->get_scoreText_12();
		int32_t* L_1 = __this->get_address_of_score_6();
		String_t* L_2 = Int32_ToString_m141394615((int32_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1944369699, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LineRenderManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager__ctor_m429748140 (LineRenderManager_t120866003 * __this, const RuntimeMethod* method)
{
	{
		__this->set_radius_10((10.0f));
		__this->set_mySetTime_12((0.7f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LineRenderManager::Start()
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_Start_m3515553268 (LineRenderManager_t120866003 * __this, const RuntimeMethod* method)
{
	{
		LineRenderManager_LineRenderManagerInit_m2547092565(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LineRenderManager::Update()
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_Update_m3617602935 (LineRenderManager_t120866003 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LineRenderManager_Update_m3617602935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_gameManager_11();
		NullCheck(L_0);
		GameManager_t1536523654 * L_1 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_2 = L_1->get_isGameOver_2();
		if (L_2)
		{
			goto IL_0048;
		}
	}
	{
		GameObject_t1113636619 * L_3 = __this->get_gameManager_11();
		NullCheck(L_3);
		GameManager_t1536523654 * L_4 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_3, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_4);
		bool L_5 = L_4->get_isPaused_4();
		if (L_5)
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = __this->get_myTimer_13();
		float L_7 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_myTimer_13(((float)il2cpp_codegen_add((float)L_6, (float)L_7)));
		LineRenderManager_UpdatePosition_m280241269(__this, /*hidden argument*/NULL);
		LineRenderManager_SetPosition_m851472669(__this, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Void LineRenderManager::TaskOnClick()
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_TaskOnClick_m1632601449 (LineRenderManager_t120866003 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LineRenderManager_TaskOnClick_m1632601449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1113636619 * L_0 = __this->get_gameManager_11();
		NullCheck(L_0);
		GameManager_t1536523654 * L_1 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_2 = L_1->get_isGameOver_2();
		if (L_2)
		{
			goto IL_007a;
		}
	}
	{
		GameObject_t1113636619 * L_3 = __this->get_gameManager_11();
		NullCheck(L_3);
		GameManager_t1536523654 * L_4 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_3, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_4);
		bool L_5 = L_4->get_isPaused_4();
		if (L_5)
		{
			goto IL_007a;
		}
	}
	{
		float L_6 = __this->get_myTimer_13();
		float L_7 = __this->get_mySetTime_12();
		if ((!(((float)L_6) > ((float)L_7))))
		{
			goto IL_007a;
		}
	}
	{
		__this->set_myTimer_13((0.0f));
		BulletGenerator_t1131765196 * L_8 = __this->get_bulletGenerator_14();
		Vector2_t2156229523  L_9 = __this->get_lineVector02_4();
		Vector2_t2156229523  L_10 = __this->get_lineVector01_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_11 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		Vector2_t2156229523  L_12 = Vector2_get_normalized_m2683665860((Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_13 = __this->get_Missile_6();
		NullCheck(L_13);
		Transform_t3600365921 * L_14 = GameObject_get_transform_m1369836730(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t3722313464  L_15 = Transform_get_position_m36019626(L_14, /*hidden argument*/NULL);
		NullCheck(L_8);
		BulletGenerator_GenerateBullet_m1172315483(L_8, L_12, L_15, /*hidden argument*/NULL);
	}

IL_007a:
	{
		return;
	}
}
// System.Void LineRenderManager::UpdatePosition()
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_UpdatePosition_m280241269 (LineRenderManager_t120866003 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LineRenderManager_UpdatePosition_m280241269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		GameObject_t1113636619 * L_0 = __this->get_Character_5();
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m1369836730(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = (&V_1)->get_x_1();
		GameObject_t1113636619 * L_4 = __this->get_Character_5();
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = GameObject_get_transform_m1369836730(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		float L_7 = (&V_2)->get_y_2();
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_0), ((float)il2cpp_codegen_subtract((float)L_3, (float)(8.0f))), L_7, /*hidden argument*/NULL);
		float L_8 = __this->get_ShootAngle_7();
		if ((!(((float)L_8) >= ((float)(165.0f)))))
		{
			goto IL_0055;
		}
	}
	{
		__this->set_direction_9(((int32_t)-3));
	}

IL_0055:
	{
		float L_9 = __this->get_ShootAngle_7();
		if ((!(((float)L_9) <= ((float)(10.0f)))))
		{
			goto IL_006c;
		}
	}
	{
		__this->set_direction_9(3);
	}

IL_006c:
	{
		float L_10 = __this->get_ShootAngle_7();
		int32_t L_11 = __this->get_direction_9();
		__this->set_ShootAngle_7(((float)il2cpp_codegen_add((float)L_10, (float)(((float)((float)L_11))))));
		float L_12 = __this->get_ShootAngle_7();
		__this->set_radians_8(((float)((float)((float)il2cpp_codegen_multiply((float)L_12, (float)(3.14159274f)))/(float)(180.0f))));
		GameObject_t1113636619 * L_13 = __this->get_Character_5();
		NullCheck(L_13);
		Transform_t3600365921 * L_14 = GameObject_get_transform_m1369836730(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t3722313464  L_15 = Transform_get_position_m36019626(L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = (&V_4)->get_x_1();
		float L_17 = __this->get_radius_10();
		float L_18 = __this->get_radians_8();
		double L_19 = sin((((double)((double)L_18))));
		GameObject_t1113636619 * L_20 = __this->get_Character_5();
		NullCheck(L_20);
		Transform_t3600365921 * L_21 = GameObject_get_transform_m1369836730(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t3722313464  L_22 = Transform_get_position_m36019626(L_21, /*hidden argument*/NULL);
		V_5 = L_22;
		float L_23 = (&V_5)->get_y_2();
		float L_24 = __this->get_radius_10();
		float L_25 = __this->get_radians_8();
		double L_26 = cos((((double)((double)L_25))));
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_3), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_16, (float)(8.0f))), (float)((float)il2cpp_codegen_multiply((float)L_17, (float)(((float)((float)L_19))))))), ((float)il2cpp_codegen_add((float)L_23, (float)((float)il2cpp_codegen_multiply((float)L_24, (float)(((float)((float)L_26))))))), /*hidden argument*/NULL);
		Vector2_t2156229523  L_27 = V_0;
		Vector2_t2156229523  L_28 = V_3;
		LineRenderManager_LineRenderSetPos_m3403892388(__this, L_27, L_28, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_29 = __this->get_Missile_6();
		NullCheck(L_29);
		Transform_t3600365921 * L_30 = GameObject_get_transform_m1369836730(L_29, /*hidden argument*/NULL);
		Vector2_t2156229523  L_31 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_32 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_position_m3387557959(L_30, L_32, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_33 = __this->get_Missile_6();
		NullCheck(L_33);
		Transform_t3600365921 * L_34 = GameObject_get_transform_m1369836730(L_33, /*hidden argument*/NULL);
		float L_35 = __this->get_ShootAngle_7();
		Vector3_t3722313464  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector3__ctor_m3353183577((&L_36), (0.0f), (0.0f), ((float)il2cpp_codegen_subtract((float)L_35, (float)(90.0f))), /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_eulerAngles_m135219616(L_34, L_36, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LineRenderManager::LineRenderManagerInit()
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_LineRenderManagerInit_m2547092565 (LineRenderManager_t120866003 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LineRenderManager_LineRenderManagerInit_m2547092565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LineRenderer_t3154350270 * L_0 = __this->get_lineRenderer_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		LineRenderer_t3154350270 * L_3 = GameObject_GetComponent_TisLineRenderer_t3154350270_m1162201727(L_2, /*hidden argument*/GameObject_GetComponent_TisLineRenderer_t3154350270_m1162201727_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_AddComponent_TisLineRenderer_t3154350270_m2920077551(L_5, /*hidden argument*/GameObject_AddComponent_TisLineRenderer_t3154350270_m2920077551_RuntimeMethod_var);
	}

IL_0033:
	{
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		LineRenderer_t3154350270 * L_7 = GameObject_GetComponent_TisLineRenderer_t3154350270_m1162201727(L_6, /*hidden argument*/GameObject_GetComponent_TisLineRenderer_t3154350270_m1162201727_RuntimeMethod_var);
		__this->set_lineRenderer_2(L_7);
	}

IL_0044:
	{
		LineRenderer_t3154350270 * L_8 = __this->get_lineRenderer_2();
		NullCheck(L_8);
		LineRenderer_set_startWidth_m1093267133(L_8, (1.0f), /*hidden argument*/NULL);
		LineRenderer_t3154350270 * L_9 = __this->get_lineRenderer_2();
		NullCheck(L_9);
		LineRenderer_set_endWidth_m4252049505(L_9, (1.0f), /*hidden argument*/NULL);
		__this->set_ShootAngle_7((10.0f));
		__this->set_direction_9((-1));
		return;
	}
}
// System.Void LineRenderManager::SetPosition()
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_SetPosition_m851472669 (LineRenderManager_t120866003 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LineRenderManager_SetPosition_m851472669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LineRenderer_t3154350270 * L_0 = __this->get_lineRenderer_2();
		Vector2_t2156229523  L_1 = __this->get_lineVector01_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		LineRenderer_SetPosition_m2111131184(L_0, 0, L_2, /*hidden argument*/NULL);
		LineRenderer_t3154350270 * L_3 = __this->get_lineRenderer_2();
		Vector2_t2156229523  L_4 = __this->get_lineVector02_4();
		Vector3_t3722313464  L_5 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		LineRenderer_SetPosition_m2111131184(L_3, 1, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LineRenderManager::LineRenderSetPos(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_LineRenderSetPos_m4160220108 (LineRenderManager_t120866003 * __this, GameObject_t1113636619 * ___Obj10, GameObject_t1113636619 * ___Obj21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LineRenderManager_LineRenderSetPos_m4160220108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = ___Obj10;
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m1369836730(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_3 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_lineVector01_3(L_3);
		GameObject_t1113636619 * L_4 = ___Obj21;
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = GameObject_get_transform_m1369836730(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		Vector2_t2156229523  L_7 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_lineVector02_4(L_7);
		return;
	}
}
// System.Void LineRenderManager::LineRenderSetPos(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void LineRenderManager_LineRenderSetPos_m3403892388 (LineRenderManager_t120866003 * __this, Vector2_t2156229523  ___Pos10, Vector2_t2156229523  ___Pos21, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523  L_0 = ___Pos10;
		__this->set_lineVector01_3(L_0);
		Vector2_t2156229523  L_1 = ___Pos21;
		__this->set_lineVector02_4(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MessageManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MessageManager__ctor_m561400212 (MessageManager_t336836297 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MessageManager::Start()
extern "C" IL2CPP_METHOD_ATTR void MessageManager_Start_m2429683358 (MessageManager_t336836297 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void MessageManager::Update()
extern "C" IL2CPP_METHOD_ATTR void MessageManager_Update_m1417183598 (MessageManager_t336836297 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void MessageManager::PlayPressed()
extern "C" IL2CPP_METHOD_ATTR void MessageManager_PlayPressed_m542946951 (MessageManager_t336836297 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageManager_PlayPressed_m542946951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1113636619 * L_0 = __this->get_gameManager_2();
		NullCheck(L_0);
		GameManager_t1536523654 * L_1 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_2 = L_1->get_isPlay_5();
		if (!L_2)
		{
			goto IL_00b0;
		}
	}
	{
		GameObject_t1113636619 * L_3 = __this->get_gameManager_2();
		NullCheck(L_3);
		GameManager_t1536523654 * L_4 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_3, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_4);
		L_4->set_isPaused_4((bool)1);
		GameObject_t1113636619 * L_5 = __this->get_message_3();
		NullCheck(L_5);
		Image_t2670269651 * L_6 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_5, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		NullCheck(L_6);
		Color_t2555686324  L_7 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_6);
		V_0 = L_7;
		(&V_0)->set_a_3((1.0f));
		GameObject_t1113636619 * L_8 = __this->get_message_3();
		NullCheck(L_8);
		Image_t2670269651 * L_9 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_8, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		Color_t2555686324  L_10 = V_0;
		NullCheck(L_9);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_10);
		GameObject_t1113636619 * L_11 = __this->get_message_3();
		RuntimeObject* L_12 = MessageManager_CoroutineCountDownAnimation_m3397114711(__this, (0.0f), (0.8f), L_11, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_12, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_13 = __this->get_gameManager_2();
		NullCheck(L_13);
		GameManager_t1536523654 * L_14 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_13, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_14);
		L_14->set_isPlay_5((bool)0);
		GameObject_t1113636619 * L_15 = __this->get_gameManager_2();
		NullCheck(L_15);
		GameManager_t1536523654 * L_16 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_15, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_16);
		L_16->set_isPaused_4((bool)0);
		(&V_0)->set_a_3((0.0f));
		GameObject_t1113636619 * L_17 = __this->get_message_3();
		NullCheck(L_17);
		Image_t2670269651 * L_18 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_17, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		Color_t2555686324  L_19 = V_0;
		NullCheck(L_18);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_18, L_19);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void MessageManager::PausePressed()
extern "C" IL2CPP_METHOD_ATTR void MessageManager_PausePressed_m2940643355 (MessageManager_t336836297 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageManager_PausePressed_m2940643355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1113636619 * L_0 = __this->get_gameManager_2();
		NullCheck(L_0);
		GameManager_t1536523654 * L_1 = GameObject_GetComponent_TisGameManager_t1536523654_m2510894885(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t1536523654_m2510894885_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_2 = L_1->get_isPaused_4();
		if (!L_2)
		{
			goto IL_0069;
		}
	}
	{
		GameObject_t1113636619 * L_3 = __this->get_message_3();
		NullCheck(L_3);
		Image_t2670269651 * L_4 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		NullCheck(L_4);
		Color_t2555686324  L_5 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_4);
		V_0 = L_5;
		(&V_0)->set_a_3((1.0f));
		GameObject_t1113636619 * L_6 = __this->get_message_3();
		NullCheck(L_6);
		Image_t2670269651 * L_7 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_6, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		Color_t2555686324  L_8 = V_0;
		NullCheck(L_7);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_7, L_8);
		GameObject_t1113636619 * L_9 = __this->get_message_3();
		NullCheck(L_9);
		Image_t2670269651 * L_10 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_9, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		Sprite_t280657092 * L_11 = __this->get_pause_5();
		NullCheck(L_10);
		Image_set_sprite_m2369174689(L_10, L_11, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_12 = __this->get_message_3();
		NullCheck(L_12);
		Image_t2670269651 * L_13 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(45 /* System.Void UnityEngine.UI.Graphic::SetNativeSize() */, L_13);
	}

IL_0069:
	{
		return;
	}
}
// System.Collections.IEnumerator MessageManager::CoroutineCountDownAnimation(System.Single,System.Single,UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MessageManager_CoroutineCountDownAnimation_m3397114711 (MessageManager_t336836297 * __this, float ___firstTerm0, float ___tTerm1, GameObject_t1113636619 * ___other2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageManager_CoroutineCountDownAnimation_m3397114711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * V_0 = NULL;
	{
		U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * L_0 = (U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 *)il2cpp_codegen_object_new(U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551_il2cpp_TypeInfo_var);
		U3CCoroutineCountDownAnimationU3Ec__Iterator0__ctor_m1450384833(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * L_1 = V_0;
		float L_2 = ___firstTerm0;
		NullCheck(L_1);
		L_1->set_firstTerm_0(L_2);
		U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * L_3 = V_0;
		float L_4 = ___tTerm1;
		NullCheck(L_3);
		L_3->set_tTerm_1(L_4);
		U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U24this_4(__this);
		U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * L_6 = V_0;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MessageManager/<CoroutineCountDownAnimation>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineCountDownAnimationU3Ec__Iterator0__ctor_m1450384833 (U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MessageManager/<CoroutineCountDownAnimation>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CCoroutineCountDownAnimationU3Ec__Iterator0_MoveNext_m2633806077 (U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCoroutineCountDownAnimationU3Ec__Iterator0_MoveNext_m2633806077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_004a;
			}
			case 2:
			{
				goto IL_00c3;
			}
		}
	}
	{
		goto IL_00f0;
	}

IL_0025:
	{
		float L_2 = __this->get_firstTerm_0();
		WaitForSeconds_t1699091251 * L_3 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_5(L_3);
		bool L_4 = __this->get_U24disposing_6();
		if (L_4)
		{
			goto IL_0045;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0045:
	{
		goto IL_00f2;
	}

IL_004a:
	{
		float L_5 = __this->get_tTerm_1();
		WaitForSeconds_t1699091251 * L_6 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U3CtWFSU3E__0_2(L_6);
		__this->set_U3CindexU3E__1_3(0);
		goto IL_00d1;
	}

IL_0067:
	{
		MessageManager_t336836297 * L_7 = __this->get_U24this_4();
		NullCheck(L_7);
		GameObject_t1113636619 * L_8 = L_7->get_message_3();
		NullCheck(L_8);
		Image_t2670269651 * L_9 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_8, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		MessageManager_t336836297 * L_10 = __this->get_U24this_4();
		NullCheck(L_10);
		SpriteU5BU5D_t2581906349* L_11 = L_10->get_countdown_4();
		int32_t L_12 = __this->get_U3CindexU3E__1_3();
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Sprite_t280657092 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		Image_set_sprite_m2369174689(L_9, L_14, /*hidden argument*/NULL);
		MessageManager_t336836297 * L_15 = __this->get_U24this_4();
		NullCheck(L_15);
		GameObject_t1113636619 * L_16 = L_15->get_message_3();
		NullCheck(L_16);
		Image_t2670269651 * L_17 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_16, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(45 /* System.Void UnityEngine.UI.Graphic::SetNativeSize() */, L_17);
		WaitForSeconds_t1699091251 * L_18 = __this->get_U3CtWFSU3E__0_2();
		__this->set_U24current_5(L_18);
		bool L_19 = __this->get_U24disposing_6();
		if (L_19)
		{
			goto IL_00be;
		}
	}
	{
		__this->set_U24PC_7(2);
	}

IL_00be:
	{
		goto IL_00f2;
	}

IL_00c3:
	{
		int32_t L_20 = __this->get_U3CindexU3E__1_3();
		__this->set_U3CindexU3E__1_3(((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1)));
	}

IL_00d1:
	{
		int32_t L_21 = __this->get_U3CindexU3E__1_3();
		MessageManager_t336836297 * L_22 = __this->get_U24this_4();
		NullCheck(L_22);
		SpriteU5BU5D_t2581906349* L_23 = L_22->get_countdown_4();
		NullCheck(L_23);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length)))))))
		{
			goto IL_0067;
		}
	}
	{
		__this->set_U24PC_7((-1));
	}

IL_00f0:
	{
		return (bool)0;
	}

IL_00f2:
	{
		return (bool)1;
	}
}
// System.Object MessageManager/<CoroutineCountDownAnimation>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCoroutineCountDownAnimationU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3581052021 (U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object MessageManager/<CoroutineCountDownAnimation>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCoroutineCountDownAnimationU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2391559831 (U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void MessageManager/<CoroutineCountDownAnimation>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineCountDownAnimationU3Ec__Iterator0_Dispose_m3085192059 (U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MessageManager/<CoroutineCountDownAnimation>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CCoroutineCountDownAnimationU3Ec__Iterator0_Reset_m698207040 (U3CCoroutineCountDownAnimationU3Ec__Iterator0_t3654333551 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCoroutineCountDownAnimationU3Ec__Iterator0_Reset_m698207040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CCoroutineCountDownAnimationU3Ec__Iterator0_Reset_m698207040_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoveManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MoveManager__ctor_m1586807790 (MoveManager_t918348315 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (162.8f), (-42.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_returnSpot_7(L_0);
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3353183577((&L_1), (0.0f), (6.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_posDiff_8(L_1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoveManager::Start()
extern "C" IL2CPP_METHOD_ATTR void MoveManager_Start_m893941112 (MoveManager_t918348315 * __this, const RuntimeMethod* method)
{
	{
		MoveManager_EnemyInit_m4095141009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoveManager::Update()
extern "C" IL2CPP_METHOD_ATTR void MoveManager_Update_m1885679883 (MoveManager_t918348315 * __this, const RuntimeMethod* method)
{
	{
		MoveManager_EnemyMove_m400884860(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoveManager::EnemyMove()
extern "C" IL2CPP_METHOD_ATTR void MoveManager_EnemyMove_m400884860 (MoveManager_t918348315 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MoveManager_EnemyMove_m400884860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_localPosition_m4234289348(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		if ((!(((float)L_2) > ((float)(-157.0f)))))
		{
			goto IL_013c;
		}
	}
	{
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = L_3;
		NullCheck(L_4);
		Vector3_t3722313464  L_5 = Transform_get_localPosition_m4234289348(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector3_get_left_m2428419009(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_EnemySpeed_2();
		Vector3_t3722313464  L_8 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_8, (30.0f), /*hidden argument*/NULL);
		float L_10 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_5, L_11, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localPosition_m4128471975(L_4, L_12, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_dir_3();
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_009d;
		}
	}
	{
		Transform_t3600365921 * L_14 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_15 = L_14;
		NullCheck(L_15);
		Vector3_t3722313464  L_16 = Transform_get_localPosition_m4234289348(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_17 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = __this->get_EnemySpeed_2();
		Vector3_t3722313464  L_19 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_19, (30.0f), /*hidden argument*/NULL);
		float L_21 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_22 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		Vector3_t3722313464  L_23 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_16, L_22, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localPosition_m4128471975(L_15, L_23, /*hidden argument*/NULL);
	}

IL_009d:
	{
		int32_t L_24 = __this->get_dir_3();
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_00e3;
		}
	}
	{
		Transform_t3600365921 * L_25 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_26 = L_25;
		NullCheck(L_26);
		Vector3_t3722313464  L_27 = Transform_get_localPosition_m4234289348(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_28 = Vector3_get_down_m3781355428(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_29 = __this->get_EnemySpeed_2();
		Vector3_t3722313464  L_30 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		Vector3_t3722313464  L_31 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_30, (30.0f), /*hidden argument*/NULL);
		float L_32 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_33 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		Vector3_t3722313464  L_34 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_27, L_33, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_localPosition_m4128471975(L_26, L_34, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		Transform_t3600365921 * L_35 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t3722313464  L_36 = Transform_get_localPosition_m4234289348(L_35, /*hidden argument*/NULL);
		V_1 = L_36;
		float L_37 = (&V_1)->get_y_2();
		Vector3_t3722313464 * L_38 = __this->get_address_of_EnemyPos2_5();
		float L_39 = L_38->get_y_2();
		if ((!(((float)L_37) >= ((float)L_39))))
		{
			goto IL_010d;
		}
	}
	{
		__this->set_dir_3((-1));
	}

IL_010d:
	{
		Transform_t3600365921 * L_40 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t3722313464  L_41 = Transform_get_localPosition_m4234289348(L_40, /*hidden argument*/NULL);
		V_2 = L_41;
		float L_42 = (&V_2)->get_y_2();
		Vector3_t3722313464 * L_43 = __this->get_address_of_EnemyPos3_6();
		float L_44 = L_43->get_y_2();
		if ((!(((float)L_42) <= ((float)L_44))))
		{
			goto IL_0137;
		}
	}
	{
		__this->set_dir_3(1);
	}

IL_0137:
	{
		goto IL_014d;
	}

IL_013c:
	{
		Transform_t3600365921 * L_45 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_46 = __this->get_returnSpot_7();
		NullCheck(L_45);
		Transform_set_localPosition_m4128471975(L_45, L_46, /*hidden argument*/NULL);
	}

IL_014d:
	{
		return;
	}
}
// System.Void MoveManager::EnemyInit()
extern "C" IL2CPP_METHOD_ATTR void MoveManager_EnemyInit_m4095141009 (MoveManager_t918348315 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MoveManager_EnemyInit_m4095141009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_EnemySpeed_2((0.5f));
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_localPosition_m4234289348(L_0, /*hidden argument*/NULL);
		__this->set_EnemyPos1_4(L_1);
		Vector3_t3722313464  L_2 = __this->get_EnemyPos1_4();
		Vector3_t3722313464  L_3 = __this->get_posDiff_8();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_EnemyPos2_5(L_4);
		Vector3_t3722313464  L_5 = __this->get_EnemyPos1_4();
		Vector3_t3722313464  L_6 = __this->get_posDiff_8();
		Vector3_t3722313464  L_7 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		__this->set_EnemyPos3_6(L_7);
		__this->set_dir_3(1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
